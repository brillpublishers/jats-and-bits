# Brill XML Instructions

## Welcome to Brill's XML Instructions

This website describes the Brill subset of XML Instructions for Books, Chapters, Journal Articles and Reference Works, respectively.

Please read the *Introduction* below, then continue with either the [JATS Article instructions](jats.md), [BITS Book/Chapter instructions](bits.md) or *TEI Reference Works instructions*. For more information on the development of these instructions, please read the [Changelog](changelog.md).

## Introduction

This documentation is of a subset of [JATS 1.1](http://jats.nlm.nih.gov/publishing/) and [BITS 2.0](http://jats.nlm.nih.gov/extensions/bits/). Both are the latest versions of NLM's tag suites for journal and book metadata and content. As a publisher-specific subset, it *does* conform to the Tag Suites, but &ndash; similarly &ndash; tries to restrict the tagging possibilities.

In the future we will also host our TEI Reference Work instructions here.

## Delivery

### General

- PDF has to be compatible backwards with Acrobat 4.0 (PDF 1.3).
- Each PDF document has to be optimized for fast web view.
- Security setting: no limitation
- Thumbnails have to be embedded
- No blank pages
- Searchable PDF
- PDF has to be saved as 'uncertified PDF' to avoid pop ups with online users.

### Page Size

Page size should be uniformly throughout the document and have the same size matching the printing format (final page size). No crop marks and printer marks. All pages of the document have to be in vertical format (portrait).

### Images

Images included in the PDF should match the following requirements. This is mainly important for the reproduction of images before producing the PDF which will affect the result of images included in the PDF file.
Figure resolution = final resolution (relating to the final size of the figure).
- 150-300 dpi for color, RGB mode
- 150-300 dpi for grayscale
- 600 dpi for line art (bitmap)
- EPS: if vector EPS is used all fonts have to be embedded

### Text

All fonts have to be fully embedded (no subset embedding if possible).
Text has to be fully searchable.

## Notation in this documentation

Elements are notated with angle brackets, like `<example>`.

```
<example>Mary saw John in the yard.</example>
```

Attributes are notated with a preceding at-sign, like `@type`.

```
<example type="trivial example">John saw Mary in the yard, too.</example>
```

## Abbreviations

- **BITS** Book Interchange Tag Set
- **BOCS** Brill Online Content Standard
- **BOMS** Brill Online Metadata Standard
- **DOI** Digital Object Identifier
- **EM** Editorial Manager
- **JATS** Journal Article Tag Set
- **TEI** Text Encoding Initiative

## Special Remarks

Some parts of Brill products are not contained in the BITS/JATS documentation, like a special image on the page opposite the first page. Elements like these have an idiosyncratic way to be tagged and are all put in this list. This explicates the Brill stylesheet and opens the way for new BITS/JATS elements.

- **Line numbers on quoted text** &ndash; Expected: `<label>` in `<verse-line>`. Not part of JATS or BITS.
- **Image frontispiece on articles and books** &ndash; Expected: `<fig>` to be allowed in front-matter. Solution: First tag in content is `<figwrap position="float" content-type="frontispiece">`.
- **Article dedication** &ndash; Expected: `<dedication>` in the JATS tag suite. Solution: move dedication to a normal `<author-notes>`.
