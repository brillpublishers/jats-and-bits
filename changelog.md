# Changelog for Brill's XML Instructions

Below an overview of the most important changes added. This page also has a list of known issues and open questions.

## Versions

### 3.0 (2021-02-11)

> - Number of refinements, corrections, and adjustments. Full list of changes available from weterings@brill.com. 

### 2.3 (2018-10-09)

> - Number of refinements, corrections, and adjustments. Full list of changes available from weterings@brill.com. 

### 2.2 (2018-04-11)

> - Number of refinements, corrections, and adjustments. Full list of changes available from weterings@brill.com. 

### 2.1 (2017-08-01)

> - Range of minor corrections and improvements. Full list of changes available from weterings@brill.com.

### 2.0 (2017-04-26)

> - Rollout for official use. Full list of changes available from weterings@brill.com.

### 1.5 (2017-01-11)

> - Fixed issues #35, #36, #25

### 1.4 (2016-11-21)

> - Fixed open issues, added more explanation to tag usage.

### 1.3 (2016-08-18)

> - Added <a href="/sitemap/">Sitemap</a>

### 1.2 (2016-06-30)

> - Added `<!DOCTYPE>` declaration to JATS and BITS.
> - Adjusted `<title-group>` in BITS, book chapters or book parts.
> - Removed **Req.** `<contrib-group>` from JATS journal article, and BITS book chapter or book part.
> - Added new items to [To Do](http://brill.gitlab.io/changelog/#to-do) list.
> - Added list of Creative Commons licenses.
> - Clean up, small corrections.

### 1.1 (2016-06-25)

> - Small corrections added to JATS and BITS, see *To Do* list above.

### 1.0 (2016-05-13)

> - Publication of Brill's JATS and BITS XML Instructions.

## To Do

A full list of open issues is available from Brill upon request. Send an email to Tom Weterings (weterings@brill.com). The most important coming improvements are:

> - Instructions on the encoding of bi-directional text.
> - Adding header version instructions for both JATS and BITS.
> - Adding file naming conventions.
> - Adding list of LTWA abbreviations for use with JATS.
> - Adding TEI instructions.
> - Adding updated PDF instructions.
> - Adding updated image instructions.
> - Fixing links to nested book-part example and `@id` syntax.
> - Improved options for including funding information.
> - Checking and updating all examples.
