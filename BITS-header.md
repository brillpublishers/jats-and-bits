# Header-only BITS for Books

**PLEASE NOTE**: These instructions are only to be used for header-only BITS XML (so XML with only metadata). For full-text BITS, or intermediate forms, please refer to our [full-text BITS instructions](/bits).

> ## Req. `<!DOCTYPE>`
>
> Always add the following `<!DOCTYPE>` declaration to Brill books:
>
> `<!DOCTYPE book PUBLIC "-//NLM//DTD BITS Book Interchange DTD v2.0 20151225//EN" "https://jats.nlm.nih.gov/extensions/bits/2.0/BITS-book2.dtd">`
>
> ## Req. `<book>` [Example](/example-book)
>
> Top-level element for this DTD. A `book`, as defined in this DTD, covers a single work or book component such as a technical monograph, government report, volume of a monographic series, STM reference work, etc.
>
> ### Req. `@xmlns:xlink`
> XML namespace declaration. The value is always `"http://www.w3.org/1999/xlink"`.
>
> ### Req. `@xml:lang`
> Indicates the main language of the book. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>
> ### Req. `@dtd-version`
> This attribute is required and always contains `"2.0"`.
>
>> ## `<collection-meta>` [Example](/example-collections)
>>
>> Bibliographic metadata describing a book set or series to which this book or book part belongs.
>>
>> If a book belongs to a series *and* a sub-series, provide two `<collection-meta>` elements.
>>
>> ### Req. `@collection-type`
>> Type of series:
>>
>> - `"series"`
>> - `"sub-series"`
>>
>>> ## Req. `<collection-id>`
>>> The series acronym, to be provided by Brill.
>>>
>>> ### Req. `@collection-id-type`
>>> Brill supports 1 value:
>>>
>>> - `"publisher"`
>>
>>> ## Req. `<title-group>`
>>>
>>>> ## Req. `<title>`
>>>>
>>>> The (sub-)series’s title.
>>>
>>>> ## `<subtitle>`
>>>>
>>>> The (sub-series)’s subtitle, if supplied.
>>>
>>>> ## `<trans-title-group>` [Example](/example-trans-title-group)
>>>>
>>>> For every translation of a (sub-)series title, supply a `<trans-title-group>`.
>>>>
>>>> ### Req. `@xml:lang`
>>>> Indicates the language of the translated title. This attribute should be included here and *not* on the sub-elements. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>
>>>>> ## `<trans-title>`
>>>>>
>>>>> A translated title.
>>>>
>>>>> ## `<trans-subtitle>`
>>>>>
>>>>> A translated subtitle.
>>
>>> ## `<contrib-group>` (Series level)
>>>
>>> Grouping of one or more contributors and information about those contributors. All types of contributors are grouped in one `<contrib-group>`. Note that at most *one* `<contrib-group>` is allowed in `<collection-meta>`.
>>>
>>> On this `<collection-meta>`level, the `contrib-group` will be the editorial board.
>>>
>>>> ## `<contrib>`
>>>>
>>>> A contributor: Container element for information about a single author, editor, or other contributor.
>>>>
>>>> ### Req. `@contrib-type`
>>>>
>>>> Type of contribution made by the contributor. Brill supports the following contribution types for Series-level contributors:
>>>>
>>>> - `"series editor"` (use as **default** unless specifically instructed otherwise)
>>>> - `"editor-in-chief"`
>>>> - `"founding editor"`
>>>> - `"managing editor"`
>>>> - `"associate editor"`
>>>> - `"technical editor"`
>>>> - `"editorial board/council member"`
>>>> - `"advisory editor"`
>>>> - `"copy editor"`
>>>> - `"translator"`
>>>>
>>>>> ## `<collab>`
>>>>>
>>>>> A group of authors, but referred to only as a group, like a consortium or an institute, like the UN. If `<collab>` is used, `<name>` is not required.
>>>>>
>>>>
>>>>> ## Req. `<name>`
>>>>>
>>>>> Container element for components of personal names, such as a `<surname>`. 
>>>>>
>>>>> ### `@name-style`
>>>>>
>>>>> Style of processing requested for a structured personal name.
>>>>>
>>>>> - `"eastern"`: The name will both be displayed and sorted/inverted with the family name preceding the given name.
>>>>> - `"western"`: The single name provided is a given name, not a family name/surname. The single name will be both the sort and the display name.
>>>>>
>>>>> Default value is `"western"`. If an author has only a single name, `name-style="eastern"` and tag the single name with `<surname>`.
>>>>>
>>>>>> ## Req. `<surname>`
>>>>>>
>>>>>> Surname of a person.
>>>>>
>>>>>> ## `<given-names>`
>>>>>>
>>>>>> All given names of a person, such as the first name, middle names, maiden name if used as part of the married name, etc. This element will *not* be repeated for every given name a person has.
>>>>>
>>>>>> ## `<prefix>`
>>>>>>
>>>>>> Honorifics or other qualifiers that usually precede a person’s name (for example, Professor, Rev., President, Senator, Dr., Sir, The Honorable).
>>>>>
>>>>>> ## `<suffix>`
>>>>>>
>>>>>> Qualifiers that follow a person’s name (for example, Sr., Jr., III, 3rd).
>>>>
>>>>> ## `<xref>`
>>>>>
>>>>> A cross reference to an affiliation.
>>>>>
>>>>> ### Req. `@ref-type`
>>>>>
>>>>> Here, the type of reference is always `"aff"`.
>>>>>
>>>>> ### Req. `@rid`
>>>>>
>>>>> Contains the `@id` of affiliation this contributor is associated with. If the reference is to more than one element, add all IDs here separated by spaces.
>>>
>>>> ## `<aff>`
>>>>
>>>> ### Req. `@id` [See @id syntax](#<@id-syntax>)
>>>>
>>>> Unique identifier for the affiliation, which is used in conjunction with `<xref ref-type="aff" rid=""/>` to refer to this affiliation.
>>>>
>>>>> ## `<institution-wrap>`
>>>>>> ## Req.`<institution>`
>>>>>>
>>>>>> Name of an institution or organization, like `Universiteit Leiden` or `Universiteit van Amsterdam`.
>>>>>>
>>>>>> This information is provided by Brill, but it must be checked. This element can also contain the name of the department, like `Leiden University Centre for Linguistics`. Provide this value with a `@content-type="dept"`.
>>>>>>
>>>>>> ### `@content-type`
>>>>>>
>>>>>> Use this attribute to specify the extra institution information. Supported values:
>>>>>>
>>>>>> - `"dept"`
>>>>
>>>>> ## `<country>`
>>>>>
>>>>> The country the institution is located in.
>>
>>> ## `<volume-in-collection>`
>>>
>>>> ## `<volume-number>`
>>>>
>>>> The volume number of this book within the described collection. This is *not* about physically splitting a single book into multiple volumes.
>>
>>> ## `<issn>`
>>>
>>> The book series' ISSN (International Standard Serial Number, the international code that uniquely identifies a serial publication title.) Normally a book series has only one ISSN (the print version). Contents come from a Master List that Brill will provide typesetters. Note that the hyphen in an ISSN should **always** be included.
>>>
>>> ### Req. `@publication-format`
>>> Indicate whether this ISSN applies to a print or an online version.
>>>
>>> Supported values:
>>>
>>> - Req. `"print"`
>>
>>> ## `<publisher>`
>>>
>>> Information on the publisher associated with the series. **Note**: only to be used in `<collection-meta>` when specifically instructed by Brill (in which case the imprint here will be different from what is given in the `<book-meta>`). Please note that this element is always required in `<book-meta>` (see below).
>>>
>>>> ## Req. `<publisher-name>`
>>>
>>>> ## Req. `<publisher-loc>`
>>>>
>>>> If the publisher has more than one location (e.g., `Leiden | Boston`), this still has to specified in one element.
>>>>
>>>> ### `@specific-use`
>>>>
>>>> Supported values are:
>>>>
>>>> - `"online"`
>>>> - `"print"
>
>> ## Req. `<book-meta>` [Example](/example-book-header)
>>
>>> ## Req. `<book-id>`
>>>
>>> An unique identifier for the book. These are provided by your Brill Desk Editor.
>>>
>>> ### Req. `@book-id-type`
>>> The following value is required:
>>>
>>> - Req. `"doi"`
>>
>>> ## Req. `<book-title-group>`
>>>
>>>> ## Req. `<book-title>`
>>>>
>>>> The major title of the book, excluding label and any subtitles.
>>>
>>>> ## `<subtitle>`
>>>>
>>>> The subtitle of the book.
>>>
>>>> ## `<trans-title-group>` [Example](/example-trans-title-group)
>>>>
>>>> For every translation of the book title, supply a `<trans-title-group>`.
>>>>
>>>> ### Req. `@xml:lang`
>>>> Indicates the language of the translated title. This attribute should be included here and *not* on the sub-elements. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>
>>>>> ## `<trans-title>`
>>>>>
>>>>> A translated title.
>>>>
>>>>> ## `<trans-subtitle>`
>>>>>
>>>>> A translated subtitle.
>>>
>>>> ## `<alt-title>` [Example](/example-alt-title)
>>>>
>>>> An alternative or different version of the title of the book used for sorting.
>>>>
>>>> ### Req. `@alt-title-type`
>>>>
>>>> Reason or purpose for a (shorter) title to be included, for Brill this is always a sorting-title, e.g. `"The Beatles" → "Beatles"`.
>>>>
>>>> - `"sort-title"`: Sort-title + convention.
>>
>>> ## Req. `<contrib-group>` (Book level)
>>>
>>> Grouping of one or more contributors and information about those contributors. All types of contributors are grouped in one `<contrib-group>`. Note that at most *one* `<contrib-group>` is allowed in `<book-meta>`.
>>>
>>>> ## `<contrib>`
>>>>
>>>> A contributor: Container element for information about a single author, editor, or other contributor.
>>>>
>>>> If a contribution is without author, do not add `<contrib>`.
>>>>
>>>> ### Req. `@contrib-type`
>>>>
>>>> Type of contribution made by the contributor. Brill supports the following contribution types for Book-level contributors:
>>>>
>>>> - `"author"` (**default value**)
>>>> - `"volume editor"` (**default** in case of an edited volume)
>>>> - `"contributor"`
>>>> - `"advisor"`
>>>> - `"editor"` (used for Text Editions only)
>>>> - `"editor/translator"`
>>>> - `"translator"`
>>>> - `"copy editor"`
>>>>
>>>>> ## `<collab>`
>>>>>
>>>>> A group of authors, but referred to only as a group, like a consortium or an institute, like the UN. If `<collab>` is used, `<name>` is not required.
>>>>>
>>>>
>>>>> ## Req. `<name>`
>>>>>
>>>>> Container element for components of personal names, such as a `<surname>`. 
>>>>>
>>>>> ### `@name-style`
>>>>>
>>>>> Style of processing requested for a structured personal name.
>>>>>
>>>>> - `"eastern"`: The name will both be displayed and sorted/inverted with the family name preceding the given name.
>>>>> - `"western"`: The single name provided is a given name, not a family name/surname. The single name will be both the sort and the display name.
>>>>>
>>>>> Default value is `"western"`. If an author has only a single name, `name-style="eastern"` and tag the single name with `<surname>`.
>>>>>
>>>>>> ## Req. `<surname>`
>>>>>>
>>>>>> Surname of a person.
>>>>>
>>>>>> ## `<given-names>`
>>>>>>
>>>>>> All given names of a person, such as the first name, middle names, maiden name if used as part of the married name, etc. This element will NOT be repeated for every given name a person has.
>>>>>
>>>>>> ## `<prefix>`
>>>>>>
>>>>>> Honorifics or other qualifiers that usually precede a person’s name (for example, Professor, Rev., President, Senator, Dr., Sir, The Honorable).
>>>>>
>>>>>> ## `<suffix>`
>>>>>>
>>>>>> Qualifiers that follow a person’s name (for example, Sr., Jr., III, 3rd).
>>>>
>>>>
>>>>> ## `<email>`
>>>>>
>>>>> The contributor's email address.
>>>>>
>>>>> ### `@xlink:href`
>>>>> The e-mail address as a `mailto:` URI.
>>>>>
>>>>
>>>>> ## `<xref/>`
>>>>>
>>>>> A cross reference to an affiliation.
>>>>>
>>>>> ### Req. `@ref-type`
>>>>>
>>>>> Here, the type of reference is always `"aff"`.
>>>>>
>>>>> ### Req. `@rid`
>>>>>
>>>>> Contains the `@id` of affiliation this contributor is associated with. If the reference is to more than one element, add all IDs here separated by spaces.
>>>
>>>> ## `<aff>`
>>>>
>>>> ### Req. `@id` [See @id syntax](#<@id-syntax>)
>>>>
>>>> Unique identifier for the affiliation, which is used in conjunction with `<xref ref-type="aff" rid=""/>` to refer to this affiliation.
>>>>
>>>>> ## `<institution-wrap>`
>>>>>> ## Req.`<institution>`
>>>>>>
>>>>>> Name of an institution or organization, like `Universiteit Leiden` or `Universiteit van Amsterdam`.
>>>>>>
>>>>>> This information is provided by Brill, but must be checked. This element can also contain the name of the department, like `Leiden University Centre for Linguistics`. Provide this value with a `@content-type="dept"`.
>>>>>>
>>>>>> ### `@content-type`
>>>>>>
>>>>>> Use this attribute to specify the extra institution information. Supported values:
>>>>>>
>>>>>> - `"dept"`
>>>>>
>>>>>> ## `<institution-id>`
>>>>>>
>>>>>> A externally defined institutional identifier, from an established identifying authority (for example, “Ringgold”).
>>>>>>
>>>>
>>>>> ## Req. `<country>`
>>>>>
>>>>> The country the institution is located in. 
>>>>
>>
>>> ## Req. `<pub-date>`
>>>
>>>> ### `@publication-format`
>>>>
>>>> Supported value:
>>>>
>>>> - Req. `"online"`
>>>>
>>>>> ## `<day>`
>>>>>
>>>>> A day, in (zero-padded) digits.
>>>>
>>>>> ## `<month>`
>>>>>
>>>>> A month, in (zero-padded) digits.
>>>>
>>>>> ## Req. `<year>`
>>>>>
>>>>> A year, in (zero-padded) digits.
>>
>>> ## `<book-volume-number>`
>>>
>>> The volume number of the book, when a single narrative is split into multiple volumes.
>>
>>> ## Req. `<isbn>`
>>>
>>> International Standard Book Number, the international code for identifying a particular product form or edition of a publication, typically a monographic publication.
>>> The code is always set without spaces or hyphens.
>>>
>>> ### Req. `@publication-format`
>>>
>>> Indicate whether this ISBN applies to an online or a print version. Supported values:
>>>
>>> - Req. `"online"`
>>> - `"print"`
>>>
>>> In case of multiple print versions with separate ISBNs, the following values are supported instead of `"print"`:
>>>
>>> - `"hardback"`
>>> - `"paperback"`
>>
>>> ## Req. `<publisher>`
>>>
>>>> ## Req. `<publisher-name>`
>>>>
>>>> Name of the imprint associated with the article or book. Brill will let the typesetters know which imprint to use for the book.
>>>
>>>> ## Req. `<publisher-loc>`
>>>>
>>>> Use the location as given on the Title Page. If the publisher has more than one location (e.g., `Leiden | Boston`), this still has to specified in one element.
>>>>
>>>> ### `@specific-use`
>>>>
>>>> Supported values are:
>>>>
>>>> - `"online"`
>>>> - `"print"`
>>
>>> ## `<edition>`
>>>
>>> The full edition statement for a document, book part, collection, event, or for a cited or referenced publication.
>>
>>> ## Req. `<permissions>` [Example](/example-permissions)
>>>
>>>> ## Req. `<copyright-statement>`
>>>>
>>>> Copyright notice or statement, suitable for printing or display.
>>>
>>>> ## Req. `<copyright-year>`
>>>>
>>>> The year of copyright. This may not be displayed.
>>>
>>>> ## Req. `<copyright-holder>`
>>>>
>>>> Name of the organizational or person that holds a copyright.
>>>
>>>> ## Req. `<license>`
>>>>
>>>> A license. Set of conditions under which the content may be used, accessed, and distributed.
>>>>
>>>> ### `@license-type`
>>>>
>>>> - `"ccc"`. Standard Brill license type. See example for full text.
>>>> - `"open-access"`. Any Open Access license. If used, `@xlink:href` and `@xlink:title` are Required.
>>>>
>>>> ### `@xlink:href`
>>>> The URL at which the license text can be found. See [Creative Commons Licenses](https://creativecommons.org/licenses/).
>>>>
>>>> ### `@xlink:title`
>>>> If used with a Creative Commons license. Corresponds to `@xlink:href`. Supported values:
>>>>
>>>> - `"CC-BY"`
>>>> - `"CC-BY-SA"`
>>>> - `"CC-BY-ND"`
>>>> - `"CC-BY-NC"`
>>>> - `"CC-BY-NC-SA"`
>>>> - `"CC-BY-NC-ND"`
>>>>
>>>>> ## Req. `<license-p>`
>>>>>
>>>>> Paragraph of text within the description of a `<license>`.
>>>
>>>> ## `<ali:free_to_read>`
>>>>
>>>> Indicates any free-access license. Only to be used on specific instruction from Brill.
>>>>
>>>> ### Req. `@xmlns:ali="http://www.niso.org/schemas/ali/1.0/"`
>>>>
>>>> ### `@start_date`
>>>>
>>>> Indicates the date on which the free-access license should start. Value should use the following format: "yyyy-mm-dd".
>>>>
>>>> ### `@end_date`
>>>>
>>>> Indicates the date on which the free-access license should end. Value should use the following format: "yyyy-mm-dd".
>>>> The absence of both start_date and end_date dates indicates a permanent free-to-read status.
>>
>>> ## Req. `<self-uri/>` (Book Level)
>>> The URI for the (online) PDF-version of the book. Do not add this tag if there is no corresponding PDF file.
>>>
>>> This empty element encloses a link to the PDF.
>>>
>>> ### `@content-type`
>>>
>>> For Brill, this is always:
>>>
>>> - `"PDF"`
>>>
>>> ### `@xlink:href`
>>> The URL at which the PDF can be found.
>>
>>> ## Req. `<counts>`
>>>
>>> Container element for counts of a document (for example, number of tables, number of words).
>>>
>>>> ## Req. `<book-page-count>`
>>>>
>>>> ### Req. `@count`
>>>>
>>>> Whereas the content can indicate a page-amount akin to `xii + 351`, in `@count` this must be a numerical total page count.
>>
>>> ## Req. `<custom-meta-group>`  [Example](/example-custom-meta)
>>>
>>> Container element for metadata not otherwise defined in the Tag Suite.
>>> Brill requires *header files* &ndash; XML files that do not contain body content &ndash; to have the `version: header` key-value pair. Brill requires *full content files* &ndash; XML files that contain both header and body content and back matter &ndash; to have the `version: fulltext` key-value pair.
>>>
>>>> ## Req. `<custom-meta>`
>>>>> ## Req. `<meta-name>`
>>>>>
>>>>> A custom metadata name (or key). Supported value:
>>>>> - `"version"`
>>>>
>>>>> ## Req. `<meta-value>`
>>>>>
>>>>> A custom metadata value. Supported value for header-only BITS:
>>>>> - `"header"`
>
>> ## `<book-body>` [Example](/example-book-body)
>>
>>> ## `<book-part>`
>>>
>>> A major organizational unit of a book, typically called a “chapter”, but given many names in common language, for example, chapter, part, unit, module, section, topic, lesson, canto, volume, or even “book”.
>>> This includes parts (Part 1, Part 2, …), introductions and other chapter-like content.
>>> Book Parts are recursive, that is, various levels of `<book-part>` are indicated by containment, not by different names for the interior parts. The body of a book part (`<body>`) may contain lower level `<book-part>` elements or just recursive sections that are tagged using the `<sec>` element. Note that book parts should be nested if relevant - so for example, if a book has several "Parts", and Part 1 includes Chapters 1 and 2, then `<book-part>` "Chapter 1" and `<book-part>` "Chapter 2" should be nested in `<book-part>` "Part 1". [Example](http://brill.gitlab.io/example-nested-book-part/)
>>>
>>> N.B. If the `<book-part>` is just a "Part" page (so only a title page serving as a top-level divider in the book, with the actual "Chapters" below it), then this "Part" page should *not* be included in the following Chapter's PDF. The "Part" `<book-part>` should also *not* receive a `<self-uri/>` to a PDF, if said PDF only consists of one page with just the "Part" title on it.
>>>
>>> ### `@xml:lang`
>>>
>>> Indicates this book-part is in another language than the main language of the book. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>
>>> ### Req. `@book-part-type`
>>>
>>> Examples may include:
>>>
>>> - `"part"`
>>> - `"chapter"`
>>> - `"section"`
>>> - `"volume"`
>>>
>>> ### Req. `@id` [See @id syntax](#<@id-syntax>)
>>>
>>> The `@id` attribute supplies a document-internal unique reference to an element. Note that an `@id` is *always* required for a `<book-part>`
>>>
>>> ### Req. `@seq`
>>>
>>> A sequential number starting at 1 per book-body that increments for each book-part in it. This sorts the book-parts in the book-body.
>>>
>>>> ## `<book-part-meta>`
>>>>
>>>>> ## Req. `<book-part-id>`
>>>>>
>>>>> Unique external identifier assigned to a book-part.
>>>>>
>>>>> ### Req. `@book-part-id-type`
>>>>>
>>>>> Type of publication identifier or the organization or system that defined the identifier.
>>>>>
>>>>> - Req. `"doi"`
>>>>
>>>>> ## Req. `<title-group>`
>>>>>
>>>>>> ## `<label>`
>>>>>>
>>>>>> Label (of an Equation, Figure, Reference, etc.), zero or one
>>>>>
>>>>>> ## Req. `<title>`
>>>>>>
>>>>>> Full title of a book chapter, or book part.
>>>>>
>>>>>> ## `<subtitle>`
>>>>>>
>>>>>> Subordinate part of a title for a document or document component. Refer to the manuscript to see what the author meant as subtitle.
>>>>>
>>>>>> ## `<trans-title-group>`
>>>>>>
>>>>>> For every translation of the title, supply a `<trans-title-group>`.
>>>>>>
>>>>>> ### Req. `@xml:lang`
>>>>>>
>>>>>> Indicates the language of the translated title. This attribute should be included here and *not* on the sub-elements. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>>>
>>>>>>> ## `<trans-title>`
>>>>>>>
>>>>>>> The translation of the title.
>>>>>>
>>>>>>> ## `<trans-subtitle>`
>>>>>>>
>>>>>>> The translation of the subtitle.
>>>>>>
>>>>>> ## `<alt-title>`
>>>>>>
>>>>>> An alternative or different version of the title of a work.
>>>>>>
>>>>>> ### `@alt-title-type`
>>>>>>
>>>>>> Reason or purpose for a (shorter) title to be included. Brill supports 2 values for `@alt-title-type`:
>>>>>>
>>>>>> - `"toc"`: Title to be used in the table of contents.
>>>>>> - `"running-head"`: Title to be used as the running head. This is a shorter version of the regular title. An `<alt-title>` with this type is optional.
>>>>
>>>>> ## `<contrib-group>` (Chapter/Part level)
>>>>>
>>>>> Grouping of one or more contributors and information about those contributors. All types of contributors are grouped in one `<contrib-group>`. Do not add this tag group if a chapter does not have an author, like for instance the *Prelims* or *Indices* of a book. Note that at most *one* `<contrib-group>` is allowed per `<book-part-meta>`.
>>>>>
>>>>>> ## Req. `<contrib>`
>>>>>>
>>>>>> A contributor: Container element for information about a single author, editor, or other contributor.
>>>>>>
>>>>>> If a contribution is without author, do not add `<contrib>`.
>>>>>>
>>>>>> ### Req. `@contrib-type`
>>>>>>
>>>>>> Type of contribution made by the contributor. Brill supports the following contribution types for Chapter-level contributors:
>>>>>>
>>>>>> - `"author"`
>>>>>> - `"editor"`
>>>>>> - `"editor/translator"`
>>>>>> - `"translator"`
>>>>>>
>>>>>>> ## `<collab>`
>>>>>>>
>>>>>>> A group of authors, but referred to only as a group, like a consortium or an institute, like the UN. If `<collab>` is used, `<name>` is not required.
>>>>>>>
>>>>>>
>>>>>>> ## Req. `<name>`
>>>>>>>
>>>>>>> Container element for components of personal names, such as a `<surname>`. 
>>>>>>>
>>>>>>> ### `@name-style`
>>>>>>>
>>>>>>> Style of processing requested for a structured personal name.
>>>>>>>
>>>>>>> - `"eastern"`: The name will both be displayed and sorted/inverted with the family name preceding the given name.
>>>>>>> - `"western"`: The single name provided is a given name, not a family name/surname. The single name will be both the sort and the display name.
>>>>>>>
>>>>>>> Default value is `"western"`. If an author has only a single name, `name-style="eastern"` and tag the single name with `<surname>`.
>>>>>>>
>>>>>>>> ## Req. `<surname>`
>>>>>>>>
>>>>>>>> Surname of a person.
>>>>>>>
>>>>>>>> ## `<given-names>`
>>>>>>>>
>>>>>>>> All given names of a person, such as the first name, middle names, maiden name if used as part of the married name, etc. This element will NOT be repeated for every given name a person has.
>>>>>>>
>>>>>>>> ## `<prefix>`
>>>>>>>>
>>>>>>>> Honorifics or other qualifiers that usually precede a person’s name (for example, Professor, Rev., President, Senator, Dr., Sir, The Honorable).
>>>>>>>
>>>>>>>> ## `<suffix>`
>>>>>>>>
>>>>>>>> Qualifiers that follow a person’s name (for example, Sr., Jr., III, 3rd).
>>>>>>
>>>>>>> ## `<email>`
>>>>>>>
>>>>>>> The contributor's email address.
>>>>>>>
>>>>>>> ### `@xlink:href`
>>>>>>> The e-mail address as a `mailto:` URI.
>>>>>>
>>>>>>> ## `<xref/>`
>>>>>>>
>>>>>>> A cross reference to an affiliation or to a footnote.
>>>>>>>
>>>>>>> ### Req. `@ref-type`
>>>>>>>
>>>>>>> Supported values:
>>>>>>>
>>>>>>> - `"aff"` 
>>>>>>> - `"fn"`
>>>>>>>
>>>>>>> ### Req. `@rid`
>>>>>>>
>>>>>>> Contains the `@id` of the affiliation this contributor is associated with, or of the associated author-notes. If the reference is to more than one element, add all IDs here separated by spaces.
>>>>>
>>>>>> ## `<aff>`
>>>>>>
>>>>>> ### Req. `@id` [See @id syntax](#<@id-syntax>)
>>>>>>
>>>>>> Unique identifier for the affiliation, which is used in conjunction with `<xref ref-type="aff" rid=""/>` to refer to this affiliation.
>>>>>>
>>>>>>> ## `<institution-wrap>`
>>>>>>>> ## Req.`<institution>`
>>>>>>>>
>>>>>>>> Name of an institution or organization, like `Universiteit Leiden` or `Universiteit van Amsterdam`.
>>>>>>>>
>>>>>>>> This element can also contain the name of the department, like `Leiden University Centre for Linguistics`. Provide this value with a `@content-type="dept"`.
>>>>>>>>
>>>>>>>> ### `@content-type`
>>>>>>>>
>>>>>>>> Use this attribute to specify the extra institution information. Supported values:
>>>>>>>>
>>>>>>>> - `"dept"`
>>>>>>>
>>>>>>> ## Req. `<country>`
>>>>>>>
>>>>>>> The country the institution is located in. This is represented separately from an `<addr-line>`.
>>>>>>
>>>>>>> ## `<addr-line>`
>>>>>>>
>>>>>>> An text line in an address.
>>>>>>>
>>>>>>> ### `@content-type`
>>>>>>>
>>>>>>> Indicates what type of address line information it is. Supported values include:
>>>>>>>
>>>>>>> - `"city"`
>>>>>>> - `"zipcode"`
>>>>
>>>>> ## Req. `<fpage>`
>>>>>
>>>>> Starting *(first)* page number of the article.
>>>>>
>>>>> ### Req. `@specific-use`
>>>>>
>>>>> This attribute’s value is always `"PDF"`.
>>>>>
>>>>> ### Req. `@seq`
>>>>>
>>>>> A sequential number starting at 1 per book that increments for each chapter or part in it. This sorts the chapters in a book.
>>>>
>>>>> ## Req. `<lpage>`
>>>>>
>>>>> Final *(last)* page number of a book part.
>>>>
>>>>> ## `<permissions>` [Example](/example-permissions)
>>>>>
>>>>> *Only* add this element if the license is different than the superordinate `<book>` as it could be in mixed-license edited
>>>>> volumes with Open Access chapters.
>>>>>
>>>>>> ## Req. `<copyright-statement>`
>>>>>>
>>>>>> Copyright notice or statement, suitable for printing or display.
>>>>>
>>>>>> ## Req. `<copyright-year>`
>>>>>>
>>>>>> The year of copyright. This may not be displayed.
>>>>>
>>>>>> ## Req. `<copyright-holder>`
>>>>>>
>>>>>> Name of the organizational or person that holds a copyright.
>>>>>
>>>>>> ## Req. `<license>`
>>>>>>
>>>>>> A license. Set of conditions under which the content may be used, accessed, and distributed.
>>>>>>
>>>>>> *Only* add this element if chapter is Open Access.
>>>>>>
>>>>>> ### `@license-type`
>>>>>>
>>>>>> - `"open-access"`. Any Open Access license. If used, `@xlink:href` and `@xlink:title` are Required.
>>>>>>
>>>>>> ### `@xlink:href`
>>>>>> The URL at which the license text can be found. See [Creative Commons Licenses](https://creativecommons.org/licenses/).
>>>>>>
>>>>>> ### `@xlink:title`
>>>>>> If used with a Creative Commons license. Corresponds to `@xlink:href`. Supported values:
>>>>>>
>>>>>> - `"CC-BY"`
>>>>>> - `"CC-BY-SA"`
>>>>>> - `"CC-BY-ND"`
>>>>>> - `"CC-BY-NC"`
>>>>>> - `"CC-BY-NC-SA"`
>>>>>> - `"CC-BY-NC-ND"`
>>>>>>
>>>>>>> ## Req. `<license-p>`
>>>>>>>
>>>>>>> Paragraph of text within the description of a `<license>`.
>>>>>
>>>>>> ## `<ali:free_to_read>`
>>>>>>
>>>>>> Indicates any free-access license. Only to be used on specific instruction from Brill.
>>>>>>
>>>>>> ### Req. `@xmlns:ali="http://www.niso.org/schemas/ali/1.0/"`
>>>>>>
>>>>>> ### `@start_date`
>>>>>>
>>>>>> Indicates the date on which the free-access license should start. Value should use the following format: "yyyy-mm-dd".
>>>>>>
>>>>>> ### `@end_date`
>>>>>>
>>>>>> Indicates the date on which the free-access license should end. Value should use the following format: "yyyy-mm-dd".
>>>>>> The absence of both start_date and end_date dates indicates a permanent free-to-read status.
>>>>
>>>>> ## `<self-uri/>` (Chapter or Part)
>>>>> The URI for the (online) PDF-version of the chapter or part. Do not add this tag if there is no corresponding PDF file.
>>>>>
>>>>> This empty element encloses a link to the PDF.
>>>>>
>>>>> ### `@content-type`
>>>>>
>>>>> For Brill, this is always:
>>>>>
>>>>> - `"PDF"`
>>>>>
>>>>> ### `@xlink:href`
>>>>> The URL at which the PDF can be found.
>>>>
>>>>> ## `<abstract>`
>>>>> The book chapter abstract, so a summarized description of the content.
>>>>>
>>>>>> ## Req. `<title>`
>>>>>>
>>>>>> The title for the abstract. Usually, this is `"Abstract"`, but it can also be `"Samenvatting"`, `"Summary"` or `"Σύνοψις"`. The Brill Desk Editor will provide you with a title if it is different than `"Abstract"`. Do not add style elements!
>>>>>
>>>>>> ## Req. `<p>` [See Paragraph](#paragraph)
>>>>
>>>>> ## `<trans-abstract>`
>>>>> An abstract in another language than the main language of the `<book-part>`.
>>>>>
>>>>> ### Req. `@xml:lang`
>>>>>
>>>>> Indicates the language on a translated abstract in another language than the main language of the `<book-part>`. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>>
>>>>>> ## Req. `<p>` [See Paragraph](#paragraph)
>>>>
>>>>> ## `<kwd-group>`
>>>>>
>>>>> Keyword group. Container element for one set of keywords (such as `<kwd>`s) used to describe a document.
>>>>>
>>>>> ### Req. `@kwd-group-type`
>>>>>
>>>>> Indicates the type of keyword group, supported values:
>>>>>
>>>>> - `"uncontrolled"`
>>>>>
>>>>> In future versions, systems like the *US Library of Congress Subject Headings* may be used.
>>>>>
>>>>> ### `@xml:lang`
>>>>>
>>>>> Indicates a keyword grouping in another language than the main language of the `<book-part>`. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>>
>>>>>> ## Req. `<title>`
>>>>>>
>>>>>> The title for the keyword group. Usually, this is `"Keywords"`; the Brill Desk Editor will provide you with a title if it is different than `"Keywords"`. Do not add style elements!
>>>>>
>>>>>> ## Req. `<kwd>`
>>>>>>
>>>>>> Keyword, like a subject term, key phrase, indexing word etc. The author will supply this information.
>>>>
>>>>
>>>>> ## Req. `<counts>`
>>>>>
>>>>> Container element for counts of a document (for example, number of tables, number of words).
>>>>>
>>>>>> ## Req. `<book-page-count>`
>>>>>>
>>>>>> ### Req. `@count`
>>>>>>
>>>>>> Whereas the content can indicate a page-amount akin to `xii + 351`, in `@count` this must be a numerical total page count.
>>>

# Paragraphs

Paragraphs are `<p>` tags, with similar semantics as the HTML `<p>` tag.
Due to footnotes’ JATS/BITS design, `<p>` tags may contain anything a
`<sec>` may, but please refrain from adding anything but markup tags in
running text.

Do not use `<bold>`, `<italic>` or `<sc>` etc. on places where the Brill Style
supplies its use. Those usages are not semantic uses of small caps style,
i.e. do not supply `<sc>` Small Caps in a `<caption>` Caption element, as
captions—per Brill Typographic Style—are always set in small caps.

> ## `<p>`
>
> A text paragraph.
>
>> ## `<bold>`
>>
>> Font bold weight.
>
>> ## `<italic>`
>>
>> Font italic style.
>
>> ## `<sc>`
>>
>> Small Caps. Note that any text surrounded by `<sc>` tags should be included in their proper case (as if they weren't small caps): i.e., any capitals should be included as capitals, *not* lower case, while proper lower case should be included as such. Note also that as per Brill Typographic Style, `<sc>` should *not* be used at all for mixed-case abbreviations.
>
>> ## `<ext-link>`
>>
>> Link to an external file or resource.
>>
>> ### `@xlink:href`
>>
>> The URI where the resource can be found.


# @id syntax

> Each `@id` is unique within the XML document. Brill has defined the following `@id` syntax:
> 
> Pre-determined capital letter(s) + six numerical characters. 
> 
> E.g., `<book-part id="BP000001">`
> 
> Note that numbering does not need to be consecutive, just that entire `@id` needs to be unique within the XML document.
> 
> The following pre-determined `@id` capital letters are supported for header-only BITS:
> 
> - AFF : `<aff>`
> - BP : `<book-part>` 
