Table of contents

* [Introduction](README.md)
* [JATS](jats.md)
* [BITS](bits.md)
* [Header-only BITS](BITS-header.md)
* [Examples](examples.md)
* [Site map](sitemap.md)
* [Contributors](contributors.md)
* [Changelog](changelog.md)
* [Versions](versions.md)
