# Sitemap

## Sitemap and quick Table of Contents for Brill's XML Instructions 


Click on the links to quickly go to a section of the [JATS](https://brillpublishers.gitlab.io/jats-and-bits/jats) Journal or [BITS](https://brillpublishers.gitlab.io/jats-and-bits/bits) Book XML Instructions. 

## JATS Table of Contents

> `<!DOCTYPE>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/jats#req-doctype)

> `<article-meta>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/jats#req-article-meta)

> `<contrib-group>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/jats#contrib-group)

> `<pub-date>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/jats#req-pub-date)

> `<permissions>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/jats#req-permissions-example)

> `<body>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/jats#body-example)

> `<p>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/jats#paragraphs)

> `<fn-group>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/jats#fn-group)

> `<fig>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/jats#figures)

> `<ref-list>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/jats#reference-list)

## BITS Table of Contents

> `<!DOCTYPE>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/bits#req-doctype)

> `<book-meta>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/bits#req-book-part-meta)

> `<contrib-group>` : [Book Series](https://brillpublishers.gitlab.io/jats-and-bits/bits#contrib-group-series-level) - [Book](https://brillpublishers.gitlab.io/jats-and-bits/bits#req-contrib-group-book-level) - [Chapter/Part](https://brillpublishers.gitlab.io/jats-and-bits/bits#contrib-group-chapter-or-part)

> `<pub-date>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/bits#req-pub-date)

> `<permissions>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/bits#permissions-example)

> `<self-uri>` : [Book](https://brillpublishers.gitlab.io/jats-and-bits/bits#self-uri-book-level) - [Chapter/Part](https://brillpublishers.gitlab.io/jats-and-bits/bits#req-self-uri-chapter-or-part)

> `<book-part>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/bits#req-book-part-meta)

> `<body>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/bits#book-body-example)

> `<p>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/bits#paragraphs)

> `<fn-group>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/bits#fn-group)

> `<fig>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/bits#figures)

> `<ref-list>` : [link](https://brillpublishers.gitlab.io/jats-and-bits/bits#reference-list)
