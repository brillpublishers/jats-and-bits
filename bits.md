# BITS for Books

A book is built up from the top with a `<book>` element, that contains the book's metadata and subsidiary chapters. The model is extensive, to allow for edited volumes and monographs alike to be tagged in this one tag set. Each chapter is a `<book-part>`, just like the Parts, Sections that you see in the TOC.

See [jats.nlm.nih.org](http://jats.nlm.nih.gov/extensions/bits/tag-library/2.0/index.html) for NLM’s own description of the BITS standard. Their description is normative. This document describes a subset to help uniformity in Brill Book and Book Chapter XML.
 
The inside of a `<book-part>` is almost identical to a journal article’s body, with only some extra elements that have no place in journals.

> ## Req. `<!DOCTYPE>`
>
> Always add the following `<!DOCTYPE>` declaration to Brill books:
>
> `<!DOCTYPE book PUBLIC "-//NLM//DTD BITS Book Interchange DTD v2.0 20151225//EN" "https://jats.nlm.nih.gov/extensions/bits/2.0/BITS-book2.dtd">`
>
> ## Req. `<book>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#Book-header-example)
>
> Top-level element for this DTD. A `book`, as defined in this DTD, covers a single work or book component such as a technical monograph, government report, volume of a monographic series, STM reference work, etc.
>
> ### Req. `@xmlns:xlink`
> XML namespace declaration.
>
> ### `@xmlns:mml`
> XML namespace declaration. Use these *only* if the article contains an element from the MathML namespaces. Brill uses MathML3: [MathML](https://www.w3.org/TR/MathML3/).
>
> - `xmlns:mml="http://www.w3.org/1998/Math/MathML"`
>
> ### Req. `@xml:lang`
> Indicates the main language of the book. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>
> ### Req. `@dtd-version`
> This attribute is required and always contains `"2.0"`.
>
>> ## `<collection-meta>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#Collections-Example)
>>
>> Bibliographic metadata describing a book set or series to which this book or book part belongs.
>>
>> If a book belongs to a series *and* a sub-series, provide two `<collection-meta>` elements.
>>
>> ### Req. `@collection-type`
>> Type of series:
>>
>> - `"series"`
>> - `"sub-series"`
>>
>>> ## Req. `<collection-id>`
>>> An Brill identifier, that your Brill Desk Editor will supply.
>>>
>>> ### Req. `@collection-id-type`
>>> Brill supports 1 value:
>>>
>>> - `"publisher"`
>>
>>> ## Req. `<title-group>`
>>>
>>>> ## Req. `<title>`
>>>>
>>>> The (sub-)series’s title.
>>>
>>>> ## `<subtitle>`
>>>>
>>>> The (sub-series)’s subtitle, if supplied.
>>>
>>>> ## `<trans-title-group>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#trans-title-group-example)
>>>>
>>>> For every translation of a (sub-)series title, supply a `<trans-title-group>`.
>>>>
>>>> ### Req. `@xml:lang`
>>>> Indicates the language of the translated title. This attribute should be included here and *not* on the sub-elements. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>
>>>>> ## `<trans-title>`
>>>>>
>>>>> A translated title.
>>>>
>>>>> ## `<trans-subtitle>`
>>>>>
>>>>> A translated subtitle.
>>
>>> ## `<contrib-group>` (Series level)
>>>
>>> Grouping of one or more contributors and information about those contributors. All types of contributors are grouped in one `<contrib-group>`. Note that at most *one* `<contrib-group>` is allowed in `<collection-meta>`.
>>>
>>> On this `<collection-meta>`level, the `contrib-group` will be the editorial board.
>>>
>>>> ## `<contrib>`
>>>>
>>>> A contributor: Container element for information about a single author, editor, or other contributor.
>>>>
>>>> ### Req. `@contrib-type`
>>>>
>>>> Type of contribution made by the contributor. Brill supports the following contribution types for Series-level contributors:
>>>>
>>>> - `"series editor"` (default, unless specifically identified as one of the other types on the page/by the Brill Desk Editor)
>>>> - `"editor-in-chief"`
>>>> - `"founding editor"`
>>>> - `"managing editor"`
>>>> - `"associate editor"`
>>>> - `"technical editor"`
>>>> - `"editorial board/council member"`
>>>> - `"advisory editor"`
>>>> - `"copy editor"`
>>>> - `"translator"`
>>>>
>>>> ### `@deceased`
>>>>
>>>> Use if the contributor was deceased when the document was published. Supported values:
>>>>
>>>> - `"yes"`
>>>> - `"no"` Default value, which makes this attributes unnecessary.
>>>>
>>>> ### `@equal-contrib`
>>>>
>>>> Indicates whether or not all contributors contributed equally. If the contributor whom this attribute modifies contributed equally with all other contributors, this attribute should be set to “yes”; if his/her contribution was greater or lesser, then this attribute should be set to “no”.
>>>>
>>>> - `"yes"`
>>>> - `"no"`
>>>> @equal-contrib is an optional attribute; there is no default.
>>>>
>>>>> ## `<contrib-id>`
>>>>>
>>>>> An ORCID identifier for a contributor. Note that an ORCID identifier should include the complete URL, for example: https://orcid.org/0000-0002-1825-0097. Value must match regex pattern https://orcid.org/[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}[X0-9]{1}.
>>>>>
>>>>> ### Req. `@contrib-id-type`
>>>>>
>>>>> - `"orcid"`
>>>>>
>>>>> ### Req. `@authenticated`
>>>>>
>>>>> - `"true"`
>>>>
>>>>> ## `<collab>`
>>>>>
>>>>> A group of authors, but referred to only as a group, like a consortium or an institute, like the UN. If `<collab>` is used, `<name>` is not required.
>>>>>
>>>>
>>>>> ## Req. `<name>`
>>>>>
>>>>> Container element for components of personal names, such as a `<surname>`. Use `<name-alternatives>` if the name has multiple variants (e.g., in different scripts). 
>>>>>
>>>>> ### `@name-style`
>>>>>
>>>>> Style of processing requested for a structured personal name.
>>>>>
>>>>> - `"eastern"`: The name will both be displayed and sorted/inverted with the family name preceding the given name.
>>>>> - `"western"`: The name will be displayed with the given name preceding the family name, but will be sorted/inverted with the family name preceding the given name.
>>>>> - `"given-only"`: The single name provided is a given name, not a family name/surname. The single name will be both the sort and the display name.
>>>>>
>>>>> Default value is `"western"`. If an author has only a single name, `name-style="given-only"` and tag the single name with `<given-names>`.
>>>>>
>>>>>> ## Req. `<surname>`
>>>>>>
>>>>>> Surname of a person.
>>>>>
>>>>>> ## `<given-names>`
>>>>>>
>>>>>> All given names of a person, such as the first name, middle names, maiden name if used as part of the married name, etc. This element will *not* be repeated for every given name a person has.
>>>>>
>>>>>> ## `<prefix>`
>>>>>>
>>>>>> Honorifics or other qualifiers that usually precede a person’s name (for example, Professor, Rev., President, Senator, Dr., Sir, The Honorable).
>>>>>
>>>>>> ## `<suffix>`
>>>>>>
>>>>>> Qualifiers that follow a person’s name (for example, Sr., Jr., III, 3rd).
>>>>
>>>>> ## `<name-alternatives>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#String-name-Example)
>>>>>
>>>>> If a contributor’s name has multiple variants, use this element to include *all* variants. Put the variants in the same order as they would be on the printed page.
>>>>>
>>>>> The contents of this element are identical to the above description of `<name>`.
>>>>>
>>>>>> ## `<name>`
>>>>>>
>>>>>> ### `@xml:lang`
>>>>>>
>>>>>> Indicates the language of the name, if in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>>>
>>>>>>> ## `<surname>`
>>>>>>
>>>>>>> ## `<given-names>`
>>>>>>
>>>>>>> ## `<prefix>`
>>>>>>
>>>>>>> ## `<suffix>`
>>>>>
>>>>>> ## `<string-name>`
>>>>>>
>>>>>> ### `@xml:lang`
>>>>>>
>>>>>> Indicates the language of the name, if in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>
>>>>> ## `<xref>`
>>>>>
>>>>> A cross reference to an affiliation.
>>>>>
>>>>> ### Req. `@ref-type`
>>>>>
>>>>> Here, the type of reference is always `"aff"`.
>>>>>
>>>>> ### Req. `@rid`
>>>>>
>>>>> Contains the `@id` of affiliation this contributor is associated with. If the reference is to more than one element, add all IDs here separated by spaces.
>>>
>>>> ## `<aff>`
>>>>
>>>> Element used to include an affiliation. Note that for every new affiliation, a separate `<aff>` element should be included.
>>>>
>>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>>
>>>> Unique identifier for the affiliation, which is used in conjunction with `<xref ref-type="aff" rid=""/>` to refer to this affiliation.
>>>>
>>>>> ## `<institution-wrap>`
>>>>>> ## Req.`<institution>`
>>>>>>
>>>>>> Name of an institution or organization, like `Universiteit Leiden` or `Universiteit van Amsterdam`.
>>>>>>
>>>>>> This information is provided by Brill, but it must be checked. This element can also contain the name of the department, like `Leiden University Centre for Linguistics`. Provide this value with a `@content-type="dept"`.
>>>>>>
>>>>>> ### `@content-type`
>>>>>>
>>>>>> Use this attribute to specify the extra institution information. Supported values:
>>>>>>
>>>>>> - `"dept"`
>>>>>
>>>>>> ## `<institution-id>`
>>>>>>
>>>>>> A externally defined institutional identifier, from an established identifying authority (for example, “Ringgold”).
>>>>>>
>>>>>> ### Req. `@institution-id-type`
>>>>>>
>>>>>> Brill only supports Ringgold identifiers. Supported values:
>>>>>>
>>>>>> - `"ringgold"`
>>>>
>>>>> ## `<country>`
>>>>>
>>>>> The country the institution is located in. This is represented separately from an `<addr-line>`.
>>
>>> ## `<volume-in-collection>`
>>>
>>>> ## `<volume-number>`
>>>>
>>>> The volume number of this book within the described collection. This is *not* about physically splitting a single book into multiple volumes.
>>
>>> ## `<issn>`
>>>
>>> The book series' ISSN (International Standard Serial Number, the international code that uniquely identifies a serial publication title.) Normally a book series has only one ISSN (the print version). Contents come from a Master List that Brill will provide typesetters. Note that the hyphen in an ISSN should always be included.
>>>
>>> ### Req. `@publication-format`
>>> Indicate whether this ISSN applies to a print or an online version.
>>>
>>> Supported values:
>>>
>>> - Req. `"print"`
>>> - `"online"`
>
>> ## Req. `<book-meta>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#Book-header-example)
>>
>>> ## Req. `<book-id>`
>>>
>>> An unique identifier for the book. These are provided by your Brill Desk Editor. Note that for the DOI, any alphabetic characters within the DOI itself should _always_ be included as lower-case in the XML file. A DOI consists of its prefix that identifies the publisher and a suffix that identifies the requisite publication. Prefix and suffix are separated by a '/' character. Available prefixes are:
>>> - Brill Germany: 10.30965
>>> - Brill: 10.1163
>>> - Böhlau Verlag Köln: 10.7788
>>> - Böhlau Verlag Wien: 10.7767
>>> - Vandenhoeck & Ruprecht: 10.13109
>>> - V&R unipress: 10.14220
>>> Value should match regex pattern `(10.14220|10.13109|10.7788|10.7767|10.1163|10.30965)/[0-9a-z.-]*`
>>>
>>> ### Req. `@book-id-type`
>>> Brill supports 2 values: 
>>>
>>> - Req. `"doi"`
>>> - `"publisher-id"`
>>
>>> ## Req. `<book-title-group>`
>>>
>>>> ## Req. `<book-title>`
>>>>
>>>> The major title of the book, excluding label and any subtitles.
>>>
>>>> ## `<subtitle>`
>>>>
>>>> The subtitle of the book.
>>>
>>>> ## `<trans-title-group>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#trans-title-group-example)
>>>>
>>>> For every translation of the book title, supply a `<trans-title-group>`.
>>>>
>>>> ### Req. `@xml:lang`
>>>> Indicates the language of the translated title. This attribute should be included here and *not* on the sub-elements. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>
>>>>> ## `<trans-title>`
>>>>>
>>>>> A translated title.
>>>>
>>>>> ## `<trans-subtitle>`
>>>>>
>>>>> A translated subtitle.
>>>
>>>> ## `<alt-title>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#alt-title-example)
>>>>
>>>> An alternative or different version of the title of the book used for sorting.
>>>>
>>>> ### Req. `@alt-title-type`
>>>>
>>>> Reason or purpose for a (shorter) title to be included, for Brill this is always a sorting-title, e.g. `"The Beatles" → "Beatles"`.
>>>>
>>>> - `"sort-title"`: Sort-title + convention.
>>
>>> ## Req. `<contrib-group>` (Book level)
>>>
>>> Grouping of one or more contributors and information about those contributors. All types of contributors are grouped in one `<contrib-group>`. Note that at most *one* `<contrib-group>` is allowed in `<book-meta>`.
>>>
>>>> ## `<contrib>`
>>>>
>>>> A contributor: Container element for information about a single author, editor, or other contributor.
>>>>
>>>> If a contribution is without author, do not add `<contrib>`.
>>>>
>>>> ### Req. `@contrib-type`
>>>>
>>>> Type of contribution made by the contributor. Brill supports the following contribution types for Book-level contributors:
>>>>
>>>> - `"author"`
>>>> - `"volume editor"` (default Book-level editor)
>>>> - `"contributor"`
>>>> - `"advisor"`
>>>> - `"editor"` (used for Text Editions only)
>>>> - `"editor/translator"`
>>>> - `"translator"`
>>>> - `"copy editor"`
>>>>
>>>> ### `@corresp`
>>>>
>>>> Use in case the book has multiple authors and this contributor is specifically identified as Corresponding Author. Do not include in all other cases. Can be used for multiple contributors to the same book. Supported values:
>>>>
>>>> - `"yes"`
>>>>
>>>> ### `@deceased`
>>>>
>>>> Use if the contributor was deceased when the document was published. Supported values:
>>>>
>>>> - `"yes"`
>>>> - `"no"` Default value, which makes this attributes unnecessary.
>>>>
>>>> ### `@equal-contrib`
>>>>
>>>> Indicates whether or not all contributors contributed equally. If the contributor whom this attribute modifies contributed equally with all other contributors, this attribute should be set to “yes”; if his/her contribution was greater or lesser, then this attribute should be set to “no”.
>>>>
>>>> - `"yes"`
>>>> - `"no"`
>>>> @equal-contrib is an optional attribute; there is no default.
>>>>
>>>>> ## `<contrib-id>`
>>>>>
>>>>> An ORCID identifier for a contributor. Note that an ORCID identifier should include the complete URL, for example: https://orcid.org/0000-0002-1825-0097
>>>>>
>>>>> ### Req. `@contrib-id-type`
>>>>>
>>>>> - `"orcid"`
>>>>>
>>>>> ### Req. `@authenticated`
>>>>>
>>>>> - `"true"`
>>>>
>>>>> ## `<collab>`
>>>>>
>>>>> A group of authors, but referred to only as a group, like a consortium or an institute, like the UN. If `<collab>` is used, `<name>` is not required.
>>>>>
>>>>
>>>>> ## Req. `<name>`
>>>>>
>>>>> Container element for components of personal names, such as a `<surname>`. Use `<name-alternatives>` if the name has multiple variants (e.g., in different scripts). 
>>>>>
>>>>> ### `@name-style`
>>>>>
>>>>> Style of processing requested for a structured personal name.
>>>>>
>>>>> - `"eastern"`: The name will both be displayed and sorted/inverted with the family name preceding the given name.
>>>>> - `"western"`: The name will be displayed with the given name preceding the family name, but will be sorted/inverted with the family name preceding the given name.
>>>>> - `"given-only"`: The single name provided is a given name, not a family name/surname. The single name will be both the sort and the display name.
>>>>>
>>>>> Default value is `"western"`. If an author has only a single name, `name-style="given-only"` and tag the single name with `<given-names>`.
>>>>>
>>>>>> ## Req. `<surname>`
>>>>>>
>>>>>> Surname of a person.
>>>>>
>>>>>> ## `<given-names>`
>>>>>>
>>>>>> All given names of a person, such as the first name, middle names, maiden name if used as part of the married name, etc. This element will NOT be repeated for every given name a person has.
>>>>>
>>>>>> ## `<prefix>`
>>>>>>
>>>>>> Honorifics or other qualifiers that usually precede a person’s name (for example, Professor, Rev., President, Senator, Dr., Sir, The Honorable).
>>>>>
>>>>>> ## `<suffix>`
>>>>>>
>>>>>> Qualifiers that follow a person’s name (for example, Sr., Jr., III, 3rd).
>>>>
>>>>> ## `<name-alternatives>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#String-name-Example)
>>>>>
>>>>> If a contributor’s name has multiple variants, use this element to include *all* variants. Put the variants in the same order as they would be on the printed page.
>>>>>
>>>>> The contents of this element are identical to the above description of `<name>`.
>>>>>
>>>>>> ## `<name>`
>>>>>>
>>>>>> ### `@xml:lang`
>>>>>>
>>>>>> Indicates the language of the name, if in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>>>
>>>>>>> ## `<surname>`
>>>>>>
>>>>>>> ## `<given-names>`
>>>>>>
>>>>>>> ## `<prefix>`
>>>>>>
>>>>>>> ## `<suffix>`
>>>>>
>>>>>> ## `<string-name>`
>>>>>>
>>>>>> ### `@xml:lang`
>>>>>>
>>>>>> Indicates the language of the name, if in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>
>>>>> ## `<email>`
>>>>>
>>>>> The contributor's email address.
>>>>>
>>>>> ### `@xlink:href`
>>>>> The e-mail address as a `mailto:` URI.
>>>>>
>>>>
>>>>> ## `<xref/>`
>>>>>
>>>>> A cross reference to an affiliation.
>>>>>
>>>>> ### Req. `@ref-type`
>>>>>
>>>>> Here, the type of reference is always `"aff"`.
>>>>>
>>>>> ### Req. `@rid`
>>>>>
>>>>> Contains the `@id` of affiliation this contributor is associated with. If the reference is to more than one element, add all IDs here separated by spaces.
>>>
>>>> ## `<aff>`
>>>>
>>>> Element used to include an affiliation. Note that for every new affiliation, a separate `<aff>` element should be included.
>>>>
>>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>>
>>>> Unique identifier for the affiliation, which is used in conjunction with `<xref ref-type="aff" rid=""/>` to refer to this affiliation.
>>>>
>>>>> ## `<institution-wrap>`
>>>>>> ## Req.`<institution>`
>>>>>>
>>>>>> Name of an institution or organization, like `Universiteit Leiden` or `Universiteit van Amsterdam`.
>>>>>>
>>>>>> This information is provided by Brill, but must be checked. This element can also contain the name of the department, like `Leiden University Centre for Linguistics`. Provide this value with a `@content-type="dept"`.
>>>>>>
>>>>>> ### `@content-type`
>>>>>>
>>>>>> Use this attribute to specify the extra institution information. Supported values:
>>>>>>
>>>>>> - `"dept"`
>>>>>
>>>>>> ## `<institution-id>`
>>>>>>
>>>>>> A externally defined institutional identifier, from an established identifying authority (for example, “Ringgold”).
>>>>>>
>>>>>> ### Req. `@institution-id-type`
>>>>>>
>>>>>> Brill only supports Ringgold identifiers. Supported values:
>>>>>>
>>>>>> - `"ringgold"`
>>>>
>>>>> ## Req. `<country>`
>>>>>
>>>>> The country the institution is located in. This is represented separately from an `<addr-line>`.
>>>>
>>>>> ## `<addr-line>`
>>>>>
>>>>> An text line in an address.
>>>>>
>>>>> ### `@content-type`.
>>>>>
>>>>> Indicates what type of address line information it is. Supported values include:
>>>>>
>>>>> - `"city"`
>>>>> - `"zipcode"`
>>
>>> ## Req. `<pub-date>`
>>>
>>>> ### `@publication-format`
>>>>
>>>> - Req. `"online"` (Note: this should _always_ be the date of delivery of the XML package to Brill. Once the book has been published online, this date should **NEVER** be adjusted in case the title later needs to be redelivered.)
>>>> - `"print"`
>>>>
>>>>> ## Req. `<day>`
>>>>>
>>>>> A day, in (zero-padded) digits.
>>>>
>>>>> ## Req. `<month>`
>>>>>
>>>>> A month, in (zero-padded) digits.
>>>>
>>>>> ## Req. `<year>`
>>>>>
>>>>> A year, in (zero-padded) digits.
>>
>>> ## `<book-volume-number>`
>>>
>>> The volume number of the book, when a single narrative is split into multiple volumes.
>>
>>> ## Req. `<isbn>`
>>>
>>> International Standard Book Number, the international code for identifying a particular product form or edition of a publication, typically a monographic publication.
>>> The code is always set without spaces or hyphens.
>>>
>>> ### Req. `@publication-format`
>>>
>>> Indicate whether this ISBN applies to an online or a print version. Supported values:
>>>
>>> - Req. `"online"`
>>> - `"print"`
>>>
>>> In case of multiple print versions with separate ISBNs, the following values are supported instead of `"print"`:
>>>
>>> - `"hardback"`
>>> - `"paperback"`
>>
>>> ## Req. `<publisher>`
>>>
>>>> ## Req. `<publisher-name>`
>>>>
>>>> Name of the imprint associated with the article or book. Brill will let the typesetters know which imprint to use for the book.
>>>>
>>>> Currently, for `<publisher-name>` only the text values below are permitted. (NOTE:  historically, other values may exist and should be retained if such files are redelivered, unless expressly noted otherwise.)
>>>>
>>>> - `Brill`
>>>> - `Brill | Nijhoff`
>>>> - `Brill | Hotei`
>>>> - `Brill | Sense`
>>>> - `Brill | Schöningh`
>>>> - `Brill | Fink`
>>>> - `Brill | mentis`
>>>> - `Vandenhoeck & Ruprecht`
>>>> - `Böhlau Verlag`
>>>> - `V&R Unipress`
>>>
>>>> ## Req. `<publisher-loc>`
>>>>
>>>> Use the location as given on the Title Page. If the publisher has more than one location (`Leiden | Boston`), this still has to specified in one element.
>>>>
>>>> ### `@specific-use`
>>>>
>>>> Supported values are:
>>>>
>>>> - `"online"`
>>>> - `"print"`
>>
>>> ## `<edition>`
>>>
>>> The full edition statement for a document, book part, collection, event, or for a cited or referenced publication.
>>
>>> ## `<supplementary-material>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#supplementary-material-example)
>>> A pointer to resources that support the article or book, but which are not part of the content of the work.
>>>
>>> ### Req. `@xlink:href`
>>> Will be supplied by the Brill Desk Editor.
>>>
>>> Supported values are:
>>>
>>> - figshare DOI
>>> - supplement filename
>>>
>>> ### Req. `@specific-use`
>>> Will be supplied by the Brill Desk Editor.
>>>
>>> - `"figshare"`
>>> - `"local"`
>>>
>>> ### `@mimetype`
>>> Only to be used when `@specific-use="local"`. Supported values can be found here: [https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types). If the value to be used is unclear, consult the Brill Production Editor.
>>>
>>>> ### `<caption>`
>>>> Only when `@specific-use="local"`
>>>>
>>>>> ### Req. `<title>`
>>>>> Add the title of the supplementary file, will be supplied by the Brill Desk Editor.
>>
>>> ## Req. `<permissions>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#permissions-examples)
>>>
>>>> ## Req. `<copyright-statement>`
>>>>
>>>> Copyright notice or statement, suitable for printing or display.
>>>
>>>> ## Req. `<copyright-year>`
>>>>
>>>> The year of copyright. This may not be displayed.
>>>
>>>> ## Req. `<copyright-holder>`
>>>>
>>>> Name of the organizational or person that holds a copyright.
>>>
>>>> ## Req. `<license>`
>>>>
>>>> A license. Set of conditions under which the content may be used, accessed, and distributed.
>>>>
>>>> ### `@license-type`
>>>>
>>>> - `"ccc"`. Standard Brill license type. See example for full text.
>>>> - `"open-access"`. Any Open Access license. If used, `@xlink:href` and `@xlink:title` are Required.
>>>>
>>>> ### `@xlink:href`
>>>> The URL at which the license text can be found. See [Creative Commons Licenses](https://creativecommons.org/licenses/).
>>>>
>>>> ### `@xlink:title`
>>>> If used with a Creative Commons license. Corresponds to `@xlink:href`. Supported values:
>>>>
>>>> - `"CC-BY"`
>>>> - `"CC-BY-SA"`
>>>> - `"CC-BY-ND"`
>>>> - `"CC-BY-NC"`
>>>> - `"CC-BY-NC-SA"`
>>>> - `"CC-BY-NC-ND"`
>>>>
>>>>> ## Req. `<license-p>`
>>>>>
>>>>> Paragraph of text within the description of a `<license>`.
>>>
>>>> ## `<ali:free_to_read>`
>>>>
>>>> Indicates any free-access license. Only to be used on specific instruction from Brill.
>>>>
>>>> ### Req. `@xmlns:ali="http://www.niso.org/schemas/ali/1.0/"`
>>>>
>>>> ### `@start_date`
>>>>
>>>> Indicates the date on which the free-access license should start. Value should use the following format: "yyyy-mm-dd".
>>>>
>>>> ### `@end_date`
>>>>
>>>> Indicates the date on which the free-access license should end. Value should use the following format: "yyyy-mm-dd".
>>>> The absence of both start_date and end_date dates indicates a permanent free-to-read status.
>>
>>> ## Req. `<self-uri/>` (Book Level)
>>> The URI for the (online) PDF-version of the book. Do not add this tag if there is no corresponding PDF file.
>>>
>>> This empty element encloses a link to the PDF.
>>>
>>> ### `@content-type`
>>>
>>> For Brill, this is always:
>>>
>>> - `"PDF"`
>>>
>>> ### `@xlink:href`
>>> The URL at which the PDF can be found.
>>
>>> ## `<funding-group>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#funding-group-example)
>>> Container element for information about the funding of the research reported in the book (for example, grants, contracts, sponsors).
>>>
>>>> ## `<award-group>`
>>>> Container element for information concerning one award under which the work (or the research on which the work was based) was supported.
>>>>
>>>> ### `@id` [See id-syntax](#id-syntax)
>>>>
>>>> The `@id` attribute supplies a document-internal unique reference to an element.
>>>>
>>>>> ## `<funding-source>`
>>>>> Agency or organization that funded the research on which a work was based.
>>>>>
>>>>>> ## `<institution-wrap>`
>>>>>>
>>>>>>> ## Req.`<institution>`
>>>>>>>
>>>>>>> Name of the funding institution or organization.
>>>>>>
>>>>>>> ## `<institution-id>`
>>>>>>>
>>>>>>> ### Req. `@institution-id-type`
>>>>>>>
>>>>>>> For funders, both DOI and Ringgold values are supported. Supported values:
>>>>>>>
>>>>>>> - `"doi"`
>>>>>>> - `"ringgold"`
>>>>
>>>>> ## `<award-id>`
>>>>>
>>>>> Unique identifier assigned to an award, contract, or grant. I.e., the grant number.
>>>>
>>>>> ## `<principal-award-recipient>`
>>>>>
>>>>> Individual(s) or institution(s) to whom the award was given (for example, the principal grant holder or the sponsored individual).
>>>>
>>>>> ## `<principal-investigator>`
>>>>>
>>>>> Individual(s) responsible for the intellectual content of the work reported in the document.
>>>
>>>> ## `<funding-statement>`
>>>>
>>>> Displayable prose statement that describes the funding for the research on which a work was based.
>>
>>> ## Req. `<counts>`
>>>
>>> Container element for counts of a document (for example, number of tables, number of words).
>>>
>>>> ## Req. `<book-page-count>`
>>>>
>>>> ### Req. `@count`
>>>>
>>>> Whereas the content can indicate a page-amount akin to `xii + 351`, in `@count` this must be a numerical total page count.
>>
>>> ## Req. `<custom-meta-group>`  [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#custom-meta-example)
>>>
>>> Container element for metadata not otherwise defined in the Tag Suite.
>>> Brill requires *header files* &ndash; XML files that do not contain body content &ndash; to have the `version: header` key-value pair. Brill requires *full content files* &ndash; XML files that contain both header and body content and back matter &ndash; to have the `version: fulltext` key-value pair.
>>>
>>>> ## Req. `<custom-meta>`
>>>>> ## Req. `<meta-name>`
>>>>>
>>>>> A custom metadata name (or key). Supported value:
>>>>> - `"version"`
>>>>
>>>>> ## Req. `<meta-value>`
>>>>>
>>>>> A custom metadata value. Supported values:
>>>>> - `"header"`
>>>>> - `"fulltext"`
>
>> ## `<front-matter>`
>>
>> This should contain all front matter of a book, i.e., everything with Roman numeral page numbers.
>>
>>> ## `<front-matter-part>`
>>>
>>> This element should be used for all front matter excluding that explicitly mentioned under the subsequent options. Front matter to be included under this element are a.o.the half title page, series page, title page, copyright page, etc. 
>>>
>>> **Important Note: **_beyond_ the separate `<front-matter-part>`s for Half Title Page, Series Page, Title Page, Copyright Page, the `<toc>`, and any List of Figures/Maps/Etc., an _additional_ `<front-matter-part>` should be included that does _not_ include the full text, but _must_ include a link to a PDF of all these parts together, as well as a separate DOI. This should receive the title "Preliminary Material".
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>
>>> The `@id` attribute supplies a document-internal unique reference to an element. Note that an `@id` is *always* required for all main elements directly below `<front-matter>`
>>>
>>> ### Req. `@specific-use`
>>>
>>> This element is **required** for every `<front-matter-part>`, *including* the Copyright Page and the "Preliminary Material", but *not* for the following: Half Title Page, Series Page, and Title Page. Supported values are:
>>>
>>> - `"online"`
>>>
>>>> ## Req. `<book-part-meta>` [See Book-part-meta](#book-part-meta)
>>>
>>>> ## `<named-book-part-body>`
>>>>
>>>>> ## `<sec>` [See Sections](#sections)
>>>>>
>>>>> All  sections of the front matter that are consecutive may be included as `<sec>` elements in the same `<named-book-part-body>`.
>>>>>
>>>>>> ## `<title>`
>>>>>>
>>>>>> Every `<front-matter-part>` *must* include a title. If no specific title is available, use a descriptive title instead, e.g., "Copyright Page".
>>>
>>>> ## `<back>`
>>>>
>>>>> ## `<label>`
>>>>>
>>>>> Number and/or prefix word placed at the beginning of display elements. For example "Chapter 1", "Introduction:", "Conclusion:", "Appendix:", etc. 
>>>>
>>>>> ## `<title>`
>>>>>
>>>>> Title of the back-matter of this book-part.
>>>>
>>>>> ## `<fn-group>` [See Footnotes](#footnotes)
>>>>>
>>>>> Note that having a `<fn-group>` here means that a `<fn-group>` directly under `<book-back>` is not allowed.
>>>>
>>>>> ## `<glossary>`    [See Glossary](#glossary)
>>>>
>>>>> ## `<ref-list>`    [See Reference List](#reference-list)
>>>>
>>>>> ## `<sec>` [See Sections](#sections)
>>
>>> ## `<toc>`
>>>
>>> The Table of Contents. This element may be used multiple times to also include, for example, a List of Figures or a List of Tables. Please note that the attribute `@specific-use="online"` is *not* permitted on `<toc>`.
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>
>>> The `@id` attribute supplies a document-internal unique reference to an element. Note that an `@id` is *always* required for all main elements directly below `<front-matter>`
>>>
>>>> ## Req. `<toc-title-group>`
>>>>
>>>>> ## Req. `<title>`
>>>
>>>> ## `<toc-div>`
>>>>
>>>> Division, typically for the purposes of display, in a structural Table of Contents, for example, a division holding the first Part of a 3-part book, and the titles of all the chapters in that Part. This element is optional.
>>>>
>>>>> ## `<toc-title-group>`
>>>>>
>>>>>> ## `<title>`
>>>>
>>>>> ## Req. `<toc-entry>`
>>>>>
>>>>> One entry in a structural Table of Contents. This may also be included as a direct sub-element of `<toc>` (thereby skipping `toc-div>`).
>>>>>
>>>>>> ## `<label>`
>>>>>>
>>>>>> For example "Chapter 1", "Introduction:", "Conclusion:", "Appendix:", etc. 
>>>>>
>>>>>> ## Req. `<title>`
>>>>>
>>>>>> ## `<subtitle>`
>>>>>
>>>>>> ## `<contrib-group>`
>>>>>>
>>>>>> Grouping of one or more contributors for the `<toc-entry>`. All types of contributors are grouped in one `<contrib-group>`. Do not add this tag group if a chapter does not have an author. 
>>>>>>
>>>>>>> ## Req. `<contrib>`
>>>>>>>
>>>>>>> A contributor: Container element for information about a single author, editor, or other contributor.
>>>>>>>
>>>>>>> ### Req. `@contrib-type`
>>>>>>>
>>>>>>> Type of contribution made by the contributor. Brill supports the following contribution types for Chapter-level contributors:
>>>>>>>
>>>>>>> - `"author"`
>>>>>>> - `"editor"`
>>>>>>> - `"editor/translator"`
>>>>>>> - `"translator"`
>>>>>>>
>>>>>>>> ## `<collab>`
>>>>>>>>
>>>>>>>> A group of authors, but referred to only as a group, like a consortium or an institute, like the UN. If `<collab>` is used, `<name>` is not required.
>>>>>>>>
>>>>>>>
>>>>>>>> ## Req. `<name>`
>>>>>>>>
>>>>>>>> Container element for components of personal names, such as a `<surname>`. Use `<name-alternatives>` if the name has multiple variants (e.g., in different scripts). 
>>>>>>>>
>>>>>>>> ### `@name-style`
>>>>>>>>
>>>>>>>> Style of processing requested for a structured personal name.
>>>>>>>>
>>>>>>>> - `"eastern"`: The name will both be displayed and sorted/inverted with the family name preceding the given name.
>>>>>>>> - `"western"`: The name will be displayed with the given name preceding the family name, but will be sorted/inverted with the family name preceding the given name.
>>>>>>>> - `"given-only"`: The single name provided is a given name, not a family name/surname. The single name will be both the sort and the display name.
>>>>>>>>
>>>>>>>> Default value is `"western"`. If an author has only a single name, `name-style="given-only"` and tag the single name with `<given-names>`.
>>>>>>>>
>>>>>>>>> ## Req. `<surname>`
>>>>>>>>>
>>>>>>>>> Surname of a person.
>>>>>>>>
>>>>>>>>> ## `<given-names>`
>>>>>>>>>
>>>>>>>>> All given names of a person, such as the first name, middle names, maiden name if used as part of the married name, etc. This element will NOT be repeated for every given name a person has.
>>>>>>>>
>>>>>>>>> ## `<prefix>`
>>>>>>>>>
>>>>>>>>> Honorifics or other qualifiers that usually precede a person’s name (for example, Professor, Rev., President, Senator, Dr., Sir, The Honorable).
>>>>>>>>
>>>>>>>>> ## `<suffix>`
>>>>>>>>>
>>>>>>>>> Qualifiers that follow a person’s name (for example, Sr., Jr., III, 3rd).
>>>>>>>
>>>>>>>> ## `<name-alternatives>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#String-name-Example)
>>>>>>>>
>>>>>>>> If a contributor’s name has multiple variants, use this element to include *all* variants. Put the variants in the same order as they would be on the printed page.
>>>>>>>>
>>>>>>>> The contents of this element are identical to the above description of `<name>`.
>>>>>>>>
>>>>>>>>> ## `<name>`
>>>>>>>>>
>>>>>>>>> ### `@xml:lang`
>>>>>>>>>
>>>>>>>>> Indicates the language of the name, if in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>>>>>>
>>>>>>>>>> ## `<surname>`
>>>>>>>>>
>>>>>>>>>> ## `<given-names>`
>>>>>>>>>
>>>>>>>>>> ## `<prefix>`
>>>>>>>>>
>>>>>>>>>> ## `<suffix>`
>>>>>>>>
>>>>>>>>> ## `<string-name>`
>>>>>>>>>
>>>>>>>>> ### `@xml:lang`
>>>>>>>>>
>>>>>>>>> Indicates the language of the name, if in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>
>>> ## `<ack>`
>>>
>>> Acknowledgment. Textual material that names the parties who the author wishes to thank or recognize for their assistance in, for example, producing the work, funding the work, inspiring the work, or assisting in the research on which the work is based.
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>
>>> The `@id` attribute supplies a document-internal unique reference to an element. Note that an `@id` is *always* required for all main elements directly below `<front-matter>`
>>>
>>> ### Req. `@specific-use`
>>>
>>> Supported values are:
>>>
>>> - `"online"`
>>>
>>>> ## Req. `<title>`
>>>
>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>
>>>> ## `<sec>` [See Sections](#sections)
>>>
>>>> ## `<fn-group>` [See Footnotes](#footnotes)
>>>>
>>>> Note that having a `<fn-group>` here means that a `<fn-group>` directly under `<book-back>` is not allowed.
>>
>>> ## `<bio>`
>>>
>>> Biographical data concerning the contributor(s); for example, the "Notes on Contributors" section.
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>
>>> The `@id` attribute supplies a document-internal unique reference to an element. Note that an `@id` is *always* required for all main elements directly below `<front-matter>`
>>>
>>> ### Req. `@specific-use`
>>>
>>> Supported values are:
>>>
>>> - `"online"`
>>>
>>>> ## Req. `<title>`
>>>
>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>
>>>> ## `<sec>` [See Sections](#sections)
>>>
>>>> ## `<fn-group>` [See Footnotes](#footnotes)
>>>> 
>>>> Note that having a `<fn-group>` here means that a `<fn-group>` directly under `<book-back>` is not allowed.
>>
>>> ## `<dedication>`
>>>
>>> A named book component that contains text dedicating the book in some fashion.
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>
>>> The `@id` attribute supplies a document-internal unique reference to an element. Note that an `@id` is *always* required for all main elements directly below `<front-matter>`
>>>
>>> ### Req. `@specific-use`
>>>
>>> Supported values are:
>>>
>>> - `"online"`
>>>
>>>> ## Req. `<book-part-meta>` [See Book-part-meta](#book-part-meta)
>>>
>>>> ## Req. `<named-book-part-body>`
>>>>
>>>>> ## `<sec>` [See Sections](#sections)
>>>>
>>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>
>>>> ## `<back>`
>>>>
>>>>> ## `<label>`
>>>>>
>>>>> Number and/or prefix word placed at the beginning of display elements. For example "Chapter 1", "Introduction:", "Conclusion:", "Appendix:", etc. 
>>>>
>>>>> ## `<title>`
>>>>>
>>>>> Title of the back-matter of this book-part.
>>>>
>>>>> ## `<fn-group>` [See Footnotes](#footnotes)
>>>>>
>>>>> Note that having a `<fn-group>` here means that a `<fn-group>` directly under `<book-back>` is not allowed.
>>>>
>>>>> ## `<glossary>`    [See Glossary](#glossary)
>>>>
>>>>> ## `<ref-list>`    [See Reference List](#reference-list)
>>>>
>>>>> ## `<sec>` [See Sections](#sections)
>>
>>> ## `<preface>`
>>>
>>> A named book component that contains text which is called a “Preface”. Includes all the same requirements and options as `<dedication>`.
>>
>>> ## `<foreword>`
>>>
>>> A named book component that contains text which is called a “Foreword”. Includes all the same requirements and options as `<dedication>`.
>>
>>> ## `<glossary>`    [See Glossary](#glossary)
>
>> ## `<book-body>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#book-body-example)
>>
>>> ## `<book-part>`
>>>
>>> A major organizational unit of a book, typically called a “chapter”, but given many names in common language, for example, chapter, part, unit, module, section, topic, lesson, canto, volume, or even “book”.
>>> This includes parts (Part 1, Part 2, …), introductions and other chapter-like content.
>>> Book Parts are recursive, that is, various levels of `<book-part>` are indicated by containment, not by different names for the interior parts. The body of a book part (`<body>`) may contain lower level `<book-part>` elements or just recursive sections that are tagged using the `<sec>` element. Note that book parts should be nested if relevant - so for example, if a book has several "Parts", and Part 1 includes Chapters 1 and 2, then `<book-part>` "Chapter 1" and `<book-part>` "Chapter 2" should be nested in `<book-part>` "Part 1". [Example](http://brill.gitlab.io/example-nested-book-part/)
>>>
>>> **Important Note**: If the `<book-part>` is just a "Part" page (using `@book-part-type="part"`; so only a title page serving as a top-level divider in the book, acting as a "container" for the Chapters below it), then this "Part" page should *not* be included in the following Chapter's PDF. The "Part" `<book-part>` should then also *not* receive an `@id`, a DOI, a `<self-uri/>` to a PDF, or any other content beyond a `<book-part-meta>` with a `<title-group>` and `<title>`.
>>>
>>> ### `@xml:lang`
>>>
>>> Indicates this book-part is in another language than the main language of the book. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>
>>> ### Req. `@book-part-type`
>>>
>>> Examples may include:
>>>
>>> - `"part"`(to be used for top-level dividers in the book that act as "containers" for the chapters below)
>>> - `"chapter"`
>>> - `"section"`
>>> - `"volume"`
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>
>>> The `@id` attribute supplies a document-internal unique reference to an element. Note that an `@id` is **required** for a `<book-part>`, _unless_ `@book-part-type="part"`, in which case it should _not_ be included.
>>>
>>> ### Req. `@seq`
>>>
>>> A sequential number starting at 1 per book-body that increments for each book-part in it. This sorts the book-parts in the book-body.
>>>
>>>> ## Req. `<book-part-meta>` [See Book-part-meta](#book-part-meta)
>>>
>>>> ## `<body>`
>>>>
>>>> Main textual portion of the document that conveys the narrative content.
>>>> In NISO BITS, `<body>` can only occur within `<book-part>`.
>>>>
>>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>>
>>>>> ## `<sec>` [See Sections](#sections)
>>>>>
>>>>>> ## `<sig-block>`
>>>>>>
>>>>>> Area of text and graphic material placed at the end of the body of a document or document component to hold the graphic signature or description of the person(s) responsible for or attesting to the content.
>>>>>>
>>>>>>> ## `<sig>`
>>>>>>>
>>>>>>> One contributor’s signature and associated material (such as a text restatement of the affiliation) inside a signature block.
>>>>>>
>>>>>>>> ## `<graphic>`
>>>>>>>>
>>>>>>>> Description of and pointer to an external file containing a still image.
>>>>>>>>
>>>>>>>> ### `@xlink:href`
>>>>>>>>
>>>>>>>> The URI where the resource can be found.
>>>>>>>>
>>>>>>>> ### `@orientation`
>>>>>>>>
>>>>>>>> Whether the object should be positioned tall (long edge vertical `"portrait"`) or wide (long edge horizontal `"landscape"`).
>>>>>>>>
>>>>>>>> - `"portrait"`
>>>>>>>> - `"landscape"`
>>>>>>>>
>>>>>>>> ### `@position`
>>>>>>>>
>>>>>>>> Whether the object must be anchored to its place or may float algorithmically to another location in the document. Also used for margin placement.
>>>>>>>>
>>>>>>>> - `"anchor"` Object must remain on exact location in the text flow.
>>>>>>>> - `"float"` **Default value**. Object may move to a new column, page, end of document, etc.
>>>>>>>> - `"margin"` The object should be placed in the margin or gutter.
>>>
>>>> ## `<back>`
>>>>
>>>>> ## `<label>`
>>>>>
>>>>> Number and/or prefix word placed at the beginning of display elements. For example "Chapter 1", "Introduction:", "Conclusion:", "Appendix:", etc. 
>>>>
>>>>> ## `<title>`
>>>>>
>>>>> Title of the back-matter of this book-part.
>>>>
>>>>> ## `<fn-group>` [See Footnotes](#footnotes)
>>>>>
>>>>> Note that having a `<fn-group>` here means that a `<fn-group>` directly under `<book-back>` is not allowed.
>>>>
>>>>> ## `<glossary>`    [See Glossary](#glossary)
>>>>
>>>>> ## `<ref-list>`    [See Reference List](#reference-list)
>>>>
>>>>> ## `<sec>` [See Sections](#sections)
>
>> ## `<book-back>`
>>
>> Material published with a book but following the narrative flow, e.g., appendices, lists of references, notes on contributors, etc.
>>
>> ### Req. `@specific-use`
>>
>> Supported values are:
>>
>> - `"online"`
>>
>>> ## `<bio>`
>>>
>>> Biographical data concerning the contributor(s); for example, the "Notes on Contributors" section.
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>
>>> The `@id` attribute supplies a document-internal unique reference to an element. Note that an `@id` is *always* required for all main elements directly below `<front-matter>`
>>>
>>> ### Req. `@specific-use`
>>>
>>> Supported values are:
>>>
>>> - `"online"`
>>>
>>>> ## Req. `<title>`
>>>
>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>
>>>> ## `<sec>` [See Sections](#sections)
>>>
>>>> ## `<fn-group>` [See Footnotes](#footnotes)
>>>>
>>>> Note that having a `<fn-group>` here means that a `<fn-group>` directly under `<book-back>` is not allowed.
>>
>>> ## `<index>`    [See Index](#index)
>>>
>>> ### Req. `@specific-use`
>>>
>>> Supported values are:
>>>
>>> - `"online"`
>>
>>> ## `<index-group>`    [See Index](#index)
>>>
>>> ### Req. `@specific-use`
>>>
>>> Supported values are:
>>>
>>> - `"online"`
>>
>>> ## `<ref-list>`    [See Reference List](#reference-list)
>>>
>>> ### Req. `@specific-use`
>>>
>>> Supported values are:
>>>
>>> - `"online"`
>>
>>> ## `<fn-group>` [See Footnotes](#footnotes)
>>>
>>> Note that having a `<fn-group>` in the `<book-back>` means that having a `<fn-group>` elswhere in the XML is not allowed.
>>>
>>> ### Req. `@specific-use`
>>>
>>> Supported values are:
>>>
>>> - `"online"`
>>
>>> ## `<glossary>`    [See Glossary](#glossary)
>>>
>>> ### Req. `@specific-use`
>>>
>>> Supported values are:
>>>
>>> - `"online"`
>>
>>> ## `<book-app>`
>>>
>>> An Appendix to an entire book.
>>>
>>> ### Req. `@specific-use`
>>>
>>> Supported values are:
>>>
>>> - `"online"`
>>>
>>> ### `@xml:lang`
>>>
>>> Indicates this book-part is in another language than the main language of the book. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>
>>> The `@id` attribute supplies a document-internal unique reference to an element. 
>>>
>>>> ## `<book-part-meta>` [See Book-part-meta](#book-part-meta)
>>>
>>>> ## `<body>`
>>>>
>>>> Main textual portion of the document that conveys the narrative content.
>>>> In NISO BITS, `<body>` can only occur within `<book-part>`.
>>>>
>>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>>
>>>>> ## `<sec>` [See Sections](#sections)
>>>
>>>> ## `<back>`
>>>>
>>>>> ## `<label>`
>>>>>
>>>>> Number and/or prefix word placed at the beginning of display elements. For example "Chapter 1", "Introduction:", "Conclusion:", "Appendix:", etc. 
>>>>
>>>>> ## `<title>`
>>>>>
>>>>> Title of the back-matter of this book-part.
>>>>
>>>>> ## `<fn-group>` [See Footnotes](#footnotes)
>>>>>
>>>>> Note that having a `<fn-group>` here means that a `<fn-group>` directly under `<book-back>` is not allowed.
>>>>
>>>>> ## `<glossary>`    [See Glossary](#glossary)
>>>>
>>>>> ## `<ref-list>`    [See Reference List](#reference-list)
>>>>
>>>>> ## `<sec>` [See Sections](#sections)

# Book-part-meta

This is a required element for every`<book-part>`, `<front-matter-part>`, `<dedication>`, `<foreword>`, `<preface>` and `<book-app>`

> ## Req. `<book-part-meta>`
>
>> ## `<book-part-id>`
>>
>> Unique external identifier assigned to a book-part. Note that for the DOI, any alphabetic characters within the DOI itself should _always_ be included as lower-case in the XML file.
>>
>> ### Req. `@book-part-id-type`
>>
>> Type of publication identifier or the organization or system that defined the identifier. Note that is **required** for _all_ parts of the book containing a `<book-part-meta>` _except_ for the Half Title Page, Series Page, and Title Page.
>>
>> - `"doi"`
>
>> ## Req. `<title-group>`
>>
>>> ## `<label>`
>>>
>>> Label (of an Equation, Figure, Reference, etc.), zero or one. For example "Chapter 1", "Introduction:", "Conclusion:", "Appendix:", etc. 
>>>
>>> ## Req. `<title>`
>>>
>>> Full title of a book chapter, or book part. If no specific title is available, use a descriptive title instead, e.g., "Dedication".
>>
>>> ## `<subtitle>`
>>>
>>> Subordinate part of a title for a document or document component. Refer to the manuscript to see what the author meant as subtitle.
>>
>>> ## `<trans-title-group>`
>>>
>>> For every translation of the title, supply a `<trans-title-group>`.
>>>
>>> ### Req. `@xml:lang`
>>>
>>> Indicates the language of the translated title. This attribute should be included here and *not* on the sub-elements. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>
>>>> ## `<trans-title>`
>>>>
>>>> The translation of the title.
>>>
>>>> ## `<trans-subtitle>`
>>>>
>>>> The translation of the subtitle.
>>>
>>> ## `<alt-title>`
>>>
>>> An alternative or different version of the title of a work.
>>>
>>> ### `@alt-title-type`
>>>
>>> Reason or purpose for a (shorter) title to be included. Brill supports 2 values for `@alt-title-type`:
>>>
>>> - `"toc"`: Title to be used in the table of contents.
>>> - `"running-head"`: Title to be used as the running head. This is a shorter version of the regular title. An `<alt-title>` with this type is optional.
>>>
>
>> ## `<contrib-group>` (Chapter/Part level)
>>
>> Grouping of one or more contributors and information about those contributors. All types of contributors are grouped in one `<contrib-group>`. Do not add this tag group if a chapter does not have an author, like for instance the *Prelims* or *Indices* of a book. Note that at most *one* `<contrib-group>` is allowed per `<book-part-meta>`.
>>
>>> ## Req. `<contrib>`
>>>
>>> A contributor: Container element for information about a single author, editor, or other contributor.
>>>
>>> If a contribution is without author, do not add `<contrib>`.
>>>
>>> ### Req. `@contrib-type`
>>>
>>> Type of contribution made by the contributor. Brill supports the following contribution types for Chapter-level contributors:
>>>
>>> - `"author"`
>>> - `"editor"`
>>> - `"editor/translator"`
>>> - `"translator"`
>>>
>>> ### `@corresp`
>>>
>>> Use in case the chapter/part has multiple authors and this contributor is specifically identified as Corresponding Author. Do not include in all other cases. Can be used for multiple contributors to the same chapter/part. Supported values:
>>>
>>> - `"yes"`
>>>
>>> ### `@deceased`
>>>
>>> Use if the contributor was deceased when the document was published. Supported values:
>>>
>>> - `"yes"`
>>> - `"no"` Default value, which makes this attributes unnecessary.
>>>
>>> ### `@equal-contrib`
>>>
>>> Indicates whether or not all contributors contributed equally. If the contributor whom this attribute modifies contributed equally with all other contributors, this attribute should be set to “yes”; if his/her contribution was greater or lesser, then this attribute should be set to “no”.
>>>
>>> - `"yes"`
>>> - `"no"`
>>> @equal-contrib is an optional attribute; there is no default.
>>>
>>>> ### `<contrib-id>`
>>>>
>>>> An ORCID identifier for a contributor. Note that an ORCID identifier should include the complete URL, for example: https://orcid.org/0000-0002-1825-0097
>>>>
>>>> ### Req. `@contrib-id-type`
>>>>
>>>> - `"orcid"`
>>>>
>>>> ### Req. `@authenticated`
>>>>
>>>> - `"true"`
>>>
>>>> ## `<collab>`
>>>>
>>>> A group of authors, but referred to only as a group, like a consortium or an institute, like the UN. If `<collab>` is used, `<name>` is not required.
>>>>
>>>> ## Req. `<name>`
>>>>
>>>> Container element for components of personal names, such as a `<surname>`. Use `<name-alternatives>` if the name has multiple variants (e.g., in different scripts). 
>>>>
>>>> ### `@name-style`
>>>>
>>>> Style of processing requested for a structured personal name.
>>>>
>>>> - `"eastern"`: The name will both be displayed and sorted/inverted with the family name preceding the given name.
>>>> - `"western"`: The name will be displayed with the given name preceding the family name, but will be sorted/inverted with the family name preceding the given name.
>>>> - `"given-only"`: The single name provided is a given name, not a family name/surname. The single name will be both the sort and the display name.
>>>>
>>>> Default value is `"western"`. If an author has only a single name, `name-style="given-only"` and tag the single name with `<given-names>`.
>>>>
>>>>> ## Req. `<surname>`
>>>>>
>>>>> Surname of a person.
>>>>
>>>>> ## `<given-names>`
>>>>>
>>>>> All given names of a person, such as the first name, middle names, maiden name if used as part of the married name, etc. This element will NOT be repeated for every given name a person has.
>>>>
>>>>> ## `<prefix>`
>>>>>
>>>>> Honorifics or other qualifiers that usually precede a person’s name (for example, Professor, Rev., President, Senator, Dr., Sir, The Honorable).
>>>>
>>>>> ## `<suffix>`
>>>>>
>>>>> Qualifiers that follow a person’s name (for example, Sr., Jr., III, 3rd).
>>>
>>>> ## `<name-alternatives>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#String-name-Example)
>>>>
>>>> If a contributor’s name has multiple variants, use this element to include *all* variants. Put the variants in the same order as they would be on the printed page.
>>>>
>>>> The contents of this element are identical to the above description of `<name>`.
>>>>
>>>>> ## `<name>`
>>>>>
>>>>> ### `@xml:lang`
>>>>>
>>>>> Indicates the language of the name, if in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>>
>>>>>> ## `<surname>`
>>>>>
>>>>>> ## `<given-names>`
>>>>>
>>>>>> ## `<prefix>`
>>>>>
>>>>>> ## `<suffix>`
>>>>
>>>>> ## `<string-name>`
>>>>>
>>>>> ### `@xml:lang`
>>>>>
>>>>> Indicates the language of the name, if in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>
>>>> ## `<email>`
>>>>
>>>> The contributor's email address.
>>>>
>>>> ### `@xlink:href`
>>>> The e-mail address as a `mailto:` URI.
>>>
>>>> ## `<xref/>`
>>>>
>>>> A cross reference to an affiliation or to a footnote.
>>>>
>>>> ### Req. `@ref-type`
>>>>
>>>> Supported values:
>>>>
>>>> - `"aff"` 
>>>> - `"fn"`
>>>>
>>>> ### Req. `@rid`
>>>>
>>>> Contains the `@id` of the affiliation this contributor is associated with, or of the associated author-notes. If the reference is to more than one element, add all IDs here separated by spaces.
>>
>>> ## `<aff>`
>>>
>>> Element used to include an affiliation. Note that for every new affiliation, a separate `<aff>` element should be included.
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>
>>> Unique identifier for the affiliation, which is used in conjunction with `<xref ref-type="aff" rid=""/>` to refer to this affiliation.
>>>
>>>> ## `<institution-wrap>`
>>>>
>>>>> ## Req.`<institution>`
>>>>>
>>>>> Name of an institution or organization, like `Universiteit Leiden` or `Universiteit van Amsterdam`.
>>>>>
>>>>> This element can also contain the name of the department, like `Leiden University Centre for Linguistics`. Provide this value with a `@content-type="dept"`.
>>>>>
>>>>> ### `@content-type`
>>>>>
>>>>> Use this attribute to specify the extra institution information. Supported values:
>>>>>
>>>>> - `"dept"`
>>>>
>>>>> ## `<institution-id>`
>>>>>
>>>>> A externally defined institutional identifier, from an established identifying authority (for example, “Ringgold”).
>>>>>
>>>>> ### Req. `@institution-id-type`
>>>>>
>>>>> Brill only supports Ringgold identifiers. Supported values:
>>>>>
>>>>> - `"ringgold"`
>>>
>>>> ## Req. `<country>`
>>>>
>>>> The country the institution is located in. This is represented separately from an `<addr-line>`.
>>>
>>>> ## `<addr-line>`
>>>>
>>>> An text line in an address.
>>>>
>>>> ### `@content-type`
>>>>
>>>> Indicates what type of address line information it is. Supported values include:
>>>>
>>>> - `"city"`
>>>> - `"zipcode"`
>
>> ## `<fpage>`
>>
>> Starting *(first)* page number of the book part.
>>
>> ### Req. `@specific-use`
>>
>> This attribute’s value is always `"PDF"`.
>>
>> ### Req. `@seq`
>>
>> A sequential number starting at 1 per book that increments for each chapter or part in it. This sorts the chapters in a book.
>
>> ## `<lpage>`
>>
>> Final *(last)* page number of a book part.
>
>> ## `<supplementary-material>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#supplementary-material-example)
>> A pointer to resources that support the article or book, but which are not part of the content of the work.
>>
>> ### Req. `@xlink:href`
>> Will be supplied by the Brill Desk Editor.
>>
>> Supported values are:
>>
>> - figshare DOI
>> - supplement filename
>>
>> ### Req. `@specific-use`
>> Will be supplied by the Brill Desk Editor.
>>
>> - `"figshare"`
>> - `"local"`
>>
>> ### `@mimetype`
>> Only to be used when `@specific-use="local"`. Supported values can be found here: [https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types). If the value to be used is unclear, consult the Brill Production Editor.
>>
>>> ### `<caption>`
>>> Only when `@specific-use="local"`
>>>
>>>> ### Req. `<title>`
>>>> Add the title of the supplementary file, will be supplied by the Brill Desk Editor.
>
>> ## `<permissions>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#Permissions-Examples)
>>
>> *Only* add this element if the license is different than the superordinate `<book>` as it could be in mixed-license edited
>> volumes with Open Access chapters.
>>
>>> ## Req. `<copyright-statement>`
>>>
>>> Copyright notice or statement, suitable for printing or display.
>>
>>> ## Req. `<copyright-year>`
>>>
>>> The year of copyright. This may not be displayed.
>>
>>> ## Req. `<copyright-holder>`
>>>
>>> Name of the organizational or person that holds a copyright.
>>
>>> ## Req. `<license>`
>>>
>>> A license. Set of conditions under which the content may be used, accessed, and distributed.
>>>
>>> *Only* add this element if chapter is Open Access.
>>>
>>> ### `@license-type`
>>>
>>> - `"open-access"`. Any Open Access license. If used, `@xlink:href` and `@xlink:title` are Required.
>>>
>>> ### `@xlink:href`
>>> The URL at which the license text can be found. See [Creative Commons Licenses](https://creativecommons.org/licenses/).
>>>
>>> ### `@xlink:title`
>>> If used with a Creative Commons license. Corresponds to `@xlink:href`. Supported values:
>>>
>>> - `"CC-BY"`
>>> - `"CC-BY-SA"`
>>> - `"CC-BY-ND"`
>>> - `"CC-BY-NC"`
>>> - `"CC-BY-NC-SA"`
>>> - `"CC-BY-NC-ND"`
>>>
>>>> ## Req. `<license-p>`
>>>>
>>>> Paragraph of text within the description of a `<license>`.
>>
>>> ## `<ali:free_to_read>`
>>>
>>> Indicates any free-access license. Only to be used on specific instruction from Brill.
>>>
>>> ### Req. `@xmlns:ali="http://www.niso.org/schemas/ali/1.0/"`
>>>
>>> ### `@start_date`
>>>
>>> Indicates the date on which the free-access license should start. Value should use the following format: "yyyy-mm-dd".
>>>
>>> ### `@end_date`
>>>
>>> Indicates the date on which the free-access license should end. Value should use the following format: "yyyy-mm-dd".
>>> The absence of both start_date and end_date dates indicates a permanent free-to-read status.
>
>> ## `<self-uri/>` (Chapter or Part)
>> The URI for the (online) PDF-version of the chapter or part. Do not add this tag if there is no corresponding PDF file.
>>
>> This empty element encloses a link to the PDF.
>>
>> ### `@content-type`
>>
>> For Brill, this is always:
>>
>> - `"PDF"`
>>
>> ### `@xlink:href`
>> The URL at which the PDF can be found.
>
>> ## `<abstract>`
>> The book chapter abstract, so a summarized description of the content.
>>
>>> ## Req. `<title>`
>>>
>>> The title for the abstract. Usually, this is `"Abstract"`, but it can also be `"Samenvatting"`, `"Summary"` or `"Σύνοψις"`. The Brill Desk Editor will provide you with a title if it is different than `"Abstract"`. Do not add style elements!
>>
>>> ## Req. `<p>` [See Paragraphs](#paragraphs)
>
>> ## `<trans-abstract>`
>> An abstract in another language than the main language of the `<book-part>`.
>>
>> ### Req. `@xml:lang`
>>
>> Indicates the language on a translated abstract in another language than the main language of the `<book-part>`. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>
>>> ## Req. `<p>` [See Paragraphs](#paragraphs)
>
>> ## `<kwd-group>`
>>
>> Keyword group. Container element for one set of keywords (such as `<kwd>`s) used to describe a document.
>>
>> ### Req. `@kwd-group-type`
>>
>> Indicates the type of keyword group, supported values:
>>
>> - `"uncontrolled"`
>>
>> In future versions, systems like the *US Library of Congress Subject Headings* may be used.
>>
>> ### `@xml:lang`
>>
>> Indicates a keyword grouping in another language than the main language of the `<book-part>`. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>
>>> ## Req. `<title>`
>>>
>>> The title for the keyword group. Usually, this is `"Keywords"`; the Brill Desk Editor will provide you with a title if it is different than `"Keywords"`. Do not add style elements!
>>
>>> ## Req. `<kwd>`
>>>
>>> Keyword, like a subject term, key phrase, indexing word etc. The author will supply this information.
>
>> ## `<funding-group>`
>> Container element for information about the funding of the research reported in the chapter (for example, grants, contracts, sponsors).
>>
>>> ## `<award-group>`
>>> Container element for information concerning one award under which the work (or the research on which the work was based) was supported.
>>>
>>> ### `@id` [See id-syntax](#id-syntax)
>>>
>>> The `@id` attribute supplies a document-internal unique reference to an element.
>>>
>>>> ## `<funding-source>`
>>>> Agency or organization that funded the research on which a work was based.
>>>>
>>>>> ## `<institution-wrap>`
>>>>>
>>>>>> ## Req.`<institution>`
>>>>>>
>>>>>> Name of the funding institution or organization.
>>>>>>
>>>>>>> ## `<institution-id>`
>>>>>>>
>>>>>>> ### Req. `@institution-id-type`
>>>>>>>
>>>>>>> For funders, both DOI and Ringgold values are supported. Supported values:
>>>>>>>
>>>>>>> - `"doi"`
>>>>>>> - `"ringgold"`
>>>
>>>> ## `<award-id>`
>>>>
>>>> Unique identifier assigned to an award, contract, or grant. I.e., the grant number.
>>>
>>>> ## `<principal-award-recipient>`
>>>>
>>>> Individual(s) or institution(s) to whom the award was given (for example, the principal grant holder or the sponsored individual).
>>>
>>>> ## `<principal-investigator>`
>>>>
>>>> Individual(s) responsible for the intellectual content of the work reported in the document.
>>
>>> ## `<funding-statement>`
>>>
>>> Displayable prose statement that describes the funding for the research on which a work was based.
>
>> ## Req. `<counts>`
>>
>> Container element for counts of a document (for example, number of tables, number of words).
>>
>>> ## Req. `<book-page-count>`
>>>
>>> ### Req. `@count`
>>>
>>> Whereas the content can indicate a page-amount akin to `xii + 351`, in `@count` this must be a numerical total page count.


# Sections

Sections are headed by a `<sec>` element, which contains two genres of content:
title information (a `<label>` and a `<title>`) and content (all other elements described).

> ## `<sec>`
> Headed group of material; the basic structural unit of the body of a document.
>
> A section can contain another `<sec>` recursively. If a sections has a chapter number or a similar label, this MUST be placed in a `<label>`.
>
> ### `@id` [See id-syntax](#id-syntax)
>
> Give a Section an ID to refer to it when the manuscript says things like *“See chapter 11”*.
>
> ### `@sec-type`
>
> Required only if the `<sec>` starts with a heading title: this attribute is then used to indicate the heading level. Brill supports a maximum of 8 heading levels, same as in the Brill Typographic Style. Note that this attribute should not be used for any other purpose. The following values are permitted:
>
> - `"heading-1"`
> - `"heading-2"`
> - `"heading-3"`
> - `"heading-4"`
> - `"heading-5"`
> - `"heading-6"`
> - `"heading-7"`
> - `"heading-8"`
>
>> ## `<label>`
>>
>> The Section number, like `2` or `15.5.1` or `XIV.15.A.β.ii`.
>
>> ## Req. `<title>`
>>
>> The actual heading text of the section.
>>
>>> ## `<target/>`
>>>
>>> This element can be used as a marker to indicate the page number in the PDF. Note that this element can also be included below several other elements, as needed. 
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax). 
>>>
>>> The value here should be given as "Page1", "Page2", etc.
>>
>>> ## `<xref>`
>>>
>>> Cross reference a footnote. All `<xref>`s in the text must be clickable in the PDF and online displays. Note that adding footnotes to titles should be avoided if possible.
>>>
>>> ### Req. `@ref-type`
>>>
>>> Supported value:
>>>
>>> - `"fn"`: Refers from a text reference to footnote (`<fn>`). This should include element `<sup>` and the footnote number/marker.
>>>
>>> ### Req. `@rid`
>>>
>>> Contains the `@id` of the element this element points to. If the reference is to more than one element, add all IDs here separated by spaces.
>
>> ## `<table-wrap>`  [See Tables](#tables)
>
>> ## `<table-wrap-group>` [See Tables](#tables)
>
>> ## `<fig>`         [See Figures](#figures)
>
>> ## `<fig-group>`   [See Figures](#figures)
>
>> ## `<disp-quote>`  [See Quotations](#quotations)
>
>> ## `<verse-group>` [See Verse](#verse)
>
>> ## `<def-list>`    [See Definition List](#definition-list)
>
>> ## `<list>`        [See List](#list)
>
>> ## `<p>` 		  [See Paragraphs](#paragraphs)
>
>> ## Recursively `<sec>`
>
>> ## `<boxed-text>`
>>
>> Contains textual material that is part of the body but is outside the flow of the narrative text (whether enclosed in a box or not). For example, marginalia, sidebars, cautions, tips, warnings, note boxes.
>>
>>> ## `<title>`
>>
>>> ## `<p>`
>>
>>> ## `<sec>`
>
>> ## `<mml:math>`
>>
>> The MathML spec is not part of BITS / JATS and readers should to refer to its definition: [MathML](https://www.w3.org/TR/MathML3/). Do not forget to add `@xmlns:mml` to `<book>`.
>
>> ## `<tex-math>`
>>
>> Supply the mathematical formula in (La/Xe)TeX text inside a `<tex-math>`.


# Paragraphs

Paragraphs are `<p>` tags, with similar semantics as the HTML `<p>` tag.
Due to footnotes’ JATS/BITS design, `<p>` tags may contain anything a
`<sec>` may, but please refrain from adding anything but markup tags in
running text.

Do not use `<bold>`, `<italic>` or `<sc>` etc. on places where the Brill Style
supplies its use. Those usages are not semantic uses of small caps style,
i.e. do not supply `<sc>` Small Caps in a `<caption>` Caption element, as
captions—per Brill Typographic Style—are always set in small caps.

> ## `<p>`
>
> A text paragraph.
>
>> ## `<inline-graphic>`
>>
>> Description of and pointer to an external graphic that is displayed or set in the same line as the text.
>> Only use graphics for characters or signs not included in Unicode. Think of Egyptological signs not included in Unicode.
>>
>> ### Req. `@xlink:href`
>>
>> The URI where the resource can be found.
>
>> ## `<xref>`
>> A cross reference to an object within the document (e.g. a table, affiliation or bibliographic citation).
>> It can be used to reference to anything with an `@id`.
>> All `<xref>`s in the text must be clickable in the PDF and online displays.
>>
>> ### Req. `@ref-type`
>>
>> Contains the type of reference. Possible values:
>>
>> - `"aff"`: Refers from a `<contrib>` to an `<aff>`. This element is usually empty.
>> - `"bibr"`: Refers from a cited source to its place in the bibliography (the `<ref>`).
>> - `"fig"`: Refers from a text reference to the actual figure (`<fig>` or `<fig-group>`).
>> - `"fn"`: Refers from a text reference to footnote (`<fn>`). This should include element `<sup>` and the footnote number.
>> - `"table"`: Refers from a text reference to the actual figure (`<table-wrap>` or `<table-wrap-group>`).
>>
>> ### Req. `@rid`
>>
>> Contains the `@id` of the element this element points to.
>> If the reference is to more than one element, add all IDs here separated by spaces.
>
>> ## `<named-content>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#named-content-example)
>>
>> Use this element for a word or subject that has distinct semantics or special significance.
>>
>> ### `@content-type`
>>
>> When necessary, content-type can be:
>>
>> - `"frontispiece"`
>> - `"in-memoriam"`
>> - `"verse-line-number"`
>
>> ## `<bold>`
>>
>> Font bold weight.
>
>> ## `<italic>`
>>
>> Font italic style.
>
>> ## `<sc>`
>>
>> Small Caps. Note that any text surrounded by `<sc>` tags should be included in their proper case (as if they weren't small caps): i.e., any capitals should be included as capitals, *not* lower case, while proper lower case should be included as such. Note also that as per Brill Typographic Style, `<sc>` should *not* be used at all for mixed-case abbreviations.
>
>> ## `<ext-link>`
>>
>> Link to an external file or resource.
>>
>> ### `@xlink:href`
>>
>> The URI where the resource can be found.
>
>> ## `<index-term>`
>>
>> The index term to be embedded into the narrative of a document so that an Index can be created programmatically. Note that any word(s) in the text that this `<index-term>` will be referring to, should also be kept *outside* this element, even if that means repeating the same word(s) again *within* this element.
>>
>> Inline index marking is optional when the index is created manually.
>>
>>> ## `<term>`
>>>
>>> The actual term as it appears in the index (which can be distinct from the word(s) used in the text itself).
>>>
>>> ## Req. `@id` [See id-syntax](#id-syntax)
>>>
>>>> ## Recursively `<index-term>`
>>>>
>>>> Used when the index is multi-level. This should then once again contain `<term>`, etc.
>>>
>>>> ## `<see>`
>>>>
>>>> Generates *See: xyz* entries in a generated index.
>>>
>>>> ## `<see-also>`
>>>>
>>>> Generates *See also: xyz* entries in a generated index.
>
> ## Right-to-Left and Bi-Directional Text
>
> Indicating that a text uses another text direction than left-to-right can be simply indicated by including the text in correct Unicode (UTF-8) *and* including attribute `@xml:lang` for the relevant elements:
>
>> ## `<p>`
>>
>> ### `@xml:lang`
>>
>> Indicates this element (and all sub-elements) are in this particular language. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>
> Additionally, if the text in an element is bi-directional (e.g., the main text in a `<p>` is in Arabic, but certain words or parts of a phrase are in English), then the parts of the text in another direction than the main part of the text within that element can be indicated by the following element:
>
>>> ## `< styled-content >`
>>>
>>> ### `@xml:lang`
>>>
>>> Indicates this element is in this particular language. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>
>> ## `<target/>`
>>
>> This element can be used as a marker to indicate the page number in the PDF. Note that this element can also be included below several other elements, as needed. 
>>
>> ### Req. `@id` [See id-syntax](#id-syntax). 
>>
>> The value here should be given as "Page1", "Page2", etc.


# Footnotes

Footnotes should be included in a `<fn-group>`. Please note that `<fn-group>` can be included as an element directly under `<book-back>` *or* elsewhere in the XML (under `<back>` and other specifically mentioned elements), not in both at the same time.

> ## `<fn-group>`
>
> Container element for footnotes. 
>
>> ## `<title>`
>>
>> Only required for a `<fn-group>` included in the `<book-back>`. In that case, use "Notes" as the title.
>
>> ## Req. `<fn>`
>>
>> A reference to a `<fn>` is made with the [`<xref>`](/elem-xref/) element.
>>
>> ### `@xml:lang`
>>
>> Used if footnote language is different than the main language of the book. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>
>> ### Req. `@id` [See id-syntax](#id-syntax)
>>
>> The identifier that links a `<xref>` with the footnote.
>>
>>> ## Req. `<label>`
>>>
>>> The footnote number. Do not use `<sup>` here.
>>
>>> ## Req. `<p>` [See Paragraphs](#paragraphs)
>>>
>>> The actual text of the footnote is put in one or more `<p>` text paragraphs.


# Tables

For multiple tables (`<table-wrap>` elements) that are to be displayed together, use the `<table-wrap-group>`.

In certain cases, it is permitted to included tables as if they were an image, using the element `<fig>` [See Figures](#figures). If doing so, and the table is in the print version divided over multiple pages, please include it as *one* image here, without repeated heading lines, etc.

> ## `<table-wrap-group>`
>
> Container element for tables.
>
> Container element for tables.
>
>> ## `<table-wrap>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#table-example)
>>
>> Wrapper element for a complete table, including the tabular material (rows and columns), caption (including title), footnotes, and alternative descriptions of the table for purposes of accessibility.
>>
>> ### `@id` [See id-syntax](#id-syntax)
>>
>> A document-unique identifier for the element.
>>
>>> ## `<label>`
>>>
>>> The label for a table-wrap.
>>
>>> ## `<caption>`
>>>
>>>> ## `<title>`
>>>>
>>>> The title for a table-wrap.
>>>
>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>
>>> ## `<table>`
>>>
>>> The rows and columns part of a full table.
>>>
>>> ### `@cellpadding`
>>>
>>> Amount of space, measured in pixels, between the border of a cell and the contents of a table cell.
>>>
>>> ### `@frame`
>>>
>>> Specifies which sides of the table should be given rules, making a box or frame of lines around the table.
>>>
>>>> ## `<thead>`
>>>>
>>>> Container element for the table header rows of a table.
>>>>
>>>>> ## `<tr>`
>>>>>
>>>>> Container element for all the cells in a single table row.
>>>>>
>>>>>> ## `<th>`
>>>>>>
>>>>>> One cell in the table header, as opposed to an ordinary cell in the body of a table.
>>>>>>
>>>>>> ### `@colspan`
>>>>>>
>>>>>> How many columns this element spans, e.g. `<th colspan="2">` is two columns wide.
>>>>>>
>>>>>> ### `@rowspan`
>>>>>>
>>>>>> How many rows this element spans, e.g. `<th rowspan="4">` is four rows high. This attribute is unusual in headers.
>>>>>
>>>>>> ## `<td>`
>>>>>>
>>>>>> One ordinary cell in the body of a table, as opposed to a cell in the table header.
>>>>>>
>>>>>> ### `@colspan`
>>>>>>
>>>>>> How many columns this element spans, e.g. `<td colspan="2">` is two columns wide.
>>>>>>
>>>>>> ### `@rowspan`
>>>>>>
>>>>>> How many rows this element spans, e.g. `<td rowspan="4">` is four rows high.
>>>>>>
>>>>>> ### `@scope`
>>>>>>
>>>>>> Use this with value `"row"` as the first `<td>` when there is a table header on the side of a table.
>>>>
>>>> `<tfoot>`
>>>>
>>>> Container element for the footer rows within a NISO JATS table.
>>>>
>>>> This element comes after a [`<thead>`](/elem-thead/) and before a [`<tbody>`](/elem-tbody/) so that implementation can render the table footer before a (very long) body.
>>>
>>>> `<tbody>`
>>>>
>>>> Container element that holds the rows and columns in one of the body (as opposed to the header) portions of a table.
>>
>>> `<attrib>`
>>>
>>> Attribution information. Do not use `<sc>` tags in this element, as the Small Caps style is part of the
>>> Brill Typographic Style and NOT semantic.


# Figures

Figures can be graphic or textual material. For figures that have to be displayed together,
the element `<fig-group>` is available. For single figures, use the element `<fig>` and follow the instructions below.

Note that QR-codes should also be included in this manner, with the direct image of the QR code linked via `<graphic>` and the relevant URL given in `<caption>`.

> ## `<fig-group>`
>
> Container element for figures that are to be displayed together.
>
>> ## `<caption>`
>>
>> The caption for a fig-group.
>>
>>> ## `<title>`
>>>
>>> The title for a fig-group.
>
>> ## `<fig>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#list-fig-example)
>>
>> Block of graphic or textual material that is identified as a figure, usually bearing a caption and a label such as “Figure 3.” or “Figure”.
>>
>> ### `@id` [See id-syntax](#id-syntax)
>>
>> A document-unique identifier for the element.
>> Starts with `"FIG"`.
>>
>> ### Req. `@orientation`
>>
>> Whether the object should be positioned tall (longe edge vertical `"portrait"`) or wide (long edge horizontal `"landscape"`).
>>
>> - `"portrait"`
>> - `"landscape"`
>>
>> ### `@position`
>>
>> Whether the object must be anchored to its place or may float algorithmically to another location in the document. Also used for margin placement.
>>
>> - `"anchor"` Object must remain on exact location in the text flow.
>> - `"float"` **Default value**. Object may move to a new column, page, end of document, etc.
>> - `"margin"` The object should be placed in the margin or gutter.
>>
>> ### `@fig-type`
>>
>> The type of figure. Supported values:
>>
>> - `"graphic"`
>>
>>> ## `<label>`
>>>
>>> The label for a figure.
>>
>>> ## Req. `<caption>`
>>>
>>> The caption for a figure.
>>>
>>>> ## Req. `<title>`
>>>>
>>>> The title for a figure.
>>>
>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>>
>>>> The paragraph provides explanation.
>>
>>> ## Req. `<graphic>`
>>>
>>> Description of and pointer to an external file containing a still image.
>>>
>>> ### Req. `@xlink:href`
>>>
>>> The URI where the resource can be found.
>>>
>>>> ## Req. `<alt-text>`
>>>>
>>>> Alternate Text Name. Word or phrase used to provide a very short textual name, description, or purpose-statement for a structure such as a graphic or figure. This element is to be included for accessibility purposes and as such is always required. In this element, repeat the text given in the relevant `<caption>``<title>` _unless_ Brill provides a specific alt-text.
>>
>>> ## `<attrib>`
>>>
>>> Attribution information. Do not use `<sc>` tags in this element, as the Small Caps style is part of the
>>> Brill Typographic Style and NOT semantic.


# Quotations

A block quote is in this tag set a `<disp-quote>`. These can be nested, but regularly
contain just a `<p>` and, optionally an `<attrib>` attribution.

> ## `<disp-quote>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#disp-quote-example)
>> ## `<label>`
>>
>> The reference number for the block quote.
>
>> ## `<title>`
>>
>> A title displayed before a block quote.
>
>> ## Nested `<disp-quote>`
>
>> ## Req. `<p>` [See Paragraphs](#paragraphs)
>>
>> The actual text of the quotation is put in one or more `<p>` text paragraphs.
>
>> ## `<attrib>`
>>
>> Attribution information. Do not use `<sc>` tags in this element, as the Small Caps style is part of the
>> Brill Typographic Style and NOT semantic.


# List

Ordered and unordered list alike are put in a `<list>`.

> ## `<list>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#list-fig-example)
>
> Sequence of two or more items, which may or may not be ordered.
>
> ### `@list-type`
>
> - `"order"` Ordered list. Prefix character is a number or a letter, depending on local style.
> - `"bullet"` Unordered or bulleted list. Prefix character is a dash conforming to BTS.
> - `"alpha-lower"` Ordered list. Prefix character is a lowercase alphabetical character.
> - `"alpha-upper"` Ordered list. Prefix character is an uppercase alphabetical character.
> - `"roman-lower"` Ordered list. Prefix character is a lowercase roman numeral.
> - `"roman-upper"` Ordered list. Prefix character is an uppercase roman numeral.
> - `"simple"` Simple or plain list (No prefix character before each item)
>
> ### `@id` [See id-syntax](#id-syntax)
>
> The `@id` attribute supplies a document-internal unique reference to an element.
>
> ### `@continued-from`
>
> To be used if a `<list>` is a continuation from another `<list>` earlier in the document. If used, the earlier `<list>` should include an `@id`, and the value of said `@id` should be used as the value of `@continued-from` here.
>
>> ## `<list-item>`
>>
>> Single item (one entry) in a list of items.
>>
>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>
>>> The actual text of the list-item is put in one or more `<p>` text paragraphs.  


# Definition List

A definition list has a special layout: a word/phrase or term on the one hand is paired with descriptions, explanations or definitions on the other hand.

> ## `<def-list>`
>
> List in which each item consists of two parts: 1) a word, phrase, term, graphic, chemical structure, or equation, that is paired with 2) one or more descriptions, discussions, explanations, or definitions of it.
>
> ### `@list-type`
>
> - `"order"` Ordered list. Prefix character is a number or a letter, depending on local style.
> - `"bullet"` Unordered or bulleted list. Prefix character is a dash conforming to BTS.
> - `"alpha-lower"` Ordered list. Prefix character is a lowercase alphabetical character.
> - `"alpha-upper"` Ordered list. Prefix character is an uppercase alphabetical character.
> - `"roman-lower"` Ordered list. Prefix character is a lowercase roman numeral.
> - `"roman-upper"` Ordered list. Prefix character is an uppercase roman numeral.
> - `"simple"` Simple or plain list (No prefix character before each item)
>
> ### `@list-content`
>
> Type of list, usually with a semantically meaningful name (for example, a “where-list” for the list of the variables that follows an equation, a “notation-list”, or a “procedure-list” for a list of procedural steps to follow).
>
>> ## `<title>`
>>
>> A title displayed before a def-list.
>
>> ## `<term-head>`
>>
>> Heading over the first column (the `term` column) of a `definition list` (two-part list).
>
>> ## `<def-head>`
>>
>> Heading over the second column (the definition column) of a definition list (two-part list).
>
>> ## `<def-item>`
>>
>> One item in a definition (two-part) list.
>>
>>> ## `<term>`
>>>
>>> Word, phrase, graphic, chemical structure, equation, or other noun that is being defined or described.
>>> This element is typically used in conjunction with a definition in a `definition list` (two-part list). The term occupies the first column of the two-part list and is the subject of the definition or description, which occupies the second column.
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>
>>> A document-unique identifier for the element.
>>
>>> ## `<def>`
>>>
>>> Definition, explanation, or expansion of a word, phrase, or abbreviation.
>>>
>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>>
>>>> The actual text of the definition is put in one or more `<p>` text paragraphs.  


# Glossary

A glossary typically contains one or more def-lists. If a description is needed before the list begins, a `<p>` may be included as well. The glossary is part of the back matter of a document.

> ## `<glossary>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#glossary-example)
>
>> ## `<label>`
>>
>> Number and/or prefix word placed at the beginning of display elements. For example "Chapter 1", "Introduction:", "Conclusion:", "Appendix:", etc. 
>
> ### `@id` [See id-syntax](#id-syntax)
>
> A document-unique identifier for the element.
>
>> ## `<title>`
>>
>> A title displayed before a glossary.
>
>> ## `<p>` [See Paragraphs](#paragraphs)
>>
>> For any descriptive text preceding the Glossary.
>
>> ## `<def-list>` [See Definition List](#definition-list)


# Verse

Verse, like poetry or quoted material where line numbers are important, use `<verse-group>`.
Line numbers however cannot be tagged separately in a `<label>` according to the BITS/JATS DTD, so instead
please use `<named-content content-type="verse-line-number">`.

> ## `<verse-group>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#verse-group-example)
>> ## `<label>`
>>
>> The reference number for the verse. This may be referred to in the text.
>
>> ## `<title>`
>>
>> The title of the verse, e.g. `Sonnet 18` or `Shall I compare thee to a summer’s day?`.
>
>> ## `<verse-line>`
>>
>> One line of a poem or verse.
>
>> ## `<attrib>`
>>
>> Attribution information. Do not use `<sc>` tags in this element, as the Small Caps style is part of the
>> Brill Typographic Style and NOT semantic.


# Reference List

A reference list is a list of bibliographic references for a document or a document component. It contains one or more ref elements. Please note that `<ref-list>` is only allowed in `<back>` and/or in `<book-back>`.

> ## `<ref-list>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#reference-list-example)<!--(/example-article-body)-->
>
> Main element for Reference List. Note that this element can be used recursively (so a `<ref-list>` can contain (multiple) other instance(s) of `<ref-list>`) to include multiple distinct reference lists (e.g., "Primary Sources", "Secondary Literature", etc.). 
>
>> ## `<title>`
>>
>> The title of the ref-list, e.g. `References`.
>
>> ## `<p>` [See Paragraphs](#paragraphs)
>>
>> For any descriptive text preceding the Reference List.
>
>> ## `<ref>`
>>
>> One item in a bibliographic list.
>>
>> ### Req. `@id` [See id-syntax](#id-syntax)
>>
>> A document-unique identifier for the element.
>>
>>> ## `<label>`
>>>
>>> The reference number for a reference. This may be referred to in the text.
>>
>>> ## `<mixed-citation>`
>>>
>>> Bibliographic description of a work. Includes a combination of bibliographic reference elements and untagged text. Spacing and punctuation are preserved. 
>>> Note that any punctuation that is *not*  itself part of the proper value of an element, but is dictated only by the reference style, should not be included in the element itself. E.g., if reference style dictates that a journal article title should be surrounded by double quotation marks ("), but these are not normally part of the article title itself, then these double quotation makrs should *not* be included within the element `<source>`, but instead directly outside it.
>>> Similarly, if the Reference list uses an author date style that leads to dates such as "2018a", "2018b", then only the year should be placed inside the element `<year>`, with the "a" or "b" directly outside it. For example: `<year>`2018`</year>`a.
>>>
>>> ### Req. `@publication-type`
>>>
>>> Supported values *major, need to be tagged fully if used*:
>>>
>>> - `"book"`
>>> - `"journal"`
>>> - `"web"`
>>>
>>> In case of `"web"`, please add `<uri>`
>>>
>>> Supported values *minor, full tagging is not needed*:
>>>
>>> - `"data"`
>>> - `"list"`
>>> - `"other"`
>>> - `"patent"`
>>> - `"thesis"`
>>> - `"commun"` for communiqués
>>> - `"confproc"` for conference proceedings.
>>> - `"discussion"`
>>> - `"gov"`
>>>
>>>> ## `<person-group>`
>>>>
>>>> Container element for one or more contributors in a bibliographic reference.
>>>>
>>>> ### `@person-group-type`
>>>>
>>>> Specify authors and other contributors to a reference. A separate `<person-group>` element should be used for each role. Supported values are:
>>>>
>>>> - `"all-authors"`
>>>> - `"assignee"`
>>>> - `"author"`
>>>> - `"compiler"`
>>>> - `"curator"`
>>>> - `"director"`
>>>> - `"editor"`
>>>> - `"guest-editor"`
>>>> - `"inventor"`
>>>> - `"transed"`
>>>> - `"translator"`
>>>>
>>>>> ## `<string-name>`
>>>>>
>>>>> Container element for components of personal names *unstructured*
>>>>>
>>>>>> ## `<surname>`
>>>>>>
>>>>>> Surname of a person.
>>>>>
>>>>>> ## `<given-names>`
>>>>>>
>>>>>> All given names of a person, such as the first name, middle names, maiden name if used as part of the married name, etc. This element will NOT be repeated for every given name a person has.
>>>>>
>>>>>> ## `<prefix>`
>>>>>>
>>>>>> Honorifics or other qualifiers that usually precede a person’s name (for example, Professor, Rev., President, Senator, Dr., Sir, The Honorable).
>>>>>
>>>>>> ## `<suffix>`
>>>>>>
>>>>>> Qualifiers that follow a person’s name (for example, Sr., Jr., III, 3rd). 
>>>
>>>> ## `<collab>`
>>>>
>>>> Group of contributors credited under a single name; includes an organization credited as a contributor.
>>>
>>>> ## `<article-title>`
>>>>
>>>> Full title of an article or other journal component, such as a book review. When the cited document is a book-chapter, use:
>>>
>>>> ## `<chapter-title>`
>>>>
>>>> Title of a cited book chapter.
>>>
>>>> ## Req. `<source>`
>>>>
>>>> Title of a document (for example, journal, book, conference proceedings) that contains (is the source of) the material being cited in a bibliographic reference or product.
>>>
>>>> ## `<date>`
>>>>
>>>>> ## `<year>`
>>>>>
>>>>> A year, in (zero-padded) digits.
>>>
>>>> ## `<publisher-name>`
>>>>
>>>> ## `<publisher-loc>`
>>>
>>>> ## `<volume>`
>>>
>>>> ## `<issue>`
>>>
>>>> ## `<fpage>`
>>>
>>>> ## `<lpage>`
>>>
>>>> ## `<uri>`


# Index

Structural, multilevel index, typically used for capturing a back-of-the-book.
This element does not occur in NISO JATS.

> ## `<index-group>`
>
> Optional element. Only use when the book has multiple, distinct indexes, for example: an Index of Terms *and* an Index of Names.
>
>> ## `<index-title-group>`
>>
>> Optional element. Only to be used here if there is an overarching title for the group of indexes.
>>
>>> ## `<title>`
>>>
>>> The title displayed before the group of indexes.
>
>> ## Req. `<index>`
>>
>>> ## Req. `<index-title-group>`
>>>
>>> Container element to hold the various titles for a structured Index (or a subdivision of such an Index)
>>>
>>>> ## Req. `<title>`
>>>>
>>>> A title displayed before an index.
>>
>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>
>>> For any descriptive text preceding the Index.
>>
>>> ## `<index-div>`
>>>
>>> Optional element. Division, typically for the purposes of display, in a structural index, for example, a division holding the heading "A" and all of the initial alphabetically arranged index entries.
>>>
>>>> ## `<index-title-group>`
>>>>> ## `<title>`
>>>>>
>>>>> The title of a division in an index.
>>>
>>>> ## Req. `<index-entry>`
>>>>
>>>> One entry, with all its subdivisions, see and see also terms, and navigational pointers (if any) in a structural index. Can also be included directly under `<index>`.
>>>>
>>>>> ## Req. `<term>`
>>>>>
>>>>> The actual term used in the index.
>>>>
>>>>> ## `<nav-pointer-group>`
>>>>>
>>>>> Optional element: to be used if there are multiple `<nav-pointer>`s for the same `<index-entry>`.
>>>>>
>>>>>> ## `<nav-pointer>`
>>>>>>
>>>>>> The cross-reference-like element used to point from an Index Entry (`<index-entry>`) to the place in the text being referenced. Can also be included directly under `<index-entry>` without `<nav-pointer-group>`.
>>>>>>
>>>>>> ### Req. `@rid`
>>>>>>
>>>>>> Contains the `@id` of the `<index-term>` this element points to.
>>>>
>>>>> ## `<see-also-entry>`
>>>>>
>>>>> Used in an embedded index entry to hold preferred terms that make `See Also` references in a generated index. Typically used alongside `<nav-pointer>`s for the current `<index-entry>`.
>>>>
>>>>> ## `<see-entry>`
>>>>>
>>>>> Used in an embedded index entry to hold preferred terms that make `See Instead` references in a generated index. Typically used without `<nav-pointer>`s for the current `<index-entry>`.
>>>>
>>>>> ## `<x>`
>>>>>
>>>>> To be used for punctuation or text outside of any other sub-elements of `<term>`. May also be used to include page numbers in a non-generated (manually produced) index.


# id-syntax

> Each `@id` is unique within the XML document. Brill has defined the following `@id` syntax:
> 
> Pre-determined capital letter(s) + six numerical characters. 
> 
> E.g., `<sec id="S000001" sec-type="heading-1">`
> 
> Note that numbering does not need to be consecutive, just that entire `@id` needs to be unique within the XML document.
> 
> The following pre-determined `@id` capital letters are supported:
> 
> - AFF : `<aff>`
> - APP : `<book-app>`
> - AWG : `<award-group>`
> - BP : `<book-part>` 
> - FM : for any main element directly below `<front-matter>` 
> - FN : `<fn>` 
> - GL : `<glossary>` 
> - IMG : `<fig>` 
> - L : `<list>` 
> - P : `<p>`
> - Page : `<target>`
> - R : `<ref>` 
> - S : `<sec>` 
> - T : `<table-wrap>`
> - TRM : `<term>` 
