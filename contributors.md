# Contributors

Contributors to Brill BITS and JATS: 

* [Tom Weterings](weterings@brill.com)
* [Frans Havekes](havekes@brill.com)
* Apolline Rinaudo
* Imre Zevenhuizen
* Leonieke  Aalders
* Redmer Kronemeijer
* Lianne Heslinga


Documentation of the Brill Standard of BITS and JATS: The Book and Journal XML standards designed to work for publishers and typesetters.

