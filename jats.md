# JATS for Journal Articles

A journal issue is built up from articles, each article is a file with an `<article>` XML-root-element. The article metadata, like the title and authors, and the journal it appears in, with a title, series information etc. are all included in this `<article>` element.

This document describes a subset of JATS specific for Brill journals. See [jats.nlm.nih.org](http://jats.nlm.nih.gov/publishing/tag-library/1.1/index.html) for NLM’s own description of the JATS standard. Their description is normative. This document describes a subset to help uniformity in Brill Journal Article XML. 

> ## Req. `<!DOCTYPE>` 
>
> Always add the following `<!DOCTYPE>` declaration to Brill journal articles:
>
> `<!DOCTYPE article PUBLIC "-//NLM//DTD JATS (Z39.96) Journal Publishing DTD v1.1 20151215//EN" "https://jats.nlm.nih.gov/publishing/1.1/JATS-journalpublishing1.dtd">`
>
> ## Req. `<article>`  [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#article-example)
>
> A journal article, like a typical research article, but also book and product reviews, editorials, commentaries and other content. These types are specified in `@article-type`.
>
> This is the root element for journal content.
>
> ### Req. `@article-type`.
>
> Kind or type of the article. The author or Brill will notify the typesetter of this value. Usually, it is `"research-article"`.
>
> Brill supports all 29 values for `@article-type` that NLM prescribes: `"abstract"`, `"addendum"`,
`"announcement"`, `"article-commentary"`, `"book-review"`, `"books-received"`, `"brief-report"`,
`"calendar"`, `"case-report"`, `"collection"`, `"correction"`, `"discussion"`, `"dissertation"`,
`"editorial"`, `"in-brief"`, `"introduction"`, `"letter"`, `"meeting-report"`, `"news"`,
`"obituary"`, `"oration"`, `"partial-retraction"`, `"product-review"`, `"rapid-communication"`,
`"reply"`, `"reprint"`, `"research-article"`, `"retraction"`, `"review-article"`, `"translation"`.
>
> Optionally, with Brill permission, a typesetter may use `"other"` as a value for this attribute.
>
>
> **Please Note**: for any files that include one of the following values for `@article-type`:
>
> `"correction"`, `"editorial"`, `"introduction"`, `"obituary"`, `"partial-retraction"`, or `"retraction"`
>
> the element `<ali:free_to_read xmlns:ali="http://www.niso.org/schemas/ali/1.0/"/>` is **Required** under `<permissions>` (as explained in more detail below).
>
>
> ### Req. `@xmlns:xlink`
> XML namespace declaration.
>
> ### `@xmlns:mml`
> XML namespace declaration. Use these *only* if the article contains an element from the MathML namespaces. Brill uses MathML3: [MathML](https://www.w3.org/TR/MathML3/).
>
> - `xmlns:mml="http://www.w3.org/1998/Math/MathML"`
>
> ### Req. `@xml:lang`
> Indicates the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>
> ### Req. `@dtd-version`
> This attribute is required and always contains `"1.1"`.
>
> ### `@specific-use="prepub"`
> For all _Advance Articles_, add `@specific-use="prepub"` to the root element. Do not add `<volume>` or `<issue>`.
>
>> ## Req. `<front>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#journal-header-example)
>>
>>> ## Req. `<journal-meta>`
>>>
>>>> ## Req. `<journal-id>`
>>>>
>>>> Brill uses the E-ISSN as identifier; the type of the identifier is indicated using the `@journal-id-type` attribute. Contents come from a Master List that Brill will provide typesetters. This element is **required** twice, once with a `@journal-id-type="eissn"` and once with `@journal-id-type="publisher-id"`. Note that the hyphen in an ISSN should always be included.
>>>>
>>>> ### Req. `@journal-id-type`
>>>>
>>>> Type of journal identifier or the authority that created a particular journal identifier. Supported values:
>>>>
>>>> - `"eissn"`: Include the journal’s E-ISSN in this tag.
>>>> - `"publisher-id"`: Include the journal abbreviation in this tag.
>>>
>>>> ## Req. `<journal-title-group>`
>>>>
>>>> Contents come from a Master List that Brill will provide typesetters.
>>>>
>>>>> ## Req. `<journal-title>`
>>>>>
>>>>> Full title of the journal in which the article was published.
>>>>
>>>>> ## `<journal-subtitle>`
>>>>>
>>>>> This is not an issue title. Issue titles are described in the article metadata. This is a *permanent* subtitle to a journal.
>>>>
>>>>> ## `<trans-title-group>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#trans-title-group-example)
>>>>>
>>>>> For every translation of the journal title, supply a `<trans-title-group>`.
>>>>>
>>>>> ### Req. `@xml:lang`
>>>>>
>>>>> Indicates the language of the translated title. This attribute should be included here and *not* on the sub-elements. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>>
>>>>>> ## `<trans-title>`
>>>>>>
>>>>>> The translation of the title.
>>>>>
>>>>>> ## `<trans-subtitle>`
>>>>>>
>>>>>> The translation of the subtitle.
>>>>
>>>>> ## Req. `<abbrev-journal-title>`
>>>>>
>>>>> Short form of the title of the journal in which an article is published. Contents come from a Master List that Brill will provide typesetters.
>>>>>
>>>>> ## Req. `@abbrev-type`
>>>>>
>>>>> Supported values:
>>>>> - `"ltwa"`
>>>
>>>> ## Req. `<issn>`
>>>>
>>>> Brill requires that both the E-ISSN and ISSN are always used. Contents come from a Master List that Brill will provide typesetters. In case a journal does not have a ISSN, Brill will let the typesetters know. Note that the hyphen in an ISSN should always be included.
>>>>
>>>> ### Req. `@publication-format`
>>>>
>>>> Indicate whether this ISSN applies to a print or an online version. Supported values:
>>>>
>>>> - `"print"`
>>>> - `"online"`
>>>
>>>> ## `<isbn>`
>>>>
>>>> International Standard Book Number, the international code for identifying a particular product form or edition of a publication, typically a monographic publication.
>>>>
>>>> The code is always set without spaces or hyphens.
>>>>
>>>> ### Req. `@publication-format`
>>>>
>>>> Indicate whether this ISBN applies to a print or an online version. Supported values:
>>>>
>>>> - `"print"`
>>>> - `"online"`
>>>
>>>> ## Req. `<publisher>`
>>>>> ## Req. `<publisher-name>`
>>>>>
>>>>> Name of the imprint associated with the article or book. Brill will let the typesetters know which imprint to use for the journal.
>>>>>
>>>>> Currently, for `<publisher-name>` only the text values below are permitted. (NOTE:  historically, other values may exist and should be retained if such files are redelivered, unless expressly noted otherwise.)
>>>>>
>>>> - `Brill`
>>>> - `Brill | Nijhoff`
>>>> - `Brill | Hotei`
>>>> - `Brill | Sense`
>>>> - `Brill | Schöningh`
>>>> - `Brill | Fink`
>>>> - `Brill | mentis`
>>>> - `Vandenhoeck & Ruprecht`
>>>> - `Böhlau Verlag`
>>>> - `V&R Unipress`
>>>>
>>>>> ## Req. `<publisher-loc>`
>>>>>
>>>>> Use the location as given on the Title Page of the journal prelims. If the publisher has more than one location (`Leiden | Boston`), this still has to specified in one element.
>>>>>
>>>>> ### `@specific-use`
>>>>>
>>>>> Supported values are:
>>>>>
>>>>> - `"online"`
>>>>> - `"print"`
>>
>>> ## Req. `<article-meta>`
>>>
>>> Information concerning the article that identifies or describes the article.
>>>
>>>> ## Req. `<article-id>`
>>>>
>>>> Unique external identifier assigned to an article. Note that for the DOI, any alphabetic characters within the DOI itself should _always_ be included as lower-case in the XML file.
>>>>
>>>> ### Req. `@pub-id-type`
>>>>
>>>> Type of publication identifier or the organization or system that defined the identifier.
>>>>
>>>> - Req. `"doi"`
>>>> - `"EM"` This is the Editorial Manager manuscript number. Always include if provided.
>>>
>>>> ## `<article-categories>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#Article-categories-example)
>>>>
>>>>> ## `<subj-group>`
>>>>>
>>>>> ### `@subj-group-type`
>>>>>
>>>>> Type of subject group.
>>>>>
>>>>> For some journals, articles are grouped into categories which this Tag Set calls `<subject>`s, which may be grouped into `<subj-group>`s. These subject categories are typically shown in the Table of Contents, at the top of the first print or display page, or on the web splash page for an article.
>>>>> The Brill Desk Editor will provide these when needed.
>>>>>
>>>>>> ## `<subject>`
>>>>>>
>>>
>>>> ## Req. `<title-group>`
>>>>
>>>>> ## Req. `<article-title>`
>>>>>
>>>>> Full title of an article or other journal component, such as an editorial or book review.
>>>>
>>>>> ## `<alt-title>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#alt-title-example)
>>>>>
>>>>> An alternative or different version of the title of a work.
>>>>>
>>>>> ### Req. `@alt-title-type`
>>>>>
>>>>> Reason or purpose for a (shorter) title to be included. Brill supports 2 values for `@alt-title-type`:
>>>>>
>>>>> - `"toc"`: Title to be used in the table of contents.
>>>>> - `"running-head"`: Title to be used as the running head. This is a shorter version of the regular title. An `<alt-title>` with this type is optional.
>>>>>
>>>>> A `<alt-title alt-title-type="toc">` is always required to be in a `<title-group>`, but does not need to be a shorter version of the regular title.
>>>>
>>>>> ## `<subtitle>`
>>>>>
>>>>> Subordinate part of a title for a document or document component. Refer to the manuscript to see what the author meant as subtitle.
>>>>
>>>>> ## `<trans-title-group>`
>>>>>
>>>>> For every translation of the article title, supply a `<trans-title-group>`.
>>>>>
>>>>> ### Req. `@xml:lang`
>>>>>
>>>>> Indicates the language of the translated title. This attribute should be included here and *not* on the sub-elements. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>>
>>>>>> ## `<trans-title>`
>>>>>>
>>>>>> The translation of the title.
>>>>>
>>>>>> ## `<trans-subtitle>`
>>>>>>
>>>>>> The translation of the subtitle.
>>>
>>>> ## `<contrib-group>`
>>>>
>>>> Grouping of one or more contributors and information about those contributors. All types of contributors are grouped in one `<contrib-group>`. Do not add this tag group if an article does not have an author. Note that at most *one* `<contrib-group>` is allowed in `<article-meta>`.
>>>>
>>>>> ## Req. `<contrib>`
>>>>>
>>>>> A contributor: Container element for information about a single author, editor, or other contributor.
>>>>>
>>>>> If a contribution is without author, do not add `<contrib>`.
>>>>>
>>>>> ### Req. `@contrib-type`
>>>>>
>>>>> Type of contribution made by the contributor. Brill supports following contribution types:
>>>>>
>>>>> - `"author"`
>>>>> - `"editor"`
>>>>> - `"translator"`
>>>>>
>>>>> ### `@corresp`
>>>>>
>>>>> This atribute is **required** in case the article has multiple authors. By default this attribute should be assigned to the first listed contributor, _unless_ another contributor is specifically identified as Corresponding Author. Do not include in all other cases. Supported values:
>>>>>
>>>>> - `"yes"`
>>>>>
>>>>> ### `@deceased`
>>>>>
>>>>> Use if the contributor was deceased when the document was published. Supported values: 
>>>>>
>>>>> - `"yes"`
>>>>> - `"no"` Default value, which makes this attributes unnecessary.
>>>>>
>>>> ### `@equal-contrib`
>>>>
>>>> Indicates whether or not all contributors contributed equally. If the contributor whom this attribute modifies contributed equally with all other contributors, this attribute should be set to “yes”; if his/her contribution was greater or lesser, then this attribute should be set to “no”.
>>>>
>>>> - `"yes"`
>>>> - `"no"`
>>>> @equal-contrib is an optional attribute; there is no default.
>>>>
>>>>>> ## `<contrib-id>`
>>>>>>
>>>>>> An ORCID identifier for a contributor. Note that an ORCID identifier should include the complete URL, for example: https://orcid.org/0000-0002-1825-0097
>>>>>>
>>>>>> ### Req. `@contrib-id-type`
>>>>>>
>>>>>> - `"orcid"`
>>>>>>
>>>>>> ### Req. `@authenticated`
>>>>>>
>>>>>> - `"true"`
>>>>>
>>>>>> ## `<collab>`
>>>>>>
>>>>>> Group of contributors credited under a single name; includes an organization credited as a contributor. If `<collab>` is used, `<name>` is not required.
>>>>>>
>>>>>
>>>>>> ## Req. `<name>`
>>>>>>
>>>>>> Container element for components of personal names, such as a `<surname>`. Use `<name-alternatives>` if the name has multiple variants (e.g., in different scripts).
>>>>>>
>>>>>> ### `@name-style`
>>>>>>
>>>>>> Style of processing requested for a structured personal name.
>>>>>>
>>>>>> - `"eastern"`: The name will both be displayed and sorted/inverted with the family name preceding the given name.
>>>>>> - `"western"`: The name will be displayed with the given name preceding the family name, but will be sorted/inverted with the family name preceding the given name.
>>>>>> - `"given-only"`: The single name provided is a given name, not a family name/surname. The single name will be both the sort and the display name.
>>>>>>
>>>>>> Default value is `"western"`. If an author has only a single name, `name-style="given-only"` and tag the single name with `<given-names>`.
>>>>>>
>>>>>>> ## Req. `<surname>`
>>>>>>>
>>>>>>> Surname of a person.
>>>>>>
>>>>>>> ## `<given-names>`
>>>>>>>
>>>>>>> All given names of a person, such as the first name, middle names, maiden name if used as part of the married name, etc. This element will NOT be repeated for every given name a person has.
>>>>>>
>>>>>>> ## `<prefix>`
>>>>>>>
>>>>>>> Honorifics or other qualifiers that usually precede a person’s name (for example, Professor, Rev., President, Senator, Dr., Sir, The Honorable).
>>>>>>
>>>>>>> ## `<suffix>`
>>>>>>>
>>>>>>> Qualifiers that follow a person’s name (for example, Sr., Jr., III, 3rd).
>>>>>
>>>>>> ## `<name-alternatives>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#String-name-Example)
>>>>>>
>>>>>> If a contributor’s name has multiple variants, use this element to include *all* variants. Put the variants in the same order as they would be on the printed page.
>>>>>>
>>>>>> The contents of this element are identical to the above description of `<name>`.
>>>>>>
>>>>>>> ## `<name>`
>>>>>>>
>>>>>>> ### `@xml:lang`
>>>>>>>
>>>>>>> Indicates the language of the name, if in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>>>>
>>>>>>>> ## `<surname>`
>>>>>>>
>>>>>>>> ## `<given-names>`
>>>>>>>
>>>>>>>> ## `<prefix>`
>>>>>>>
>>>>>>>> ## `<suffix>`
>>>>>>
>>>>>>> ## `<string-name>`
>>>>>>>
>>>>>>> ### `@xml:lang`
>>>>>>>
>>>>>>> Indicates the language of the name, if in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>>
>>>>>> ## `<email>`
>>>>>>
>>>>>> The contributor's email address.
>>>>>>
>>>>>> ### `@xlink:href`
>>>>>> The e-mail address as a `mailto:` URI.
>>>>>>
>>>>>
>>>>>> ## `<xref/>`
>>>>>>
>>>>>> A cross reference to an affiliation or to a footnote.
>>>>>>
>>>>>> ### Req. `@ref-type`
>>>>>>
>>>>>> Supported values:
>>>>>>
>>>>>> - `"aff"` 
>>>>>> - `"fn"`
>>>>>>
>>>>>> ### Req. `@rid`
>>>>>>
>>>>>> Contains the `@id` of the affiliation this contributor is associated with, or of the associated author-notes. If the reference is to more than one element, add all IDs here separated by spaces.
>>>>
>>>>> ## `<aff>`
>>>>>
>>>>> Element used to include an affiliation. Note that for every new affiliation, a separate `<aff>` element should be included.
>>>>>
>>>>> ### Req. `@id` [See id-syntax](#id-syntax)
>>>>>
>>>>> Unique identifier for the affiliation, which is used in conjunction with `<xref ref-type="aff" rid=""/>` to refer to this affiliation.
>>>>>
>>>>>> ## `<institution-wrap>`
>>>>>>
>>>>>>> ## Req.`<institution>`
>>>>>>>
>>>>>>> Name of an institution or organization, like `Universiteit Leiden` or `Universiteit van Amsterdam`.
>>>>>>>
>>>>>>> This information is provided by the author from EM information, but must be checked. This element can also contain the name of the department, like `Leiden University Centre for Linguistics`. Provide this value with a `@content-type="dept"`.
>>>>>>>
>>>>>>> ### `@content-type`
>>>>>>>
>>>>>>> Use this attribute to specify the extra institution information. Supported values:
>>>>>>>
>>>>>>> - `"dept"`
>>>>>>> - More values can come from EM and should not be removed.
>>>>>>
>>>>>>> ## `<institution-id>`
>>>>>>>
>>>>>>> A externally defined institutional identifier, from an established identifying authority (for example, “Ringgold”).
>>>>>>>
>>>>>>> ### Req. `@institution-id-type`
>>>>>>>
>>>>>>> Brill only supports Ringgold identifiers for institutions as part of an affiliation. Supported values:
>>>>>>>
>>>>>>> - `"ringgold"`
>>>>>
>>>>>> ## Req. `<country>`
>>>>>>
>>>>>> The country the institution is located in. This is represented separately from an `<addr-line>`. EM provides this information.
>>>>>
>>>>>> ## `<addr-line>`
>>>>>>
>>>>>> An text line in an address. EM provides this information.
>>>>>>
>>>>>> ### `@content-type`
>>>>>>
>>>>>> Indicates what type of address line information it is. EM provides this information. Supported values include:
>>>>>>
>>>>>> - `"city"`
>>>>>> - `"zipcode"`
>>>>>> - and more supported by EM.
>>>
>>>> ## Req. `<pub-date>`
>>>>
>>>> ### Req. `@publication-format`
>>>>
>>>> - Req. `"online"`. 
>>>>
>>>> ### Req. `@date-type`
>>>>
>>>> - Req. `"article"`. **Note**: a `<pub-date>` with this attribute is _always_ required and should include the date of original delivery of the XML for the article to Brill. If the article is originally published in Advance, and later republished in an issue, the original date assigned when the Advance version was delivered should be retained here (i.e., it should **never** be changed). This date should always be included as the first `<pub-date>` in the XML.
>>>> - `"issue"`. **Note**: a `<pub-date>` with this attribute _must_ be included _in addition to_ the `"article"` pub-date for all articles appearing in an issue. The date to be included should be the date of delivery of the issue XML package to Brill, and again once the issue is published this date should **never** be changed again. Note that this means that for any articles that are first published online as part of an issue, both  `"article"` and `"issue"` will be the same date, while articles previously published in Advance will have a different `"article"` and `"issue"` dates.
>>>>
>>>>> ## Req. `<day>`
>>>>>
>>>>> A day, in (zero-padded) digits.
>>>>
>>>>> ## Req. `<month>`
>>>>>
>>>>> A month, in (zero-padded) digits.
>>>>
>>>>> ## Req. `<year>`
>>>>>
>>>>> A year, always in 4 digits. Please note that only a *single* year may ever be included, i.e., "2018". Double years like "2017-2018" are not allowed.
>>>
>>>> ## Req. `<volume>`
>>>> Number of a journal (or other document) within a series. Note that this element should **NOT** be included in case the article is published as Advance.
>>>>
>>>> `@content-type="number"` and `@content-type="year"` are always required in two separate `<volume>` tags.
>>>>
>>>> ### Req. `@content-type`
>>>>
>>>> Brill supports 2 values for `@content-type` with `<volume>`.
>>>>
>>>> - Req. `"number"`, use same digit layout as for `<issue>`
>>>> - Req. `"year"`, always in 4 digits. Please note that only a *single* year may ever be included, i.e., "2018". Double years like "2017-2018" are not allowed.
>>>
>>>> ## Req. `<issue>`
>>>> Issue number of a journal. Note that this element should **NOT** be included in case the article is published as Advance.
>>>>
>>>> - No leading zeros.
>>>> - The dash between two numerals is always `-` (U+002D HYPHEN-MINUS).
>>>>
>>>> | Text issue number | Put this in tags |
>>>> | ----------------- | ---------------- |
>>>> |             1     | 1                |
>>>> |             10    | 10               |
>>>> |             1-2   | 1-2              |
>>>> |             1-12  | 1-12             |
>>>> |           1, 2, 3 | 1-3              |
>>>> |             05-08 | 5-8              |
>>>
>>>> ## `<issue-title>`
>>>>
>>>> This element can be used to include the theme or special issue title for a journal or BRP issue. Guest Editor(s) can be added as well by including "edited by [name(s)]". Volume and Issue number(s) should *not* be included in this element.
>>>>
>>>>
>>>> Example: `<issue-title>`Special Issue: Questions about trans-thoroughfare Fowl Ambulation, edited by A. de Boer and B. Bauer`</issue-title>`
>>>
>>>> ## Req. `<fpage>`
>>>>
>>>> Starting *(first)* page number of the article.
>>>>
>>>> ### Req. `@specific-use`
>>>>
>>>> This attribute’s value is always `"PDF"`.
>>>>
>>>> ### Req. `@seq`
>>>>
>>>> A sequential number starting at 1 per issue that increments for each article in it. This sorts the articles in an issue.
>>>> For Advance Articles always use `@seq=1`.
>>>
>>>> ## Req. `<lpage>`
>>>>
>>>> Final *(last)* page number of an article.
>>>
>>>> ## `<page-range>`
>>>>
>>>> Text describing discontinuous pagination (for example, 8-11, 14-19, 40).
>>>
>>>> ## `<supplementary-material/>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#supplementary-material example)
>>>> A pointer to external resources that support the article or book, but which are not part of the content of the work.
>>>>
>>>> ### Req. `@xlink:href`
>>>> Will be supplied by the Brill Desk Editor.
>>>>
>>>> Supported values are:
>>>>
>>>> - figshare DOI
>>>> - supplement filename
>>>>
>>>> ### Req. `@specific-use`
>>>> Will be supplied by the Brill Desk Editor.
>>>>
>>>> - `"figshare"`
>>>> - `"local"`
>>>>
>>>> ### `@mimetype`
>>>> Only to be used when `@specific-use="local"`. Supported values can be found here: [https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types). If the value to be used is unclear, consult the Brill Production Editor.
>>>>
>>>>> #### `<caption>`
>>>>> Only when `@specific-use="local"`
>>>>>
>>>>>> ##### Req. `<title>`
>>>>>> Add the title of the supplementary file, will be supplied by the Brill Desk Editor.
>>>
>>>> ## `<history>`
>>>>
>>>> This container element describes the processing history of the journal article, e.g. dates received and accepted.
>>>>
>>>>> ## `<date>`
>>>>>
>>>>> These date elements should at least provide a `<year>` and only when that information is available, also the `<month>` and `<day>` in two digit numeral values, too.
>>>>>
>>>>> ### Req. `@date-type`
>>>>>
>>>>> Event in the lifecycle of an article that this date is marking (for example, the date the manuscript was received or accepted, the date the electronic preprint was published, or the date of any revision or retraction).
>>>>> Supported values include:
>>>>>
>>>>> - `"received"`. Date that article was received.
>>>>> - `"initial-decision"`. Date that initial decision was taken on article.
>>>>> - `"rev-recd"`. Date that a revised version of the article was received. There may be more than one `@date-type` with this value.
>>>>> - `"rev-request"`. The date revisions were requested. There may be more than one `@date-type` with this value.
>>>>> - `"accepted"`. Date that article was accepted in journal.
>>>>> - `"version-of-record"`. Date that the version of record of the article was published (online or print).
>>>>> - `"pub"`. The publication date (online or print).
>>>>> - `"preprint"`. Preprint dissemination date (online or print).
>>>>> - `"retracted"`. The date an article was retracted.
>>>>> - `"corrected"`. The date an article was corrected.
>>>>>
>>>>>> ## `<day>`
>>>>>>
>>>>>> A day, in (zero-padded) digits.
>>>>>
>>>>>> ## `<month>`
>>>>>>
>>>>>> A month, in (zero-padded) digits.
>>>>>
>>>>>> ## Req. `<year>`
>>>>>>
>>>>>> A year, in (zero-padded) digits.
>>>>>
>>>
>>>> ## Req. `<permissions>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#Permissions-Examples)
>>>>
>>>>> ## Req. `<copyright-statement>`
>>>>>
>>>>> Copyright notice or statement, suitable for printing or display.
>>>>
>>>>> ## Req. `<copyright-year>`
>>>>>
>>>>> The year of copyright. This may not be displayed.
>>>>
>>>>> ## Req. `<copyright-holder>`
>>>>>
>>>>> Name of the organizational or person that holds a copyright.
>>>>
>>>>> ## Req. `<license>`
>>>>>
>>>>> A license. Set of conditions under which the content may be used, accessed, and distributed.
>>>>>
>>>>> ### `@license-type`
>>>>>
>>>>> - `"ccc"`. Standard Brill license type. See example for full text.
>>>>> - `"open-access"`. Any Open Access license. If used, `@xlink:href` and `@xlink:title` are Required.
>>>>>
>>>>> ### `@xlink:href`
>>>>> The URL at which the license text can be found. See [Creative Commons Licenses](https://creativecommons.org/licenses/).
>>>>>
>>>>> ### `@xlink:title`
>>>>> If used with a Creative Commons license. Corresponds to `@xlink:href`. Supported values:
>>>>>
>>>>> - `"CC-BY"`
>>>>> - `"CC-BY-SA"`
>>>>> - `"CC-BY-ND"`
>>>>> - `"CC-BY-NC"`
>>>>> - `"CC-BY-NC-SA"`
>>>>> - `"CC-BY-NC-ND"`
>>>>>
>>>>>> ## Req. `<license-p>`
>>>>>>
>>>>>> Paragraph of text within the description of a `<license>`.
>>>>
>>>>> ## `<ali:free_to_read>`
>>>>>
>>>>> Indicates any free-access license. This is **Required** (*without* `@start_date` and `@end_date`) in the case of the following values for `@article-type` (as described above): `"correction"`, `"editorial"`, `"obituary"`, `"partial-retraction"`, `"retraction"`, or `"introduction"`.
>>>>>
>>>>> ### Req. `@xmlns:ali="http://www.niso.org/schemas/ali/1.0/"`
>>>>>
>>>>> ### `@start_date`
>>>>>
>>>>> Indicates the date on which the free-access license should start. Value should use the following format: "yyyy-mm-dd".
>>>>>
>>>>> ### `@end_date`
>>>>>
>>>>> Indicates the date on which the free-access license should end. Value should use the following format: "yyyy-mm-dd".
>>>>> The absence of both start_date and end_date dates indicates a permanent free-to-read status.
>>>
>>>> ## `<self-uri/>`
>>>> The URI for the (online) PDF-version of the print article.
>>>>
>>>> This empty element encloses a link to the PDF.
>>>>
>>>> ### `@content-type`
>>>>
>>>> For Brill, this is always:
>>>>
>>>> - `"PDF"`
>>>>
>>>> ### `@xlink:href`
>>>> The URL at which the PDF can be found.
>>>
>>>> ## `<abstract>`
>>>> The article's abstract, so a summarized description of the content.
>>>>
>>>>> ## Req. `<title>`
>>>>>
>>>>> The title for the abstract. Usually, this is `"Abstract"`, but it can also be `"Samenvatting"`, `"Summary"` or `"Σύνοψις"`. The Brill Desk Editor will provide you with a title if it is different than `"Abstract"`. Do not add style elements!
>>>>
>>>>> ## Req. `<p>` [See Paragraphs](#paragraphs)
>>>
>>>> ## `<trans-abstract>`
>>>> An abstract in another language than the main language of the article.
>>>>
>>>> ### Req. `@xml:lang`
>>>>
>>>> Indicates the language on a translated abstract in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>
>>>>> ## Req. `<p>` [See Paragraphs](#paragraphs)
>>>
>>>> ## `<kwd-group>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#kwd-group-example)
>>>>
>>>> Keyword group. Container element for one set of keywords (such as `<kwd>`s) used to describe a document.
>>>>
>>>> ### Req. `@kwd-group-type`
>>>>
>>>> Indicates the type of keyword group, supported values:
>>>>
>>>> - `"uncontrolled"`
>>>>
>>>> In future versions, systems like the *US Library of Congress Subject Headings* may be used.
>>>>
>>>> ### `@xml:lang`
>>>>
>>>> Indicates a keyword grouping in another language than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>>>
>>>>> ## Req. `<title>`
>>>>>
>>>>> The title for the keyword group. Usually, this is `"Keywords"`; the Brill Desk Editor will provide you with a title if it is different than `"Keywords"`. Do not add style elements!
>>>>
>>>>> ## Req. `<kwd>`
>>>>>
>>>>> Keyword, like a subject term, key phrase, indexing word etc. The author will supply this information.
>>>
>>>> ## `<funding-group>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#funding-group-example)
>>>>
>>>> Container element for information about the funding of the research reported in the article (for example, grants, contracts, sponsors).
>>>>
>>>>> ## `<award-group>`
>>>>> Container element for information concerning one award under which the work (or the research on which the work was based) was supported.
>>>>>
>>>>> ### `@id` [See id-syntax](#id-syntax)
>>>>>
>>>>> The value for this `@id` attribute will be supplied by Brill. If none is supplied, the attribute should be omitted.
>>>>>
>>>>>> ## `<funding-source>`
>>>>>> Agency or organization that funded the research on which a work was based.
>>>>>>
>>>>>>> ## `<institution-wrap>`
>>>>>>>
>>>>>>>> ## Req.`<institution>`
>>>>>>>>
>>>>>>>> Name of the funding institution or organization.
>>>>>>>
>>>>>>>> ## `<institution-id>`
>>>>>>>>
>>>>>>>> ### Req. `@institution-id-type`
>>>>>>>>
>>>>>>>> For funders, both DOI and Ringgold values are supported. Supported values:
>>>>>>>>
>>>>>>>> - `"doi"`
>>>>>>>> - `"ringgold"`
>>>>>
>>>>>> ## `<award-id>`
>>>>>>
>>>>>> Unique identifier assigned to an award, contract, or grant. I.e., the grant number.
>>>>>
>>>>>> ## `<principal-award-recipient>`
>>>>>>
>>>>>> Individual(s) or institution(s) to whom the award was given (for example, the principal grant holder or the sponsored individual).
>>>>>
>>>>>> ## `<principal-investigator>`
>>>>>>
>>>>>> Individual(s) responsible for the intellectual content of the work reported in the document.
>>>>
>>>>> ## `<funding-statement>`
>>>>>
>>>>> Displayable prose statement that describes the funding for the research on which a work was based.
>>>
>>>> ## Req. `<custom-meta-group>`
>>>>
>>>> Container element for metadata not otherwise defined in the Tag Suite.
>>>> Brill requires *header files* &ndash; XML files that do not contain body content &ndash; to have the `version: header` key-value pair. Brill requires *full content files* &ndash; XML files that contain both header and body content and back matter &ndash; to have the `version: fulltext` key-value pair.
>>>>
>>>>> ## Req. `<custom-meta>`
>>>>>> ## Req. `<meta-name>`
>>>>>>
>>>>>> A custom metadata name (or key). Supported value:
>>>>>> - `"version"`
>>>>>
>>>>>> ## Req. `<meta-value>`
>>>>>>
>>>>>> A custom metadata value. Supported values:
>>>>>> - `"header"`
>>>>>> - `"fulltext"`
>
>> ## `<body>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#article-body-example)
>>
>> Main textual portion of the document that conveys the narrative content.
>>
>>> ## `<p>` [See Paragraphs](#paragraphs)
>>
>>> ## `<sec>` [See Sections](#sections)
>>
>>> ## `<sig-block>`
>>>
>>> Area of text and graphic material placed at the end of the body of a document or document component to hold the graphic signature or description of the person(s) responsible for or attesting to the content.
>>>
>>>> ## `<sig>`
>>>>
>>>> One contributor’s signature and associated material (such as a text restatement of the affiliation) inside a signature block.
>>>>
>>>>> ## `<graphic>`
>>>>>
>>>>> Description of and pointer to an external file containing a still image.
>>>>>
>>>>> ### `@xlink:href`
>>>>>
>>>>> The URI where the resource can be found.
>>>>>
>>>>> ### Req. `@orientation`
>>>>>
>>>>> Whether the object should be positioned tall (longe edge vertical `"portrait"`) or wide (long edge horizontal `"landscape"`).
>>>>>
>>>>> - `"portrait"`
>>>>> - `"landscape"`
>>>>>
>>>>> ### `@position`
>>>>>
>>>>> Whether the object must be anchored to its place or may float algorithmically to another location in the document. Also used for margin placement.
>>>>>
>>>>> - `"anchor"` Object must remain on exact location in the text flow.
>>>>> - `"float"` **Default value**. Object may move to a new column, page, end of document, etc.
>>>>> - `"margin"` The object should be placed in the margin or gutter.
>>
>
>> ## `<back>`
>>
>>> ## `<ack>`
>>>
>>> Acknowledgments. Textual material that names the parties who the author wishes to thank or recognize. Should not be included here if the Acknowledgments were placed in a footnote (in which case they should be included as a `<fn>`.
>>>
>>>> ## `<title>`
>>>
>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>
>>>> ## `<sec>` [See Sections](#sections)
>>
>>> ## `<bio>`
>>>
>>> Biographical data concerning a contributor or the description of a collaboration. Only to be used if there is an extensive description of these details given as a separate section. Should not be included here if the Biography was placed in a footnote (in which case they should be included as a `<fn>`.
>>>
>>>> ## `<title>`
>>>
>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>
>>>> ## `<sec>` [See Sections](#sections)
>>
>>> ## `<app-group>`
>>>
>>> Container element for one or more appendices (`<app>` elements).
>>>
>>>> ## `<app>`
>>>>
>>>> Appendix. Additional material added to a document that typically follows the body of the document. This is ancillary or supporting material, not a direct continuation of the text of the document.
>>>>
>>>>> ## `<title>`
>>>>
>>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>>
>>>>> ## `<sec>` [See Sections](#sections)
>>>
>>>> ## `<def-list>`    [See Definition List](#definition-list)
>>>
>>>> ## `<list>`        [See List](#list)
>>
>>> ## `<fn-group>` [See Footnotes](#footnotes)
>>
>>> ## `<glossary>`    [See Glossary](#glossary)
>>
>>> ## `<ref-list>`    [See Reference List](#reference-list)



# Sections

Sections are headed by a `<sec>` element, which contains two genres of content:
title information (a `<label>` and a `<title>`) and content (all other elements described).

> ## `<sec>`
> Headed group of material; the basic structural unit of the body of a document.
>
> A section can contain another `<sec>` recursively. If a sections has a chapter number or a similar label, this MUST be placed in a `<label>`.
>
> ### `@id` [See id-syntax](#id-syntax)
>
> Give a Section an ID to refer to it when the manuscript says things like *“See chapter 11”*.
>
> ### `@sec-type`
>
> Required only if the `<sec>` starts with a heading title: this attribute is then used to indicate the heading level. Brill supports a maximum of 8 heading levels, same as in the Brill Typographic Style. Note that this attribute should not be used for any other purpose. The following values are permitted:
>
> - `"heading-1"`
> - `"heading-2"`
> - `"heading-3"`
> - `"heading-4"`
> - `"heading-5"`
> - `"heading-6"`
> - `"heading-7"`
> - `"heading-8"`
>
>> ## `<label>`
>>
>> The Section number, like `2` or `15.5.1` or `XIV.15.A.β.ii`.
>
>> ## Req. `<title>`
>>
>> The actual heading text of the section.
>>
>>> ## `<target/>`
>>>
>>> This element can be used as a marker to indicate the page number in the PDF. Note that this element can also be included below several other elements, as needed.
>>>
>>> ### Req. `@id` [See id-syntax](#id-syntax). 
>>>
>>> The value here should be given as "Page1", "Page2", etc.
>>
>>> ## `<xref>`
>>>
>>> Cross reference a footnote. All `<xref>`s in the text must be clickable in the PDF and online displays. Note that adding footnotes to titles should be avoided if possible.
>>>
>>> ### Req. `@ref-type`
>>>
>>> Supported value:
>>>
>>> - `"fn"`: Refers from a text reference to footnote (`<fn>`). This should include element `<sup>` and the footnote number/marker.
>>>
>>> ### Req. `@rid`
>>>
>>> Contains the `@id` of the element this element points to. If the reference is to more than one element, add all IDs here separated by spaces.
>
>> ## `<table-wrap>`  [See Tables](#tables)
>
>> ## `<table-wrap-group>` [See Tables](#tables)
>
>> ## `<fig>`         [See Figures](#figures)
>
>> ## `<fig-group>`   [See Figures](#figures)
>
>> ## `<disp-quote>`  [See Quotations](#quotations)
>
>> ## `<verse-group>` [See Verse](#verse)
>
>> ## `<def-list>`    [See Definition List](#definition-list)
>
>> ## `<list>`        [See List](#list)
>
>> ## `<p>` 		  [See Paragraphs](#paragraphs)
>
>> ## Recursively `<sec>`
>
>> ## `<boxed-text>`
>>
>> Contains textual material that is part of the body but is outside the flow of the narrative text (whether enclosed in a box or not). For example, marginalia, sidebars, cautions, tips, warnings, note boxes.
>>
>>> ## `<title>`
>>
>>> ## `<p>`
>>
>>> ## `<sec>`
>
>> ## `<mml:math>`
>>
>> The MathML spec is not part of BITS / JATS and readers should to refer to its definition: [MathML](https://www.w3.org/TR/MathML3/). Do not forget to add `@xmlns:mml` to `<article>`.
>
>> ## `<tex-math>`
>>
>> Supply the mathematical formula in (La/Xe)TeX text inside a `<tex-math>`.


# Paragraphs

Paragraphs are `<p>` tags, with similar semantics as the HTML `<p>` tag.
Due to footnotes’ JATS/BITS design, `<p>` tags may contain anything a
`<sec>` may, but please refrain from adding anything but markup tags in
running text.

Do not use `<bold>`, `<italic>` or `<sc>` etc. on places where the Brill Style
supplies its use. Those usages are not semantic uses of small caps style,
i.e. do not supply `<sc>` Small Caps in a `<caption>` Caption element, as
captions—per Brill Typographic Style—are always set in small caps.

> ## `<p>`
>
> A text paragraph.
>
>> ## `<inline-graphic>`
>>
>> Description of and pointer to an external graphic that is displayed or set in the same line as the text.
>> Only use graphics for characters or signs not included in Unicode. Think of Egyptological signs not included in Unicode.
>>
>> ### Req. `@xlink:href`
>>
>> The URI where the resource can be found.
>
>> ## `<xref>`
>> A cross reference to an object within the document (e.g. a table, affiliation or bibliographic citation).
>> It can be used to reference to anything with an `@id`.
>> All `<xref>`s in the text must be clickable in the PDF and online displays.
>>
>> ### Req. `@ref-type`
>>
>> Contains the type of reference. Possible values:
>>
>> - `"aff"`: Refers from a `<contrib>` to an `<aff>`. This element is usually empty.
>> - `"bibr"`: Refers from a cited source to its place in the bibliography (the `<ref>`).
>> - `"fig"`: Refers from a text reference to the actual figure (`<fig>` or `<fig-group>`).
>> - `"fn"`: Refers from a text reference to footnote (`<fn>`). This should include element `<sup>` and the footnote number.
>> - `"table"`: Refers from a text reference to the actual table (`<table-wrap>` or `<table-wrap-group>`).
>>
>> ### Req. `@rid`
>>
>> Contains the `@id` of the element this element points to.
>> If the reference is to more than one element, add all IDs here separated by spaces.
>
>> ## `<named-content>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#named-content-example)
>>
>> We could use this element for a word or subject that has distinct semantics or special significance.
>>
>> ### `@content-type`
>>
>> When necessary, content-type can be:
>>
>> - `"frontispiece"`
>> - `"in-memoriam"`
>> - `"verse-line-number"`
>
>> ## `<bold>`
>>
>> Font bold weight.
>
>> ## `<italic>`
>>
>> Font italic style.
>
>> ## `<sc>`
>>
>> Small Caps. Note that any text surrounded by `<sc>` tags should be included in their proper case (as if they weren't small caps): i.e., any capitals should be included as capitals, *not* lower case, while proper lower case should be included as such. Note also that as per Brill Typographic Style, `<sc>` should *not* be used at all for mixed-case abbreviations.
>
>> ## `<ext-link>`
>>
>> Link to an external file or resource.
>>
>> ### `@xlink:href`
>>
>> The URI where the resource can be found.
>
> ## Right-to-Left and Bi-Directional Text
>
> Indicating that a text uses another text direction than left-to-right can be simply indicated by including the text in correct Unicode (UTF-8) *and* including attribute `@xml:lang` for the relevant elements:
>
>> ## `<p>`
>>
>> ### `@xml:lang`
>>
>> Indicates this element (and all sub-elements) are in this particular language. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>
> Additionally, if the text in an element is bi-directional (e.g., the main text in a `<p>` is in Arabic, but certain words or parts of a phrase are in English), then the parts of the text in another direction than the main part of the text within that element can be indicated by the following element:
>
>>> ## `< styled-content >`
>>>
>>> ### `@xml:lang`
>>>
>>> Indicates this element is in this particular language. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>
>> ## `<media>` [See Embedded-video](#embedded-video)
>
>> ## `<target/>`
>>
>> This element can be used as a marker to indicate the page number in the PDF. Note that this element can also be included below several other elements, as needed.
>>
>> ### Req. `@id` [See id-syntax](#id-syntax). 
>>
>> The value here should be given as "Page1", "Page2", etc.

# Footnotes

Footnotes should be included in a `<fn-group>`. 

> ## `<fn-group>`
>
> Container element for all footnotes. This element appears at the end of the document.
>
>> ## Req. `<fn>`
>>
>> A reference to a `<fn>` is made with the [`<xref>`](/elem-xref/) element.
>>
>> ### `@xml:lang`
>>
>> Used if footnote language is different than the main language of the article. Supported values are [ISO 639-2 (B)](https://www.loc.gov/standards/iso639-2/php/code_list.php).
>>
>> ### Req. `@id` [See id-syntax](#id-syntax)
>>
>> The identifier that links a `<xref>` with the footnote.
>>
>>> ## Req. `<label>`
>>>
>>> The footnote number. Do not use `<sup>` here.
>>
>>> ## Req. `<p>` [See Paragraphs](#paragraphs)
>>>
>>> The actual text of the footnote is put in one or more `<p>` text paragraphs.


# Tables

For multiple tables (`<table-wrap>` elements) that are to be displayed together, use the `<table-wrap-group>`.

In certain cases, it is permitted to included tables as if they were an image, using the element `<fig>` [See Figures](#figures). If doing so, and the table is in the print version divided over multiple pages, please include it as *one* image here, without repeated heading lines, etc.

> ## `<table-wrap-group>`
>
> Container element for tables.
>
>> ## `<table-wrap>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#table-example)
>>
>> Wrapper element for a complete table, including the tabular material (rows and columns), caption (including title), footnotes, and alternative descriptions of the table for purposes of accessibility.
>>
>> ### `@id` [See id-syntax](#id-syntax)
>>
>> A document-unique identifier for the element.
>>
>>> ## `<label>`
>>>
>>> The label for a table-wrap.
>>
>>> ## `<caption>`
>>>
>>>> ## `<title>`
>>>>
>>>> The title for a table-wrap.
>>>
>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>
>>> ## `<table>`
>>>
>>> The rows and columns part of a full table.
>>>
>>> ### `@cellpadding`
>>>
>>> Amount of space, measured in pixels, between the border of a cell and the contents of a table cell.
>>>
>>> ### `@frame`
>>>
>>> Specifies which sides of the table should be given rules, making a box or frame of lines around the table.
>>>
>>>> ## `<thead>`
>>>>
>>>> Container element for the table header rows of a table.
>>>>
>>>>> ## `<tr>`
>>>>>
>>>>> Container element for all the cells in a single table row.
>>>>>
>>>>>> ## `<th>`
>>>>>>
>>>>>> One cell in the table header, as opposed to an ordinary cell in the body of a table.
>>>>>>
>>>>>> ### `@colspan`
>>>>>>
>>>>>> How many columns this element spans, e.g. `<th colspan="2">` is two columns wide.
>>>>>>
>>>>>> ### `@rowspan`
>>>>>>
>>>>>> How many rows this element spans, e.g. `<th rowspan="4">` is four rows high. This attribute is unusual in headers.
>>>>>
>>>>>> ## `<td>`
>>>>>>
>>>>>> One ordinary cell in the body of a table, as opposed to a cell in the table header.
>>>>>>
>>>>>> ### `@colspan`
>>>>>>
>>>>>> How many columns this element spans, e.g. `<td colspan="2">` is two columns wide.
>>>>>>
>>>>>> ### `@rowspan`
>>>>>>
>>>>>> How many rows this element spans, e.g. `<td rowspan="4">` is four rows high.
>>>>>>
>>>>>> ### `@scope`
>>>>>>
>>>>>> Use this with value `"row"` as the first `<td>` when there is a table header on the side of a table.
>>>>
>>>> `<tfoot>`
>>>>
>>>> Container element for the footer rows within a NISO JATS table.
>>>>
>>>> This element comes after a [`<thead>`](/elem-thead/) and before a [`<tbody>`](/elem-tbody/) so that implementation can render the table footer before a (very long) body.
>>>
>>>> `<tbody>`
>>>>
>>>> Container element that holds the rows and columns in one of the body (as opposed to the header) portions of a table.
>>
>>> `<attrib>`
>>>
>>> Attribution information. Do not use `<sc>` tags in this element, as the Small Caps style is part of the
>>> Brill Typographic Style and NOT semantic.

# Figures

Figures can be graphic or textual material. For figures that have to be displayed together,
the element `<fig-group>` is available. For single figures, use the element `<fig>` and follow the instructions below.

Note that QR-codes should also be included in this manner, with the direct image of the QR code linked via `<graphic>` and the relevant URL given in `<caption>`.

> ## `<fig-group>`
>
> Container element for figures that are to be displayed together.
>
>> ## `<caption>`
>>
>> The caption for a fig-group.
>>
>>> ## `<title>`
>>>
>>> The title for a fig-group.
>
>> ## `<fig>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#list-fig-example)
>>
>> Block of graphic or textual material that is identified as a figure, usually bearing a caption and a label such as “Figure 3.” or “Figure”.
>>
>> ### `@id` [See id-syntax](#id-syntax)
>>
>> A document-unique identifier for the element.
>> Starts with `"FIG"`.
>>
>> ### Req. `@orientation`
>>
>> Whether the object should be positioned tall (longe edge vertical `"portrait"`) or wide (long edge horizontal `"landscape"`).
>>
>> - `"portrait"`
>> - `"landscape"`
>>
>> ### `@position`
>>
>> Whether the object must be anchored to its place or may float algorithmically to another location in the document. Also used for margin placement.
>>
>> - `"anchor"` Object must remain on exact location in the text flow.
>> - `"float"` **Default value**. Object may move to a new column, page, end of document, etc.
>> - `"margin"` The object should be placed in the margin or gutter.
>>
>> ### `@fig-type`
>>
>> The type of figure. Supported values:
>>
>> - `"graphic"`
>>
>>> ## `<label>`
>>>
>>> The label for a figure.
>>
>>> ## Req. `<caption>`
>>>
>>> The caption for a figure.
>>>
>>>> ## Req. `<title>`
>>>>
>>>> The title for a figure.
>>>
>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>>
>>>> The paragraph provides explanation.
>>
>>> ## Req. `<graphic>`
>>>
>>> Description of and pointer to an external file containing a still image.
>>>
>>> ### Req. `@xlink:href`
>>>
>>> The URI where the resource can be found.
>>>
>>>> ## Req. `<alt-text>`
>>>>
>>>> Alternate Text Name. Word or phrase used to provide a very short textual name, description, or purpose-statement for a structure such as a graphic or figure. This element is to be included for accessibility purposes and as such is always required. In this element, repeat the text given in the relevant `<caption>``<title>` _unless_ Brill provides a specific alt-text.
>>
>>> ## `<attrib>`
>>>
>>> Attribution information. Do not use `<sc>` tags in this element, as the Small Caps style is part of the
>>> Brill Typographic Style and NOT semantic.


# Quotations

A block quote is in this tag set a `<disp-quote>`. These can be nested, but regularly
contain just a `<p>` and, optionally an `<attrib>` attribution.

> ## `<disp-quote>`> [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#disp-quote-example)
>> ## `<label>`
>>
>> The reference number for the block quote.
>
>> ## `<title>`
>>
>> A title displayed before a block quote.
>
>> ## Nested `<disp-quote>`
>
>> ## Req. `<p>` [See Paragraphs](#paragraphs)
>>
>> The actual text of the quotation is put in one or more `<p>` text paragraphs.
>
>> ## `<attrib>`
>>
>> Attribution information. Do not use `<sc>` tags in this element, as the Small Caps style is part of the
>> Brill Typographic Style and NOT semantic.

# List

Ordered and unordered list alike are put in a `<list>`.

> ## `<list>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#list-fig-example)
>
> Sequence of two or more items, which may or may not be ordered.
>
> ### `@list-type`
>
> - `"order"` Ordered list. Prefix character is a number or a letter, depending on local style.
> - `"bullet"` Unordered or bulleted list. Prefix character is a dash conforming to BTS.
> - `"alpha-lower"` Ordered list. Prefix character is a lowercase alphabetical character.
> - `"alpha-upper"` Ordered list. Prefix character is an uppercase alphabetical character.
> - `"roman-lower"` Ordered list. Prefix character is a lowercase roman numeral.
> - `"roman-upper"` Ordered list. Prefix character is an uppercase roman numeral.
> - `"simple"` Simple or plain list (No prefix character before each item)
>
> ### `@id` [See id-syntax](#id-syntax)
>
> The `@id` attribute supplies a document-internal unique reference to an element.
>
> ### `@continued-from`
>
> To be used if a `<list>` is a continuation from another `<list>` earlier in the document. If used, the earlier `<list>` should include an `@id`, and the value of said `@id` should be used as the value of `@continued-from` here.
>
>> ## `<list-item>`
>>
>> Single item (one entry) in a list of items.
>>
>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>
>>> The actual text of the list-item is put in one or more `<p>` text paragraphs.  

# Definition List

A definition list has a special layout: a word/phrase or term on the one hand is paired with descriptions, explanations or definitions on the other hand.

> ## `<def-list>`
>
> List in which each item consists of two parts: 1) a word, phrase, term, graphic, chemical structure, or equation, that is paired with 2) one or more descriptions, discussions, explanations, or definitions of it.
>
> ### `@list-type`
>
> - `"order"` Ordered list. Prefix character is a number or a letter, depending on local style.
> - `"bullet"` Unordered or bulleted list. Prefix character is a dash conforming to BTS.
> - `"alpha-lower"` Ordered list. Prefix character is a lowercase alphabetical character.
> - `"alpha-upper"` Ordered list. Prefix character is an uppercase alphabetical character.
> - `"roman-lower"` Ordered list. Prefix character is a lowercase roman numeral.
> - `"roman-upper"` Ordered list. Prefix character is an uppercase roman numeral.
> - `"simple"` Simple or plain list (No prefix character before each item)
>
> ### `@list-content`
>
> Type of list, usually with a semantically meaningful name (for example, a “where-list” for the list of the variables that follows an equation, a “notation-list”, or a “procedure-list” for a list of procedural steps to follow).
>
>> ## `<title>`
>>
>> A title displayed before a def-list.
>
>> ## `<term-head>`
>>
>> Heading over the first column (the `term` column) of a `definition list` (two-part list).
>
>> ## `<def-head>`
>>
>> Heading over the second column (the definition column) of a definition list (two-part list).
>
>> ## `<def-item>`
>>
>> One item in a definition (two-part) list.
>>
>>> ## `<term>`
>>>
>>> Word, phrase, graphic, chemical structure, equation, or other noun that is being defined or described.
>>> This element is typically used in conjunction with a definition in a `definition list` (two-part list). The term occupies the first column of the two-part list and is the subject of the definition or description, which occupies the second column.
>>>
>>> ### `@id` [See id-syntax](#id-syntax)
>>>
>>> A document-unique identifier for the element.
>>
>>> ## `<def>`
>>>
>>> Definition, explanation, or expansion of a word, phrase, or abbreviation.
>>>
>>>> ## `<p>` [See Paragraphs](#paragraphs)
>>>>
>>>> The actual text of the definition is put in one or more `<p>` text paragraphs.  

# Glossary

A glossary typically contains one or more def-lists. If a description is needed before the list begins, a `<p>` may be included as well. The glossary is part of the back matter of a document.

> ## `<glossary>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#glossary-example)
>
>> ## `<label>`
>>
>> Number and/or prefix word placed at the beginning of display elements (for example, equation, statement, figure). For example "Chapter 1", "Introduction:", "Conclusion:", "Appendix:", etc. 
>
>> ## `<title>`
>>
>> A title displayed before a glossary.
>
>> ## `<p>` [See Paragraphs](#paragraphs)
>>
>> For any descriptive text preceding the Glossary.
>
>> ## `<def-list>` [See Definition List](#definition-list)

# Verse

Verse, like poetry or quoted material where line numbers are important, use `<verse-group>`.
Line numbers however cannot be tagged separately in a `<label>` according to the BITS/JATS DTD, so instead
please use `<named-content content-type="verse-line-number">`.

> ## `<verse-group>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#verse-group-example)
>> ## `<label>`
>>
>> The reference number for the verse. This may be referred to in the text.
>
>> ## `<title>`
>>
>> The title of the verse, e.g. `Sonnet 18` or `Shall I compare thee to a summer’s day?`.
>
>> ## `<verse-line>`
>>
>> One line of a poem or verse.
>
>> ## `<attrib>`
>>
>> Attribution information. Do not use `<sc>` tags in this element, as the Small Caps style is part of the
>> Brill Typographic Style and NOT semantic.


# Reference List

A reference list is a list of bibliographic references for a document or a document component. It contains one or more ref elements. Please note that `<ref-list>` is only allowed in `<back>`.

> ## `<ref-list>` [Example](https://brillpublishers.gitlab.io/jats-and-bits/examples#reference-list-example)<!--(/example-article-body)-->
>
> Main element for Reference List. Note that this element can be used recursively (so a `<ref-list>` can contain (multiple) other instance(s) of `<ref-list>`) to include multiple distinct reference lists (e.g., "Primary Sources", "Secondary Literature", etc.).
>
>> ## `<title>`
>>
>> The title of the ref-list, e.g. `References`.
>
>> ## `<p>` [See Paragraphs](#paragraphs)
>>
>> For any descriptive text preceding the Reference List.
>
>> ## `<ref>`
>>
>> One item in a bibliographic list.
>>
>> ### Req. `@id` [See id-syntax](#id-syntax)
>>
>> A document-unique identifier for the element.
>>
>>> ## `<label>`
>>>
>>> The reference number for a reference. This may be referred to in the text.
>>
>>> ## `<mixed-citation>`
>>>
>>> Bibliographic description of a work. Includes a combination of bibliographic reference elements and untagged text. Spacing and punctuation are preserved. 
>>> Note that any punctuation that is *not*  itself part of the proper value of an element, but is dictated only by the reference style, should not be included in the element itself. E.g., if reference style dictates that a journal article title should be surrounded by double quotation marks ("), but these are not normally part of the article title itself, then these double quotation marks should *not* be included within the element `<source>`, but instead directly outside it.
>>> Similarly, if the Reference list uses an author date style that leads to dates such as "2018a", "2018b", then only the year should be placed inside the element `<year>`, with the "a" or "b" directly outside it. For example: `<year>`2018`</year>`a.
>>>
>>> ### Req. `@publication-type`
>>>
>>> Supported values *major, need to be tagged fully if used*:
>>>
>>> - `"book"`
>>> - `"journal"`
>>> - `"web"`
>>>
>>> In case of `"web"`, please add `<uri>`
>>>
>>> Supported values *minor, full tagging is not needed*:
>>>
>>> - `"data"`
>>> - `"list"`
>>> - `"other"`
>>> - `"patent"`
>>> - `"thesis"`
>>> - `"commun"` for communiqués
>>> - `"confproc"` for conference proceedings.
>>> - `"discussion"`
>>> - `"gov"`
>>>
>>>> ## `<person-group>`
>>>>
>>>> Container element for one or more contributors in a bibliographic reference.
>>>>
>>>> ### `@person-group-type`
>>>>
>>>> Specify authors and other contributors to a reference. A separate `<person-group>` element should be used for each role. Supported values are:
>>>>
>>>> - `"all-authors"`
>>>> - `"assignee"`
>>>> - `"author"`
>>>> - `"compiler"`
>>>> - `"curator"`
>>>> - `"director"`
>>>> - `"editor"`
>>>> - `"guest-editor"`
>>>> - `"inventor"`
>>>> - `"transed"`
>>>> - `"translator"`
>>>>
>>>>> ## `<string-name>`
>>>>>
>>>>> Container element for components of personal names *unstructured*
>>>>>
>>>>>> ## `<surname>`
>>>>>>
>>>>>> Surname of a person.
>>>>>
>>>>>> ## `<given-names>`
>>>>>>
>>>>>> All given names of a person, such as the first name, middle names, maiden name if used as part of the married name, etc. This element will NOT be repeated for every given name a person has.
>>>>>
>>>>>> ## `<prefix>`
>>>>>>
>>>>>> Honorifics or other qualifiers that usually precede a person’s name (for example, Professor, Rev., President, Senator, Dr., Sir, The Honorable).
>>>>>
>>>>>> ## `<suffix>`
>>>>>>
>>>>>> Qualifiers that follow a person’s name (for example, Sr., Jr., III, 3rd). 
>>>
>>>> ## `<collab>`
>>>>
>>>> Group of contributors credited under a single name; includes an organization credited as a contributor.
>>>
>>>> ## `<article-title>`
>>>>
>>>> Full title of an article or other journal component, such as a book review. When the cited document is a book-chapter, use:
>>>
>>>> ## `<chapter-title>`
>>>>
>>>> Title of a cited book chapter.
>>>
>>>> ## Req. `<source>`
>>>>
>>>> Title of a document (for example, journal, book, conference proceedings) that contains (is the source of) the material being cited in a bibliographic reference or product.
>>>
>>>> ## `<date>`
>>>>
>>>>> ## `<year>`
>>>>>
>>>>> A year, in (zero-padded) digits.
>>>
>>>> ## `<publisher-name>`
>>>>
>>>> ## `<publisher-loc>`
>>>
>>>> ## `<volume>`
>>>
>>>> ## `<issue>`
>>>
>>>> ## `<fpage>`
>>>
>>>> ## `<lpage>`
>>>
>>>> ## `<uri>`

# Embedded-video

In case any video needs to be displayed directly in the text on the Brill website, we can include it as an "embedded video". Note that any such videos should _also_ be included in the `<supplementary-material>` of the article. All videos are expected to be uploaded to Figshare, and relevant information will be provided by the Brill Production Editor.

Embedded videos should be included directly under element `<p>` [See Paragraphs](#paragraphs).

> ## Req. `<media>`
>
> ### Req. `@xmlns:xlink="https://www.w3.org/1999/xlink"`
>
> ### Req. `@id` [See id-syntax](#id-syntax)
>
> A document-unique identifier for the element. 
>
> ### Req. `@content-type="figshare"`
>
> ### Req. `@specific-use="original-format"`
>
> ### Req. `@xlink:href`
>
> A link to the video on Figshare, given as a full DOI link. Example: `"https://doi.org/10.6084/m9.figshare.example"`
>
> ### Req. `@position="anchor"`
>
> ### Req. `@orientation="portrait"`
>
> ### Req. `@mimetype="video"`
>
> ### Req. `@mime-subtype="avi"`
>
>> ## Req. `<object-id>`
>>
>>This element should contain the DOI of the article itself. Given as a simple DOI, so no URL. Example: 
>> 10.1163/23644583-00401003
>>
>> ### Req. `@pub-id-type="doi" `
>> ### Req. `@specific-use="metadata"`


# id-syntax

> Each `@id` is unique within the XML document. Brill has defined the following `@id` syntax:
> 
> Pre-determined capital letter(s) + six numerical characters. 
> 
> E.g., `<sec id="S000001" sec-type="heading-1">`
> 
> Note that numbering does not need to be consecutive, just that entire `@id` needs to be unique within the XML document.
> 
> The following pre-determined `@id` capital letters are supported:
> 
> - AFF : `<aff>`
> - AWG : `<award-group>` 
> - FN : `<fn>` 
> - IMG : `<fig>` 
> - L : `<list>` 
> - MED : `<media>`
> - P : `<p>`
> - Page : `<target>`
> - R : `<ref>` 
> - S : `<sec>` 
> - T : `<table-wrap>`
> - TRM : `<term>` 
