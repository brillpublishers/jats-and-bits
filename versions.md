# Versions

Changes to this document are documented here. NOTE: the below information is incomplete.

## 1.0
By Redmer Kronemeijer,  Lianne Heslinga.

First version. First-time inclusion of XML standards for BITS book content.

> ## Version 1.2
>
>> ### 2016-06-30
>>
>> - Added `<!DOCTYPE>` declaration to JATS and BITS.
>> - Adjusted `<title-group>` in BITS, book chapters or book parts.
>> - Removed **Req.** `<contrib-group>` from JATS journal article, and BITS book chapter or book part.
>> - Added new items to [To Do](http://brill.gitlab.io/changelog/#to-do) list.
>> - Added list of Creative Commons licenses.
>> - Clean up, small corrections.
>
> ## Version 1.1
>
>> ### 2016-06-25
>>
>> - Small corrections added to JATS and BITS, see *To Do* list above.
>
> ## Version 1.0
>
>> ### 2016-05-13
>>
>> - Publication of Brill's JATS and BITS Standards.
