# Examples

### alt-title example

This is an example for an alternative title. 

```
...
<book-title-group>
<book-title>The Ancient World and Beyond</book-title>
<alt-title alt-title-type="sort-title">Ancient World and Beyond, The</alt-title>
</book-title-group>
...   
``` 

### article-body example

An example for an article-body, kwd-group, ref-list and fn-group. 

```
...
    <article-meta>
        <article-id pub-id-type="doi">10.1075/jgl.5.06tza</article-id>
            <title-group>
                <article-title>Acquiring variable stress in Greek: An Optimality-Theoretic approach</article-title>
            </title-group>
            ...
            <abstract>
                The present study treats the acquisition of stress in Greek. The goal is the investigation
                of the mechanisms through which children manage ultimately to
                acquire the stress system of their native language.
                ...</abstract>
            <kwd-group>
                <kwd>faithfulness</kwd>
                <kwd>iamb</kwd>
                <kwd>markedness</kwd>
                <kwd>co-phonologies</kwd>
                <kwd>trochee</kwd>
                <kwd>stress</kwd>
                <kwd>variation</kwd>
                <kwd>unranked constraints</kwd>
                <kwd>developmental stage</kwd>
                <kwd>phonological acquisition</kwd>
            </kwd-group>
            ...
        </article-meta>
    </front
<body>
  <sec id="06tza.1">
      <label>1.</label>
      <title>Introduction</title>
      <p>Crosslinguistic child language research has demonstrated that truncation,
          whether segmental or syllabic, is the main strategy that children adopt when
          faced with the task of acquiring longer words in their language. Syllabic truncations
          seem to be mostly trochaic in shape, and the disyllabic trochee has
          proven to be the unmarked, that is, the most natural and most frequently attested
          pattern in child speech.
      ...</p>
  </sec>  
    ...
    <sec id="06tza.3">
        <label>3.</label>
        <title>Acquisition of stress in Greek</title>
        <p>Before I go into a detailed presentation of the data, it is essential to report
            briefly on the characteristics of the accentual system of Greek. <xref rid="fn6" ref-type="fn">6</xref>
            ...</p>
    </sec>  
    </body>
    
        <back>
        <fn-group>
            ...
            <fn id="fn6">
                <p>Following <xref rid= "bibr44" ref-type="bibr">Revithiadou (1999)</xref>, by accent I mean stress. Accent is the abstract notion of
                prominence. Stress is the actual realization of prominence in Greek. Pitch and tone are other
                ways of realizing prominence in languages like Japanese or Mandarin.</p>
        </fn>
        </fn-group>
            <ref-list>
                ...
                <ref id="bibr44">
                      <element-citation publication-type="thesis" publication-format="online">
                        <person-group person-group-type="author">
                            <name><surname>Revithiadou</surname>
                                <given-names>Anthi</given-names></name>
                        </person-group>
                             <source>Headmost Accent Wins</source>
                        <date><year>1999</year></date>
                          <publisher-name>HIL/LOT</publisher-name>
                        </element-citation>
                </ref>
               ...</ref-list>
        </back>
``` 

### Article-categories example

An example for article categories.
 
```
<article dtd-version="1.1">
	<front>
		<journal-meta>...</journal-meta>
		<article-meta>...
			<article-id pub-id-type="doi">10.1163/15718180120494937</article-id>
				<article-categories>
					<subj-group subj-group-type="toc-heading">
						<subject>Ouvrages reçus</subject>
					</subj-group>
				</article-categories>
			<title-group>...</title-group>
			...</article-meta>
	</front>
	<body>...</body>
	<back>...</back>
</article>
```

### Article example

An example for an article.

```
    <article
  article-type="research-article"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:mml="http://www.w3.org/1998/Math/MathML"
  xml:lang="eng"
  dtd-version="1.1"
	<front>...</front>
	<body>...</body>
	<back>...</back>
</article>        
```

### book-body example

An example for a book-body and book-part. 

```
<book-body>
        <book-part id="Bej.9789004180321.i-216_002" book-part-type="chapter">
         <book-part-meta>
             <book-part-id book-part-id-type="doi" >10.1163/ej.9789004180321.i-216.5</book-part-id>
           <title-group>
                    <title>Introduction. Milton&#8217;s Poetics Of Absence And Restoration</title>
            </title-group>
                <contrib-group>
                    ...
                </contrib-group>
                <fpage>i</fpage>
                <lpage>xxvi</lpage>
                <self-uri content-type="pdf" xlink:href="9789004183667_webready_content_s002.pdf"/>
                <counts>
                    <page-count count="18"/>
                </counts>
                </book-part-meta>
            <body>
                <sec id="Bej.002.1">
                    <title>Introduction. Milton&#8217;s Poetics Of Absence And Restoration</title>
                    <p>                       
                        One of the most significant aspects of John Milton’s poetry can be
                        found in a poem he left unfinished. Despite the centrality of the passion
                        to Christian theology and art, Milton never wrote at length about
                        the crucifixion, and his short poem on the subject remained an incomplete
                        fragment. 
                        ...</p>
                </sec>
            </body>
            <back>...</back>
        </book-part>
        
        <book-part>...</book-part>
        
        <book-part>...</book-part>
        
        <book-part>...</book-part>
        
    </book-body>
``` 

### Book header example

An example for a book header.

```
<book xmlns:xlink="http://www.w3.org/1999/xlink" xml:lang="eng" dtd-version="2.0">
	<collection-meta collection-type="series">
		<collection-id collection-id-type="publisher-id">IHC</collection-id>
		<title-group>
			<title>Islamic History and Civilization</title>
			<subtitle>Studies and Texts</subtitle>
		</title-group>
		<contrib-group>
			<contrib contrib-type="editorial board/council member">
				<name name-style="western">
					<surname>Biesterfeldt</surname>
					<given-names>Hinrich</given-names>
				</name>
			</contrib>
			<contrib contrib-type="editorial board/council member">
				<name name-style="western">
					<surname>G&#x00FC;nther</surname>
					<given-names>Sebastian</given-names>
				</name>
			</contrib>
			<contrib contrib-type="advisory editor">
				<name name-style="western">
					<surname>Kadi</surname>
					<given-names>Wadad</given-names>
				</name>
			</contrib>
		</contrib-group>
		<volume-in-collection>
			<volume-number>162</volume-number>
		</volume-in-collection>
		<issn publication-format="print">0929-2403</issn>
	</collection-meta>
	<book-meta>
		<book-id book-id-type="doi">10.1163/9789004387058</book-id>
		<book-title-group>
			<book-title>The Book in Mamluk Egypt and Syria (1250&#x2013;1517)</book-title>
			<subtitle>Scribes, Libraries and Market</subtitle>
		</book-title-group>
		<contrib-group>
			<contrib contrib-type="author">
				<name name-style="western">
					<surname>Behrens-Abouseif</surname>
					<given-names>Doris</given-names>
				</name>
			</contrib>
		</contrib-group>
		<pub-date publication-format="online">
			<day>27</day>
			<month>9</month>
			<year>2018</year>
		</pub-date>
		<isbn publication-format="print">9789004387003</isbn>
		<isbn publication-format="online">9789004387058</isbn>
		<publisher>
			<publisher-name>Brill</publisher-name>
			<publisher-loc>Leiden | Boston</publisher-loc>
		</publisher>
		<permissions>
			<copyright-statement>Copyright 2019 by Koninklijke Brill NV, Leiden, The Netherlands.</copyright-statement>
			<copyright-year>2019</copyright-year>
			<copyright-holder>Koninklijke Brill NV, Leiden, The Netherlands</copyright-holder>
			<license license-type="ccc">
				<license-p>This work is published by Koninklijke Brill NV. Koninklijke Brill NV incorporates the imprints Brill, Brill Nijhoff, Brill Hotei, Brill Sense, Brill Schöningh, Brill Fink, Brill mentis, Vandenhoeck & Ruprecht, Böhlau Verlag and V&R Unipress. Koninklijke Brill NV reserves the right to protect the publication against unauthorized use and to authorize dissemination by means of offprints, legitimate photocopies, microform editions, reprints, translations, and secondary information sources, such as abstracting and indexing services including databases. Requests for commercial re-use, use of parts of the publication, and/or translations must be addressed to Koninklijke Brill NV.</license-p>
			</license>
		</permissions>
		<self-uri content-type="PDF" xlink:href="9789004387058_webready_content_text.pdf"/>
		<counts>
			<book-page-count count="190"/>
		</counts>
		<custom-meta-group>
			<custom-meta>
				<meta-name>version</meta-name>
				<meta-value>fulltext</meta-value>
			</custom-meta>
		</custom-meta-group>
	</book-meta>
  <front-matter>...</front-matter>
  <book-body>...</book-body>
  <book-back>...</book-back>
</book>
```

### book-volume-id example

An example for a book volume id. 

```
 ...<book-meta>
 ...
	<book-title-group>
		<book-title>English Topographies in Literature and Culture</book-title>
	</book-title-group>
	<book-volume-id>10.1594/TOPLIT.726855</book-volume-id>
...</book-meta>
``` 

### custom-meta example

An example for a custom-meta. 

```
<custom-meta-group>
	<custom-meta>
		<meta-name>version</meta-name>
		<meta-value>fulltext</meta-value>
	</custom-meta>
</custom-meta-group>
``` 

### disp-quote example

An example for a quotation. 

```
...
<body>
    <disp-quote><p>The key to religion&#x2019;s evolutionary achievement lies not in it staying the same, but in its ability to respond with moral imagination and organizational vigor to the ascendant power of each age.</p>
    <attrib>Hefner, 2011:153</attrib>
    </disp-quote>
    ...
</body>
...
``` 
### funding-group example

An example for a funding-group and award-group.

```
<funding-group>
    <award-group id="ID0EWRAE2476">
        <funding-source>
            <institution-wrap>
                <institution>A. Funder<institution>
                <institution-id institution-id-type="doi">10.13039/100014612</institution-id>
            <institution-wrap>
        </funding-source>
        <award-id>IN206618</award-id>
        <principal-award-recipient>Award Recipient</principal-award-recipient>
    </award-group>
</funding-group>

``` 

### glossary example

An example for a glossary. 

```
<back>
    ...    
        <glossary>
            <title>Abbreviations</title>
            <def-list>
                <def-item list-type="bullet">
                    <term id="G15">F</term>
                    <def><p>female</p></def>
                </def-item>
                <def-item>
                    <term id="G29">gnty</term>
                    <def><p>genotype</p></def>
                </def-item>
                <def-item>
                    <term id="G43">M</term>
                    <def><p>male</p></def>
                </def-item>
            </def-list>
        </glossary>
	...
</back>
``` 

### Journal header example

A simple example for a journal header.

```
<article article-type="research-article" xmlns:xlink="http://www.w3.org/1999/xlink" dtd-version="1.1" xml:lang="en">
	<front>
    <journal-meta>
        <journal-id journal-id-type="eissn">1571-8182</journal-id>
        <journal-id journal-id-type="publisher-id">CHIL</journal-id>
        <journal-title-group>
            <journal-title>The International Journal of Children's Rights</journal-title>
         </journal-title-group>
            <issn publication-format="print">0927-5568</issn>
        <issn publication-format="online">1571-8182</issn>
        <publisher>
            <publisher-name>Brill</publisher-name>
            <publisher-loc>The Netherlands</publisher-loc>
        </publisher>
    </journal-meta>
    <article-meta>
        <article-id pub-id-type="doi">10.1163/15718180120494937</article-id>
        <title-group>
            <article-title>Children, childhood and political participation: Case studies of young people's councils</article-title>
        </title-group>
        <contrib-group>
            <contrib contrib-type="author">
                <name>
                    <surname>Wyness</surname>
                </name>
            </contrib>
        </contrib-group>
        <pub-date publication-format="online">
            <year>2001</year>
        </pub-date>
        <volume content-type="number">9</volume>
        <volume content-type="year">2001</volume>
        <issue>3</issue>
        <fpage>193</fpage>
        <lpage>212</lpage>
        <permissions>
            <copyright-statement>Copyright 2001 Koninklijke Brill NV, Leiden, The Netherlands</copyright-statement>
            <copyright-year>2001</copyright-year>
            <copyright-holder>Koninklijke Brill NV, Leiden, The Netherlands</copyright-holder>
            <license license-type="ccc" xlink:href="http://www.copyright.com">
                <license-p/>
            </license>
        </permissions>
        <self-uri content-type="pdf" xlink:href="15718182_009_03_s001_text.pdf"/>
        <custom-meta-group>
            <custom-meta>
                <meta-name>version</meta-name>
                <meta-value>header</meta-value>
            </custom-meta>
        </custom-meta-group>
    </article-meta>
</front>
	<body>...</body>
	<back>...</back>
</article>
```

### kwd-group example

An example for a group of keywords.

```
...
<article-meta>...
<abstract>...</abstract>
<kwd-group kwd-group-type="uncontrolled">
<kwd>Kings</kwd>
<kwd>Mythology</kwd>
<kwd>Ancient Past</kwd>
<kwd>Crete</kwd>
</kwd-group>
</article-meta>
...
```

### list-fig example

An example for a list and a figure. 

```
 <sec id="B10.1163_23529369-12340001_049" sec-type="head2">
        <title>IV&#x0009;Intensity of Transboundary Water Cooperation in Europe</title>
        <p>While the increasing integration of eu water law and international water law regarding major European river basins is one thing, the actual intensity of transboundary cooperation on freshwater resources ...</p>
        <list list-type="bullet">
            <list-item><p>Category 1 (very high cooperation intensity): (1) formal international water agreement, (2) international coordinating body, and (3) international rbmp;</p></list-item>
            <list-item><p>Category 2 (high cooperation intensity): (1) formal international agreement, (2) international coordinating body, and (3) no international rbmp;</p></list-item>
            <list-item><p>Category 3 (moderate cooperation intensity): (1) formal international agreement, (2) no international coordinating body, and (3) no international rbmp;</p></list-item>
            <list-item><p>Category 4 (low cooperation intensity): (1) no formal international agreement, (2) no international coordinating body, and (3) no international rbmp.</p></list-item>
        </list>
        <p>As the result of a comprehensive screening process, the study ascertained that 10 transboundary river basins (10 %) fell into category 1, 71 basins (68 %) into category 2, 19 basins (18 %) into category 3 and three basins (3 %) into category 4.<xref ref-type="fn" rid="FN646">646</xref> Although the number of basins and sub-basins allocated to category 1 (highest degree of cooperation) was comparatively small, those basins had a share of approximately 46 % of the total catchment area on eu territory (without territories of third countries), including the Danube and the Rhine as Europe&#x2019;s two largest transboundary river basins.<xref ref-type="fn" rid="FN647">647</xref> The shares of transboundary river basins coordinated with a high degree of cooperation (category 2) accounted for approximately 40 % of the total catchment area on <sc>eu</sc> territory; those with a moderate cooperation degree (category 3) accounted for 12 %; and those with a low cooperation degree (category 4) for only 2 %.<xref ref-type="fn" rid="FN648">648</xref></p>
        ...
		
		<fig id="IMG101001">
            <label>map</label>
            <caption><title>Intensity of Transboundary Cooperation in eu International River Basin Districts</title></caption>
            <graphic xlink:href="23529369_001_01_s001_i0001.jpg"></graphic>
            <p>&#x00A9; european union, 1995&#x2013;2016<xref ref-type="fn" rid="FN655">655</xref></p>
        </fig>
    </sec>
``` 

### named-content example

An example for named-content. 

```
...
<front-matter>
    <notes>
    <p><named-content content-type="frontispiece">
        <fig id="FIG_9789004283800_001" fig-type="graphic">
            <caption>
                <title>Untitled, watercolor</title>
            </caption>
            <graphic xlink:href="9789004283800_001.jpg"/>
            <attrib>2012 Elmer Banegas Flores</attrib>
        </fig>
    </named-content></p>
</notes>
</front-matter>
...
``` 

### Nested Book Part Examples

An example for nested <book-part> structure. 

```
<book-part type="part">
	<book-part-meta>
		<title-group>
			<label>Part 1</label>
			<title>Introduction</title>
			...
		</title-group>
		...
		</book-part-meta>
		<body>
			<book-part type="chapter">
				<book-part-meta>
					<title-group>
						<label>Chapter 1</label>
						<title>...</title>
					</title-group>
					...
				</book-part-meta>
				<body>
					...
				</body>
			</book-part>
			<book-part type="chapter">
				<book-part-meta>
					<title-group>
						<label>Chapter 2</label>
						<title>...</title>
					</title-group>
					...
				</book-part-meta>
				<body>
					...
				</body>
			</book-part>
		</body>
</book-part>
```

### Permissions Examples

An example for `<permissions>`. **YYYY** denotes the Copyright year. 

Note that the text in `<license-p>` should always be the same as the license text given in the PDF version of the publication.

> ## Regular License
>
> Brill standard copyright license, please use the following for Brill titles.
>
```
<permissions>
  <copyright-statement>Copyright YYYY by Koninklijke Brill NV, Leiden, The Netherlands</copyright-statement>
  <copyright-year>YYYY</copyright-year>
  <copyright-holder>Koninklijke Brill NV</copyright-holder>
  <license license-type="ccc">
    <license-p>This work is published by Koninklijke Brill NV. Koninklijke Brill NV incorporates the imprints Brill, Brill Nijhoff, Brill Hotei, Brill Sense, Brill Schöningh, Brill Fink, Brill mentis, Vandenhoeck & Ruprecht, Böhlau Verlag and V&R Unipress. Koninklijke Brill NV reserves the right to protect the publication against unauthorized use and to authorize dissemination by means of offprints, legitimate photocopies, microform editions, reprints, translations, and secondary information sources, such as abstracting and indexing services including databases. Requests for commercial re-use, use of parts of the publication, and/or translations must be addressed to Koninklijke Brill NV.</license-p>
  </license>
</permissions>
```

> ## Open Access
>
> Add the license information as specified by the Brill desk editor. For books, use the information as noted on page iv of the front matter.
> Do not forget to change the `@xlink:href`, `@xlink:title` and the texts of the `<license-p>` as needed for the specific Creative Commons license.
>
```
<permissions>
    <copyright-statement>Copyright YYYY by Author(s)/Editor(s).</copyright-statement>
    <copyright-year>YYYY</copyright-year>
    <copyright-holder>Author(s) and/or Editor(s)</copyright-holder>
    <license license-type="open-access" xlink:href="http://creativecommons.org/licenses/by-nc/4.0" xlink:title="CC-BY-NC">
      <license-p>This is an open access *[book/chapter/article]* distributed under the terms of the CC BY-NC 4.0 License.</license-p>
    </license>
</permissions>
```

> ## Free Access
>
> In some cases Brill wants to use the `<ali:free_to_read>` tag to indicate free content. Brill will supply the information for `@start_date` and `@end_date` (if applicable).
>
```
<permissions>
  <copyright-statement>Copyright YYYY by Koninklijke Brill NV, Leiden, The Netherlands</copyright-statement>
  <copyright-year>YYYY</copyright-year>
  <copyright-holder>Koninklijke Brill NV</copyright-holder>
  <license license-type="ccc">
    <license-p>This work is published by Koninklijke Brill NV. Koninklijke Brill NV incorporates the imprints Brill, Brill Nijhoff, Brill Hotei, Brill Sense, Brill Schöningh, Brill Fink, Brill mentis, Vandenhoeck & Ruprecht, Böhlau Verlag and V&R Unipress. Koninklijke Brill NV reserves the right to protect the publication against unauthorized use and to authorize dissemination by means of offprints, legitimate photocopies, microform editions, reprints, translations, and secondary information sources, such as abstracting and indexing services including databases. Requests for commercial re-use, use of parts of the publication, and/or translations must be addressed to Koninklijke Brill NV.</license-p>
  </license>
  <ali:free_to_read xmlns:ali="http://www.niso.org/schemas/ali/1.0/" start_date="YYYY-MM-DD" end_date="YYYY-MM-DD"/>
</permissions>
```

### supplementary-material example

Examples for supplementary material.

## `@specific-use="local"`

```
<supplementary-material specific-use="local" xlink:href="1568539x_148_09-10_s009_s0001.mp4" mimetype="application/mp4"><caption><title>File Title</title></caption></supplementary-material>
```

## `@specific-use="figshare"`

```
<supplementary-material specific-use="figshare" xlink:href="10.6084/m9.figshare.4725469.v1" />
```

### table example

An example for a table. 

```
<sec>
    ...
    <table-wrap id="001"><caption><label>Tabel 1</label><title>Distribution of Biblical Manuscripts according to Textual Typology</title></caption>
        <table frame="none" cellpadding="5">                    
            <thead>
                <tr>
                    <th align="left" valign="top">Pentateuch according to Lange</th>
                    <th align="left" valign="top">Prophets and Ketuvim according to Lange</th>
                    <th align="left" valign="top">Pentateuch according to Tov<xref ref-type="fn"
                        rid="mul2">a</xref></th>
                    <th align="left" valign="top">Prophets and Ketuvim according to Tov</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="left" valign="top">pre-SP</td>
                    <td align="left" valign="top">Vorlage of LXX</td>
                    <td align="left" valign="top">Equally close to MT and SP</td>
                    <td align="left" valign="top">semi-MT</td>
                </tr>
                <tr>
                    <td align="left" valign="top">52.5%</td>
                    <td align="left" valign="top">51%</td>
                    <td align="left" valign="top">39.0%</td>
                    <td align="left" valign="top">49.0%</td>
                </tr>
                <tr>...</tr>
                ...
            </tbody>
        </table>
        <table-wrap-foot>
            <fn symbol="a" id="mul2">
                <p>The numbers of Tov are quoted from TCHB, 108; cf. Tov, “Scribal and Textual Transmission,” 58–59.</p>
            </fn>
        </table-wrap-foot>
    </table-wrap>
    ...   
</sec>

``` 

### trans-title-group example

An example for a translated title group.

```
  ...
<book-meta>
	<book-id>...</book-id>
	<book-title-group>
		<book-title>Is this real? Phenomenology of the Imaginary.</book-title>
		<trans-title-group xml:lang="nl">
			<trans-title>Bestaat dit echt?</trans-title>
		</trans-title-group>
		<trans-title-group xml:lang="fr">
			<trans-title>Est-ce réel? Phénoménologies de l’imaginaire</trans-title>
		</trans-title-group>
	</book-title-group>
...</book-meta>
...   
``` 

### verse-group example

An example for a verse-group. 

```
<sec>
    ....
    <verse-group>
        <label>Du 2, 899</label>
        <verse-line>&#x723E;&#x66F9;&#x8EAB;&#x8207;&#x540D;&#x4FF1;&#x6EC5;</verse-line>
        <verse-line>&#x4E0D;&#x5EE2;&#x6C5F;&#x6CB3;&#x842C;&#x53E4;&#x6D41;</verse-line>
        <verse-line>All ye scornful are gone, body and name,</verse-line>
        <verse-line>But forever the rivers will go on flowing!</verse-line>
        <attr>Du Fu (712&#x2013;770)</attr>
    </verse-group>
    ....
</sec>
``` 

### reference-list example

An example of an article reference.

```
...
  <ref id="B8">
    <label>8</label>
    <mixed-citation publication-type="journal" publication-format="print">
        <person-group person-group-type="author"><string-name><surname>Martínez-Pantoja</surname>,
        <given-names>M. A.</given-names></string-name>, <string-name><surname>Alcocer</surname>,
        <given-names>J.</given-names></string-name>, <string-name><surname>Maeda-Martínez</surname>, <given-names>A.</given-names></string-name></person-group>. <year>2002</year>.
        <article-title>On the Spinicaudata (Branchiopoda) from Lake Cuitzeo, Michoacán, México: first report of a clam shrimp fishery</article-title>.
        <source>Hydrobiologia</source> <volume>486</volume>: <fpage>207</fpage>&ndash;<lpage>213</lpage>.
      </mixed-citation>
    </ref>
</ref-list>
...
```

An example of a book reference.

```
...
  <ref id="B10">
    <label>10</label>
    <mixed-citation publication-type="journal" publication-format="print">
        <person-group person-group-type="author"><string-name><surname>Rogers</surname>, <given-names>D. C.</given-names>
        </string-name></person-group> <year>2009</year>.
        <chapter-title>Branchiopoda (Anostraca, Notostraca, Laevicaudata, Spinicaudata, Cyclestherida)</chapter-title>, pp. <fpage>242</fpage>&ndash;<lpage>249</lpage>. In, <person-group person-group-type="editor">
        <string-name><surname>Likens</surname><given-names>G. R.</given-names> (Ed.)</person-group>,
        <source>Encyclopedia of Inland Waters</source>. <named-content content-type="subtitle">Vol. 2</named-content>.
        <publisher-name>Elsevier</publisher-name>, <publisher-loc>Oxford</publisher-loc>.
      </mixed-citation>
    </ref>
</ref-list>
...
```

### Collections Example

An example for collection.

> ## Only Series
>
> If there is no `<volume-number`, do not add `<volume-in-collection>`.
>
```
<?xml version="1.0" encoding="UTF-8"?>
<collection-meta collection-type="series">
  <collection-id collection-id-type="publisher">RSA</collection-id>
  <title-group>
    <title>The Renaissance Society of America</title>
    <sub-title>Texts and Studies Series</sub-title>
  </title-group>
  <volume-in-collection>
    <volume-number>2</volume-number>
  </volume-in-collection>
  <issn publication-format="print">2212-3091</issn>
</collection-meta>
```

>
> ## Series and Sub-Series
>
> If there is only 1 number, add to `<volume-number>` in `@collection-type="sub-series"` and remove `<volume-in-collection>` from `@collection-type="series"`.
>
```
<?xml version="1.0" encoding="UTF-8"?>
<collection-meta collection-type="series">
  <collection-id collection-id-type="publisher">BSAI</collection-id>
  <title-group>
    <title>Brill's Studies in Intellectual History</title>
  </title-group>
  <volume-in-collection>
    <volume-number>164</volume-number>
  </volume-in-collection>
  <issn publication-format="print">0920-8607</issn>
</collection-meta>
<collection-meta collection-type="sub-series">
  <collection-id collection-id-type="publisher">RSA</collection-id>
  <title-group>
    <title>Brill's Studies on Art, Art History, and Intellectual History</title>
    <sub-title></sub-title>
  </title-group>
  <volume-in-collection>
    <volume-number>1</volume-number>
  </volume-in-collection>
</collection-meta>
```

### String-name Example

An example for a `<contrib>` with multiple variations of their name. Note that `<name>` should be present both within and without `<name-alternatives>` in case multiple versions of an author's name are given.

```
<?xml version="1.0" encoding="UTF-8"?>
<contrib contrib-type="author">
  <name name-style="western">
    <surname>Chen (&#x9648;&#x9880;)</surname>
    <given-names>Qi</given-names>
  </name>
  <name-alternatives>
    <name name-style="western">
      <surname>Chen</surname>
      <given-names>Qi</given-names>
    </name>
    <string-name name-style="eastern" xml:lang="zh">&#x9648;&#x9880;</string-name>
  </name-alternatives>
  </contrib>
```

```
#Jats body&back 

**<body>** containing:
...
<fig>
<supplementary-material>@xmlns:xlink, xlink:title: figshare,
@position
<table-wrap> or:
*<table-wrap-group>@content-type containing:*
<table-wrap>

<p>
*<sec> containing:*
<label>
<title>

*<verse-group> containing: (=in case of poetry)*
<title>
<verse-line>
<attrib>

*<disp-quote> containing: (=in case of quotes)*
<p>


**<back>** containing:
<label>
<title>
<fn-group>
<ref-list>
<notes> Notes
<sec> Section

*<fn-group> containing:*
<label>
<title>
<fn>
@fn-type

*<ref-list> containing:*
<label> 
<title> 
<supplementary-material>@xmlns:xlink, xlink:title: figshare,
@position
<table-wrap> or:
<table-wrap-group>@content-type containing:
<table-wrap>

<p>
<ref>
@id
<ref-list>

*<ref> containing:*
<element-citation> !<citation> does not exist any more!
@publication-type:
bookparts
confproc
journal
other
thesis
web
@publication-format
<mixed-citation> = textual
<person-group><surname><given-names>
@person-group-type:
author
editor
<source>
<article-title>
<year>
<volume>
<issue>
<fpage>
<lpage>
<edition>
<publisher-loc>
<publisher-name>

<notes> containing:
@notes-type 
<sec>
??<supplementary-material>??


BITS-specific:
<book-part>
<book-part-meta>
```

```
<bio> ✓
<glossary> ✓
<def-list> ✓
<xref> ✓
extra <title-group> (speciaal voor BITS)

?? En zei jij niet dat er zoiets was als bloc-quote? << dat is disp-quote blijkbaar.



<body>
<fig>
<fig-group>
<table-wrap>
<table-wrap-group>
<sec>
<verse-group>
<title>
<verse-line>
<attrib>
<disp-quote>
<fn-group>
<fn>
<ref-list>
<ref>
<element-citation>
<mixed-citation>
<source>
<book-part>
<book-part-meta>

..en mogen aanvullend deze elementen er ook bij:
<alternatives> ✓
<annotation> ✓
<attrib> ✓
<bio> ✓
<boxed-text> ✓
<def> ✓
<term-head> ✓
<def-head> ✓
<def-item> ✓
<index> -just bits ✓
<index-term> -just bits ✓
<term> -just bits ✓
<see-also> -just bits ✓
<list> ✓
<list-item> ✓
<nav-point> ✓
<named-content> ✓
<sig-block> ✓ 
<sig> ✓ 
<table> ✓ 
<thead> ✓ 
<tbody> ✓ 
<tfoot> ✓ 
<tbody> ✓ 
<tr> ✓ 
<th> ✓ 
<td> ✓ 
```

```
#Examples list2

##*article*
*journal-id*
*journal-title-group*
*issn*
###*article-categories*
*article-meta*
###*body*
*counts*
*f-page*
*custom-meta-group*

##*book*
*collection-meta*
*collection-id*

##*book-meta*
###*kwd-group* 

###*book-body*
###*body*
###*book-part*
###*dedication*

*book-page-count*
###*book-volume-id*
*book-volume-number*
*custom-meta-group*

##*book-title-group*
###*alt-title*
###*trans-title-group*

##*contrib-group*
*name*
*aff*
###*address*
###*award-group*
###*author-notes*
###*bio*
###*collab*

###*date-in-citation*

###*disp-quote*
###*edition*

###*body*
*boxed-text*
*caption*
*sec*
*p*
##*fig-group*
*alternatives*
*table-wrap-group*
*label*
*list*

###*fn-group*

###*funding-group*

###*glossary*
###*history*
###*index*

###*chapter-title*

###*named-book-part*
###*named-content*
*notes*

###*person-group*

*permissions*

###*pub-history*

*publisher*

##*ref-list*
###*element-citation*
###*annotation*

*self-uri*
###*sig-block*
###*supplementary-material*

###*uri*
###*verse-group*
*volume*
*volume-in-collection*

```

```
#Examples list

*address*
*aff*
*alternatives*
*alt-title*
*annotation*
*article*
*article-categories*
*article-meta*
*author-notes*
*award-group*
*bio*
*body*
*book*
*book-body*
*book-meta*
*book-page-count*
*book-part*
*book-title-group*
*book-volume-id*
*book-volume-number*
*boxed-text*
*caption*
*chapter-title*
*collab*
*collection-id*
*collection-meta*
*contrib-group*
*counts*
*custom-meta-group*
*date-in-citation*
*dedication*
*disp-quote*
*edition*
*element-citation*
*fig-group*
*fn-group*
*f-page*
*funding-group*
*glossary*
*history*
*index*
*issn*
*journal-id*
*journal-title-group*
*kwd-group*
*label*
*list*
*name*
*named-book-part*
*named-content*
*notes*
*p*
*person-group*
*permissions*
*pub-history*
*publisher*
*ref-list*
*sec*
*self-uri*
*sig-block*
*supplementary-material*
*table-wrap-group*
*trans-title-group*
*uri*
*verse-group*
*volume*
*volume-in-collection*
```
